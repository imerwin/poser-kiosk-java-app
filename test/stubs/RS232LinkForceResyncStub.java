/******************************************************************************
* Title: RS232LinkStubStub.java
* Author: Mike Schoonover
* Date: 01/21/2022
*
* Purpose:
*
* This class is a stub for RS232LinkStub connections. It returns simulated data for testing
* purposes.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package stubs;

//-----------------------------------------------------------------------------

import remoteDeviceHandlers.*;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import controller.LoggerBasic;

import com.fazecast.jSerialComm.SerialPort;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class RS232LinkForceResyncStub
//
//
//

public class RS232LinkForceResyncStub implements Communications{

	boolean comPort;

	byte[] inByte = new byte[1];
    byte[] inBuffer = new byte[10];
    byte[] outBuffer = new byte[10];

	StringBuilder inString = new StringBuilder(1024);

	boolean simulate = false;

	LoggerBasic tsLog;

	//the timeout used for reading and writing of the serial port
	static final int PORT_TIMEOUT = 500; //debug mks -- set back to 250

	//multiply READ_LOOP_TIMEOUT * READ_SLEEP_MS to get time out in millseconds
	//these values are used in a loop
	static final int READ_LOOP_TIMEOUT = 50;
	static final int READ_SLEEP_MS = 10;

	static final int LINE_TERMINATOR = 0x0a;

	static final int READ_BYTES_LIMIT = 10000;

	static final int NUM_BYTES_TO_READ_FOR_CLEARING = 100000;

	static final int DEVICE_RESPONSE_TIMEOUT = 5000;

	static final int SUCCESS = 0;
	static final int PORT_CLOSED = 1;
	static final int PORT_ERROR = 2;
	static final int TIMEOUT = 3;

	static final long RPLIDAR_ANSWER_SYNC_BYTE1		= 0xA5;
	static final long RPLIDAR_ANSWER_SYNC_BYTE2		= 0x5A;
	public static final long RPLIDAR_ANSWER_TYPE_DEVICE_INFO = 0x4;

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::RS232LinkForceResyncStub (constructor)
//

public RS232LinkForceResyncStub()
{

}//end of RS232LinkForceResyncStub::RS232LinkForceResyncStub (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

@Override
public void init(LoggerBasic pThreadSafeLogger)
{

    tsLog = pThreadSafeLogger; comPort = false;

}// end of RS232LinkForceResyncStub::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::connectToRemote
//
/**
 * Establishes a connection with the remote device.
 *
 * STUB FUNCTIONALITY: sets comPort = true to indicate connection has been made; mimics other
 *						functions from the actual class
 *
 *
 * @param pSelectedCOMPort	the name of the Serial COM port to be used for the connection
 * @param pBaudRate			baud rate for the connection
 * @return					1 on success, -1 on failure to connect with device
 *
 */

@Override
public int connectToRemote(String pSelectedCOMPort, int pBaudRate)
{

	closeConnectionToRemote();

	int portIndex = getIndexOfSerialPortSpecifiedByName(pSelectedCOMPort);

	if(portIndex == -1){ return(-1); }

	comPort = true; //STUB FUNCTIONALITY : stub uses boolean instead of a reference

	if(comPort == false){
		tsLog.appendLine("");
		tsLog.appendLine("ERROR: cannot open COM Port!!!");
		tsLog.appendLine("");
		return(-1);
	}

	setPortOptions(pBaudRate);

	setDTR(); //establishes connection with device's com port

	tsLog.appendLine("Baud Rate = " + pBaudRate + "...");

	tsLog.appendLine("Device is connected.\n");

	return(1);

}// end of RS232LinkForceResyncStub::connectToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::connectToRemote
//
/**
 * Connects to remote device using an IP address. Not used in this class.
 *
 * @param pIPAddr			the IP address of the remote device
 * @param pPort				the port on the remote device
 * @return					always returns -1 which designates failure
 *
 */

@Override
public int connectToRemote(InetAddress pIPAddr, int pPort)
{

	return(-1);

}// end of RS232LinkForceResyncStub::connectToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::getIsConnected
//
/**
 * Returns the connected/unconnected state of the communication link.
 *
 * @return	true if connected, false if not connected
 *
 */

@Override
public boolean getIsConnected()
{

	if(comPort != false) {
		return(true);
	}else{
		return(false);
	}

}// end of RS232LinkForceResyncStub::getIsConnected
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::closeConnectionToRemote
//
/**
 * Closes the com port connection. Also sets the DTR line low.
 *
 * @return	true if port successfully closed, false if not
 *
 */

@Override
public boolean closeConnectionToRemote()
{

	if(comPort == false) { return(true); }

	clearDTR(); //breaks connection with device's com port

	return(true);

}// end of RS232LinkForceResyncStub::closeConnectionToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::setDTR
//
/**
 * Sets the serial port DTR line.
 *
 * STUB FUNCTIONALITY: returns true if connected, false otherwise
 *
 * @return	true if operation is successful
 *
 */

@Override
public boolean setDTR()
{

	if(comPort == false) { return(true); }

	return(true);

}// end of RS232LinkForceResyncStub::setDTR
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::clearDTR
//
/**
 * Clears the serial port DTR line.
 *
 * STUB FUNCTIONALITY: returns true if connected, false otherwise
 *
 * @return	true if operation is successful
 *
 */

@Override
public boolean clearDTR()
{

	if(comPort == false) { return(false); }

	return(true);

}// end of RS232LinkForceResyncStub::clearDTR
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::disposeOfAllResources
//
// Releases all objects created which may have back references. This is
// necessary because some of those objects may hold pointers back to a parent
// object (such as Java Listeners and such). These back references will prevent
// the parent object from being released and a memory leak will occur.
//
// Not all objects must be explicitly released here as most will be
// automatically freed when this object is released.
//

@Override
public void disposeOfAllResources()
{


}//end of RS232LinkForceResyncStub::disposeOfAllResources
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::setPortOptions
//
/**
 * Sets various options for comPort such as timeouts, baud rate, stop bit, parity, flow control.
 *
 * @param pBaudRate		the baud rate for the serial com port
 *
 */

void setPortOptions(int pBaudRate)
{

}// end of RS232LinkForceResyncStub::setPortOptions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::getInputStream
//
/**
 * Returns an InputStream for the serial port.
 *
 * @return InputStream for the serial port.
 *
*/

public InputStream getInputStream()
{

	return(null);

}// end of RS232LinkForceResyncStub::getInputStream
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::getOutputStream
//
/**
 * Returns an OutputStream for the serial port.
 *
 * @return OutputStream for the serial port.
 *
*/

public OutputStream getOutputStream()
{

	return(null);

}// end of RS232LinkForceResyncStub::getOutputStream
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::sendString
//
// Sends a String to the remote device. A LINE_TERMINATOR value is sent as the last byte, so it
// should not be added to the String.
//

public void sendString(String pString)
{

	if(comPort == false){ return; }

	try {

		byte tempBuffer[] = pString.getBytes();

		//comPort.writeBytes(tempBuffer, tempBuffer.length); //need to simulate

		outBuffer[0] = LINE_TERMINATOR;
		//comPort.writeBytes(outBuffer, 1); //need to simulate

	} catch (Exception e) {
		logSevere(e.getMessage() + " - Error: 422");
	}

}// end of RS232LinkForceResyncStub::sendString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::sendBytes
//
/**
 *
 * Sends an array of bytes to the remote device.
 *
 * @param pBytes	array of bytes to be sent
 * @param pNumBytes	the number of bytes to send
 *
 * @return			The number of bytes successfully written, or -1 if there was an error writing
 *					to the port.
 *
 */

@Override
public int sendBytes(byte[] pBytes, int pNumBytes)
{

	if(comPort == false){ return(-1); }

	try {

		//return(comPort.writeBytes(pBytes, pNumBytes));
		return(0); //need to simulate

	} catch (Exception e) {
		logSevere(e.getMessage() + " - Error: 536");
		return(-1);
	}

}// end of RS232LinkForceResyncStub::sendBytes
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::readStringResponse
//
// Calls readStringResponse(READ_BYTES_LIMIT).
//
// See readStringResponse(int pMaxNumBytes) for details.
//

public String readStringResponse()
{

	return(readStringResponse(READ_BYTES_LIMIT));

}// end of RS232LinkForceResyncStub::readStringResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::waitForData
//
/**
 * Waits until a specified number of bytes are available for reading. Will stop waiting if
 * specified time out duration is reached. Returns the number of bytes available via an IntParameter
 * object. Returns a success/error code.
 *
 * STUB FUNCTIONALITY: simply returns pNumBytestoWaitFor as the number of bytes available.
 *
 * @param pNumBytesToWaitFor	the number of bytes to wait for before returning
 * @param pTimeOut				the maximum number of milliseconds to wait
 * @param pNumBytesAvailable	returns the number of bytes available or zero on time out or error
 * @return						success/error code: SUCCESS, PORT_CLOSED, PORT_ERROR, TIMEOUT
 *
 */

@Override
public int waitForData(long pNumBytesToWaitFor, long pTimeOut, IntParameter pNumBytesAvailable)
{

	pNumBytesAvailable.i = 0;

	if(comPort == false){ return (PORT_CLOSED); }

	long startTimeMS = System.currentTimeMillis();

    while ((System.currentTimeMillis() - startTimeMS) <= pTimeOut) {

		//int numBytesAvailable = comPort.bytesAvailable​();
		int numBytesAvailable = (int)pNumBytesToWaitFor; //STUB FUNCTIONALITY

		if (numBytesAvailable == -1){ return(PORT_CLOSED); }

		if (numBytesAvailable >= pNumBytesToWaitFor){
			pNumBytesAvailable.i = numBytesAvailable;
			return(SUCCESS);
		}
	}

	return(TIMEOUT);

}// end of RS232LinkForceResyncStub::waitForData
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::readStringResponse
//
/**
 * Reads a character response from the remote device. Characters will be read until pMaxNumBytes
 * have been read, LINE_TERMINATOR is received to indicate the end of the response, or timeout
 * occurs. The response will be returned as a String. If timeout before any characters received, the
 * String will have length of 0. If timeout before LINE_TERMINATOR is received, the last character
 * of the String will not be LINE_TERMINATOR.
 *
 * timeout in milliseconds ~= READ_LOOP_TIMEOUT * READ_SLEEP_MS
 *
 * NOTE: this function is not normally used when data packets are used to communicate. It might
 * be used to receive an initial greeting, but that could also be done using packets.
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @return				the string read - terminated if terminator received before timeout or
 *						pMaxNumBytes characters read; empty string if timeout occurs before any
 *						characters received; null if an error occurs
 *
 */

public String readStringResponse(int pMaxNumBytes)
{

	if(comPort == false){ return(""); }

	int timeoutLoopCounter = 0;
	boolean done = false;

	inString.setLength(0);

	while (!done && timeoutLoopCounter++ < READ_LOOP_TIMEOUT){

		while(!done /*&& comPort.bytesAvailable() != 0 need to simulate*/){

			//comPort.readBytes(inByte, 1); //need to simulate

			inString.append((char)inByte[0]);

			timeoutLoopCounter = 0;

			if(inString.length() == pMaxNumBytes){ done = true; break; }

			if(inByte[0] == LINE_TERMINATOR){ done = true; }
		}

		if(!done) { threadSleep(READ_SLEEP_MS); }

	}

	return(inString.toString());

}// end of RS232LinkForceResyncStub::readStringResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::clearReceiveBuffer
//
/**
 * Clears the receive buffer by reading all bytes available. The function loops until the
 * number of available bytes is 0. Looping is done to help ensure that any bytes that might arrive
 * during the clearing are also cleared on the next pass. Also, the bytesAvailable function of some
 * libraries is an estimate, so looping until zero helps make sure the buffer is fully cleared.
 *
 * @return number of bytes cleared, -1 on error
 *
 */

@Override
public int clearReceiveBuffer()
{

	int numBytesAvailable = 0;
	int numBytesCleared = 0;

	while((numBytesAvailable /*= comPort.bytesAvailable()) need to simulate*/ != 0)){

		numBytesCleared += numBytesAvailable;

		byte[] inBytes = new byte[numBytesAvailable];

		//comPort.readBytes(inBytes, numBytesAvailable);

	}

	return(numBytesCleared);

}// end of RS232LinkForceResyncStub::clearReceiveBuffer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::readBytesResponse
//
/**
 * Reads a byte block response from the remote device. Characters will be read until pMaxNumBytes
 * have been read or timeout occurs.
 *
 * The response will be returned as a byte array. If timeout before any characters received, the
 * byte array will be empty.
 *
 * //todo mks ~ next comment is no longer true...return as a parameter?
 * The number of bytes read will be stored in class member variable numBytesRead.
 *
 * timeout in milliseconds ~= READ_LOOP_TIMEOUT * READ_SLEEP_MS
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @return				byte array containing bytes read from stream; may be empty
 *						the number of bytes read is stored in class member variable numBytesRead
 *
 */

@Override
public byte[] readBytesResponse(int pMaxNumBytes)
{

	if(comPort == false){ return(null); }

	int timeoutLoopCounter = 0;
	boolean done = false;
	byte[] inBytes = new byte[pMaxNumBytes];

	int i = 0;

	while (!done && timeoutLoopCounter++ < READ_LOOP_TIMEOUT){

		while(!done /* && comPort.bytesAvailable() != 0 need to simulate*/){

			//comPort.readBytes(inByte, 1); //need to simulate

			inBytes[i++] = inByte[0];

			timeoutLoopCounter = 0;

			if(i == pMaxNumBytes){ done = true; break; }

		}

		if(!done) { threadSleep(READ_SLEEP_MS); }

	}

	return(inBytes);

}// end of RS232LinkForceResyncStub::readBytesResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::readBytesResponse
//
/**
 * Reads a byte block response from the remote device. Characters will be read until pMaxNumBytes
 * have been read or pTimeOut milliseconds have passed.
 *
 * The response will be returned in pByteArray. If timeout before any characters received, the
 * byte array data will be invalid.
 *
 * The number of bytes read will be stored in pNumBytesRead.
 *
 * STUB FUNCTIONALITY:
 *	returns 4 garbage bytes and then a valid packet for testing resync split up over two calls
 *  the packet type is RPLIDAR_ANSWER_TYPE_DEVICE_INFO
 *  the payload size is 0x12345678 for testing purposes
 *  In the first call, it returns the first 5 bytes (four junk bytes and the first header ID byte).
 *  In the second call, it returns the remainder of the packet starting with the second header ID
 *  byte. Splitting between the header bytes (0xA5,0x5A) is a good test of the client codes sync
 *  logic.
 *
 *  The third call starts the sequence over.
 *
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @param pByteArray	a reference to an array for storing the bytes read
 * @param pNumBytesRead	returns the number of bytes available or zero on time out or error
 *
 * @return				success/error code: SUCCESS, PORT_CLOSED, PORT_ERROR, TIMEOUT
 *
 */

int numReadBytesResponseCalls = 0;

@Override
public int readBytesResponse(long pMaxNumBytes, byte[] pByteArray, IntParameter pNumBytesRead)
{

	if(comPort == false){ return(PORT_CLOSED); }

	pNumBytesRead.i = 0;

	//int lNumBytesRead = comPort.readBytes​(pByteArray, pMaxNumBytes);

	//STUB FUNCTIONALITY

	int x, numBytesRead = -1;

	//garbage bytes before the packet

	switch(numReadBytesResponseCalls){

		case 0:
			//on first call, send 4 junk bytes and the first header ID flag
			x = 0;
			pByteArray[x++] = 0; pByteArray[x++] = 1; pByteArray[x++] = 2; pByteArray[x++] = 3;
			pByteArray[x++] = (byte)RPLIDAR_ANSWER_SYNC_BYTE1;
			numBytesRead = x;
			numReadBytesResponseCalls++;
		break;

		case 1:
			//on second call, send second header ID flag and remainder of header
			x = 0;
			pByteArray[x++] = (byte)RPLIDAR_ANSWER_SYNC_BYTE2;
			// 0x12345678 sgtored in C++ Little Endian order
			pByteArray[x++] = 0x78; pByteArray[x++] = 0x56;
			pByteArray[x++] = 0x34; pByteArray[x++] = 0x12;
			pByteArray[x++] = (byte)RPLIDAR_ANSWER_TYPE_DEVICE_INFO;
			numBytesRead = x;
			numReadBytesResponseCalls = 0; //start the sequence over
		break;
	}

	//STUB FUNCTONALITY END

	if(numBytesRead == -1){ return(PORT_ERROR); }

	pNumBytesRead.i = numBytesRead;

	return(SUCCESS);

}// end of RS232LinkForceResyncStub::readBytesResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::convertBytesToCommaDelimitedDecimalString
//
// Returns the bytes in pBuffer as a comma-delimited string of decimal numbers.
//

public String convertBytesToCommaDelimitedDecimalString(byte[] pBuffer)
{

	inString.setLength(0);

	for(byte b : pBuffer){
		inString.append(b);
		inString.append(',');
	}

	return(inString.toString());

}// end of RS232LinkForceResyncStub::convertBytesToCommaDelimitedDecimalString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::getListOfSerialComPorts
//
// Retrieves from the operating system a list of the available serial com ports.
//
// Returns an array of SerialPort objects.
//

public SerialPort[] getListOfSerialComPorts()
{

	return(SerialPort.getCommPorts());

}// end of RS232LinkForceResyncStub::getListOfSerialComPorts
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::getListOfSerialComPortNames
//
// Retrieves from the operating system a list of names of the available serial com ports. These
// will be in the form of "COM1", "COM25", etc.
//
// STUB FUNCTIONALITY: returns a list with names "COM1", "COM2", "COM3".
//
// Returns a ListArray<String> object.
//

public ArrayList<String> getListOfSerialComPortNames()
{

	SerialPort[] serialPorts = getListOfSerialComPorts();

	ArrayList<String> names = new ArrayList<>(serialPorts.length);

	for(SerialPort port : serialPorts){ names.add(port.getSystemPortName()); }

	//STUB FUNCTIONALITY
	names.add("COM1"); names.add("COM2"); names.add("COM3");
	//STUB FUNCTIONALITY END

	return(names);

}// end of RS232LinkForceResyncStub::getListOfSerialComPortNames
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::getIndexOfSerialPortSpecifiedByName
//
// Finds the index of the serial port with a name matching pName
//
// Returns the index of the port.
// If no match is found, returns -1.
//

protected int getIndexOfSerialPortSpecifiedByName(String pName)
{

	ArrayList<String> ports =  getListOfSerialComPortNames();

	if(ports.isEmpty()){
		tsLog.appendLine("no serial port found!");
		return(-1);
	}

	if(!ports.contains(pName)){
		tsLog.appendLine(pName + " serial port not found!");
		return(-1);
	}

	return(ports.indexOf(pName));

}// end of RS232LinkForceResyncStub::getIndexOfSerialPortSpecifiedByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::sendInitCommands
//

public void sendInitCommands(){



}// end of RS232LinkForceResyncStub::sendInitCommands
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::threadSleep
//
// Calls the Thread.sleep function. Placed in a function to avoid the
// "Thread.sleep called in a loop" warning -- yeah, it's cheezy.
//
// Returns true if InterruptedException caught...this usually means some other thread wants to
// shut down this thread.
//

public boolean threadSleep(int pSleepTime)
{

    try {Thread.sleep(pSleepTime);} catch (InterruptedException e) { return(true); }

	return(false);

}//end of RS232LinkForceResyncStub::threadSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkForceResyncStub::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of RS232LinkForceResyncStub::logSevere
//-----------------------------------------------------------------------------

	@Override
	public void open() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void close() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public byte readByte() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public byte[] readBytes(int pNumBytes) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public byte peek() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}//end of class RS232LinkForceResyncStub
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
