/******************************************************************************
* Title: ThreadSafeLoggerTestDoubleTestDouble.java
* Author: Mike Schoonover
* Date: 12/31/21
*
* Purpose:
*
* This class handles logging messages to System.out. It is useful as a Stub to replace classes
* which implement LoggerBasic.
*
* NOTE: This stub class is NOT thread safe like the class it replaces...it just prints all messages
* to System.out.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package stubs;

import controller.LoggerBasic;
import java.util.ArrayList;
import java.util.Date;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class ThreadSafeLoggerStub
//

public class ThreadSafeLoggerStub implements LoggerBasic {

    static public final String NEW_LINE = "\n";

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::ThreadSafeLoggerStub (constructor)
//
/**
 * Constructor.
 *
 *
 */

public ThreadSafeLoggerStub()
{

}//end of ThreadSafeLoggerStub::ThreadSafeLoggerStub (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::logMessage
//
/**
 * Prints pMessage to System.out.
 *
 * @param pMessage	the message to be printed
 *
*/

@Override
public void logMessage(String pMessage)
{

	System.out.println(pMessage);

}//end of ThreadSafeLoggerStub::logMessage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::logMessageThreadSafe
//
/**
 * This function does nothing as this class is not actually thread safe and merely prints
 * everything to System.out.
 *
 */

@Override
public void logMessageThreadSafe()
{

}//end of ThreadSafeLoggerStub::logMessageThreadSafe
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::separate
//
/**
 * Writes a separator (such as a line of dashes) to the output.
 *
 */

@Override
public void separate()
{

    System.out.println("--------------------------------------------------------------------\n");

}//end of ThreadSafeLoggerStub::separate
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::date
//
/**
 * Writes the date to the output.
 *
 */

@Override
public void date()
{

    System.out.println(new Date().toString() + "\n");

}//end of ThreadSafeLoggerStub::date
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::section
//
/**
 * Writes a blank line, a separator, the date, a blank line to the output.
 *
 */

@Override
public void section()
{

    System.out.println("");
    separate();
    date();
    System.out.println("");

}//end of ThreadSafeLoggerStub::section
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::saveToFile
//
/**
 * Does nothing in this implementation.
 *
 * @param pFilenameSuffix	not used in this implementation
 *
 */

@Override
public void saveToFile(String pFilenameSuffix)
{

}//end of ThreadSafeLoggerStub::saveToFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::saveToFileThreadSafe
//
/**
 * Does nothing in this implementation.
 *
 */

@Override
public void saveToFileThreadSafe()
{

}//end of ThreadSafeLoggerStub::saveToFileThreadSafe
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::displayErrorMessage
//
/**
 * Logs an error of message pMessage to the output prepended by "Error: ".
 *
 * @param pMessage	the error message to be logged
 *
 */

@Override
public void displayErrorMessage(String pMessage)
{

    System.out.println("Error: " + pMessage);

}//end of ThreadSafeLoggerStub::displayErrorMessage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::appendString
//
/**
 * Prints a message to System.out.
 *
 * @param pText	the message to be printed
 *
 */
@Override
public void appendString(String pText)
{

    logMessage(pText);

}//end of ThreadSafeLoggerStub::appendString
//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::appendArrayListOfStrings
//
/**
 * Prints a list of Strings to System.out.
 *
 * @param pStrings	the ArrayList of Strings to be printed
 *
 */

@Override
public void appendArrayListOfStrings(ArrayList<String> pStrings)
{

	for(String line : pStrings){ logMessage(line + NEW_LINE); }

}//end of ThreadSafeLoggerStub::appendArrayListOfStrings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::appendLine
//
/**
 * Prints a message to System.out and appends a blank line.
 *
 * @param pText the message to be printed
 *
 */

@Override
public void appendLine(String pText)
{

    logMessage(pText + NEW_LINE);

}//end of ThreadSafeLoggerStub::appendLine
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ThreadSafeLoggerStub::appendToErrorLogFile
//
// Appends pMessage to the error log file "Error Log.txt".
//
// The Log.appendToErrorLogFile method is called directly as it should be
// threadsafe as it does not modify GUI components.
//

@Override
public void appendToErrorLogFile(String pMessage)
{

}//end of ThreadSafeLoggerStub::appendToErrorLogFile
//-----------------------------------------------------------------------------

}//end of class ThreadSafeLoggerStub
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
