/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package remoteDeviceHandlers;

import static org.testng.Assert.*;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

/**
 *
 * @author mike
 */
public class BufferFilterNGTest {

	BufferFilter<Integer> instance;

	public BufferFilterNGTest() {
	}

	@BeforeClass
	public static void setUpClass() throws Exception {
	}

	@AfterClass
	public static void tearDownClass() throws Exception {
	}

	@BeforeMethod
	public void setUpMethod() throws Exception {
	}

	@AfterMethod
	public void tearDownMethod() throws Exception {
	}

	/**
	 * Test of init method, of class BufferFilter.
	 */
	@Test
	public void testInit() {

		System.out.println("");
		System.out.println("-- Start of TestNG tests --");
		System.out.println("TestNG for init...");

		instance = new BufferFilter<>(3);
		instance.init(Integer.MAX_VALUE);

		// TODO review the generated test code and remove the default call to fail.
		//	fail("The test case is a prototype.");
	}

	/**
	 * Test of addNewAndTossOldest method, of class BufferFilter.
	 */
	@Test
	public void testAddNewAndTossOldest() {

		System.out.println("---");
		System.out.println("TestNG for addNewAndTossOldest...");

		testInit(); //instantiate the object and call init

		int result, expResult;

		expResult = Integer.MAX_VALUE;
		result = instance.addNewAndTossOldest(1);
		assertEquals(result, expResult);

		expResult = Integer.MAX_VALUE;
		result = instance.addNewAndTossOldest(2);
		assertEquals(result, expResult, Double.MAX_VALUE);

		expResult = Integer.MAX_VALUE;
		result = instance.addNewAndTossOldest(3);
		assertEquals(result, expResult);

		expResult = 1;
		result = instance.addNewAndTossOldest(4);
		assertEquals(result, expResult);

		expResult = 2;
		result = instance.addNewAndTossOldest(5);
		assertEquals(result, expResult);

		expResult = 3;
		result = instance.addNewAndTossOldest(6);
		assertEquals(result, expResult);

		expResult = 4;
		result = instance.addNewAndTossOldest(6);
		assertEquals(result, expResult);

	}

	/**
	 * Test of checkForRepeatValueSequence method, of class BufferFilter.
	 */
	@Test
	public void testCheckForRepeatValueSequence() {


		System.out.println("---");
		System.out.println("TestNG for checkForRepeatValueSequence...");

		testAddNewAndTossOldest(); //instantiate the object and populate with values

		Integer result, expResult;

		// is there a sequence of 1 (should always work)

		expResult = 6;
		result = instance.checkForRepeatValueSequence(1);
		assertEquals(result, expResult);

		// is there a sequence of 2 (should be two "6.0" values at the end of the buffer)

		expResult = 6;
		result = instance.checkForRepeatValueSequence(2);
		assertEquals(result, expResult);

		// should NOT be a sequence of 3 duplicates (only two "6.0" values were inserted at end)

		expResult = Integer.MAX_VALUE;
		result = instance.checkForRepeatValueSequence(3);
		assertEquals(result, expResult);

	}

}
