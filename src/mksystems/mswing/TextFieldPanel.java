/******************************************************************************
* Title: TextFieldPanel.java
* Author: Mike Schoonover
* Date: 01/29/20
*
* Purpose:
*
* Creates a JPanel containing a description label, JTextField, and a trailing
* label to display units or other information.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package mksystems.mswing;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class TextFieldPanel
//
// Creates a JPanel containing a description label, JTextField, and a trailing
// label to display units or other information.
//
// The tooltip text for each label is set to the text passed for the label.
//
// TODO MKS ~ copy info from ComboBoxPanel...especially regarding addition of " Component" to
// pName for the name of the JTextField itself.

public class TextFieldPanel extends JPanel{

    protected JLabel leftLabel;
    protected JTextField textField;
    protected JLabel rightLabel;
	String name;
	int width, height;
	boolean editable;

	public JTextField getTextField(){ return(textField); }

	String leftLabelText, rightLabelText;

	protected ActionListener actionListener;
	String actionCommand;
	protected MouseListener mouseListener;

//-----------------------------------------------------------------------------
// TextFieldPanel::TextFieldPanel
//
// This class creates a JPanel containing a label, TextField, and a following label.
//
// Parameter pLeftLabelText is often a description of the values while pRightLabelText is often
// used to display the units (Mhz, inches, feet, each, etc.)
//
// Both the containing panel and the TextField component will be assigned name of pName.
//
// Parameters pWidth and pHeight set the dimensions of the TextField...NOT the panel. The panel
// will end up being automatically sized to hold the box and the labels.
//
// If pEditable is true, the value in the box can be edited by the user. Otherwise, the user can
// only select entries in the list.
//
// The object designated to respond to actions from the TextField is passed via pActionListener.
// The action command for the TextField is passed via pActionCommand.
//
// The object designated to respond to mouse events from the TextField is passed via pMouseListener.
//

public TextFieldPanel(String pLeftLabelText, String pRightLabelText,
								String pName, boolean pEditable, int pWidth, int pHeight,
								 String pActionCommand, ActionListener pActionListener,
								MouseListener pMouseListener)
{

	leftLabelText = pLeftLabelText; rightLabelText = pRightLabelText;
	name = pName; width = pWidth; height = pHeight; editable = pEditable;
	actionCommand = pActionCommand; actionListener = pActionListener;
	mouseListener = pMouseListener;

}//end of TextFieldPanel::TextFieldPanel (constructor for doubles)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TextFieldPanel::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public void init()
{

	setName(name);

    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

    add(leftLabel = new JLabel(leftLabelText));
    leftLabel.setToolTipText(leftLabelText);

    add(Box.createRigidArea(new Dimension(5,0)));

	add(Box.createHorizontalGlue());

	textField = new JTextField();
    textField.setName(name + " Component");
	textField.setEditable(editable);
	textField.addActionListener(actionListener);
	textField.addMouseListener(mouseListener);
	textField.setMinimumSize(new Dimension(width, height));
	textField.setMaximumSize(new Dimension(width, height));
	textField.setPreferredSize(new Dimension(width, height));
	add(textField);

	add(Box.createRigidArea(new Dimension(5,0)));

    add(rightLabel = new JLabel(rightLabelText));
    rightLabel.setToolTipText(rightLabelText);

}//end of TextFieldPanel::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TextFieldPanel::setEnabled
//
// Sets the enabled state for both the containing panel and the TextField.
//

@Override
public void setEnabled(boolean pState)
{

	super.setEnabled(pState);

	textField.setEnabled(pState);

}//end of TextFieldPanel::setEnabled
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// TextFieldPanel::setToolTipText
//
// Sets the tooltip for every GUI component in the panel to pText.
//

@Override
public void setToolTipText(String pText)
{

	super.setToolTipText(pText);

	leftLabel.setToolTipText(pText);
	rightLabel.setToolTipText(pText);
	textField.setToolTipText(pText);

}//end of TextFieldPanel::setToolTipText
//-----------------------------------------------------------------------------

}//end of class TextFieldPanel
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
