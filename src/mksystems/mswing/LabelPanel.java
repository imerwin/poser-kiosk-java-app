/******************************************************************************
* Title: LabelPanel.java
* Author: Mike Schoonover
* Date: 01/29/20
*
* Purpose:
*
* Creates a JPanel containing a description label, data label, and a trailing suffix
* label to display units or other information. The font size for the labels is specified at
* instantiation.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package mksystems.mswing;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.MouseListener;
import java.awt.font.TextAttribute;
import java.util.HashMap;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class LabelPanel
//
// Creates a JPanel containing a description label, a data label, and a trailing label to display
// units or other information.
//
// The tooltip text for each label is set to the text passed for the label.
//

public class LabelPanel extends JPanel{

    protected JLabel leftLabel;
    protected JLabel dataLabel;
    protected JLabel rightLabel;
	String name;
	int width, height, fontSize;
	Color fontColor, backgroundColor;

	FILL_STYLE fillStyle;

	public JLabel getDataLabel(){ return(dataLabel); }

	String leftLabelText, dataLabelText, rightLabelText;

	protected MouseListener mouseListener;

	public static enum FILL_STYLE{
		SPREAD,
		PUSH_LEFT,
		PUSH_RIGHT
	}
	
//-----------------------------------------------------------------------------
// LabelPanel::LabelPanel
//
// This class creates a JPanel containing a description label, data label, and a suffix label.
//	
// Parameter pLeftLabelText is often a description of the values while pRightLabelText is often
// used to display the units (Mhz, inches, feet, each, etc.)
//
// The data label text is passed via pDataLabelText.
//
// Both the containing panel and the Data Label component will be assigned name of pName.
//
// Parameters pWidth and pHeight set the dimensions of the Data Label...NOT the panel. The panel
// will end up being automatically sized to hold the labels. If either pWidth or pHeight are -1,
// then the label size will not be set and will be of default size.
//
// The font size for all the labels is specifed by pFontSize.
// The label color is specified by pFontColor.
// The panel background color is specified by pBackgroundColor.
//
// If pFillStyle is SPREAD, the left label and the data label will be pushed apart. This can be
// used to line multiple data labels up in a column.
//
// If pFillStyle is PUSH_LEFT, all three labels are pushed to the left of the panel.
//
// If pFillStyle is PUSH_RIGHT, all three labels are pushed to the right of the panel.
//
// The object designated to respond to mouse events from the Data Label is passed via
// pMouseListener.
//

public LabelPanel(String pLeftLabelText, String pDataLabelText, String pRightLabelText,
							String pName, int pWidth, int pHeight, int pFontSize,
							Color pFontColor, Color pBackgroundColor,
							FILL_STYLE pFillStyle, MouseListener pMouseListener)
{

	leftLabelText = pLeftLabelText; dataLabelText = pDataLabelText;
	rightLabelText = pRightLabelText;
	name = pName; width = pWidth; height = pHeight; fontSize = pFontSize;
	fontColor = pFontColor; backgroundColor = pBackgroundColor;
	fillStyle = pFillStyle; mouseListener = pMouseListener;

}//end of LabelPanel::LabelPanel (constructor for doubles)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// LabelPanel::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public void init()
{

	setName(name);

    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

	setBackground(backgroundColor);

	if (fillStyle == FILL_STYLE.PUSH_RIGHT){ add(Box.createHorizontalGlue()); }

    add(leftLabel = new JLabel(leftLabelText));
    leftLabel.setToolTipText(leftLabelText);

    add(Box.createRigidArea(new Dimension(5,0)));

	if (fillStyle == FILL_STYLE.SPREAD){ add(Box.createHorizontalGlue()); }

	dataLabel = new JLabel(dataLabelText);
    dataLabel.setName(name);
	dataLabel.addMouseListener(mouseListener);
    //dataLabel.setToolTipText(rightLabelText);

	if (width != -1 && height != -1){
		dataLabel.setMinimumSize(new Dimension(width, height));
		dataLabel.setMaximumSize(new Dimension(width, height));
		dataLabel.setPreferredSize(new Dimension(width, height));
	}

	add(dataLabel);

	add(Box.createRigidArea(new Dimension(5,0)));

    add(rightLabel = new JLabel(rightLabelText));

	if (fillStyle == FILL_STYLE.PUSH_LEFT){ add(Box.createHorizontalGlue()); }

	Font labelFont = createFont(fontSize, fontColor);

	leftLabel.setFont(labelFont);
	dataLabel.setFont(labelFont);
	rightLabel.setFont(labelFont);

}//end of LabelPanel::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// LabelPanel::createFont
//
// Creates font of pSize and pColor.
//

protected Font createFont(int pSize, Color pColor)
{

    HashMap<TextAttribute, Object> map = new HashMap<>();

    Font font = new Font("Dialog", Font.PLAIN, pSize);

    map.put(TextAttribute.FOREGROUND, pColor);

	return(font.deriveFont(map));

}// end of LabelPanel::createFont
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// LabelPanel::setEnabled
//
// Sets the enabled state for both the containing panel and the Label.
//

@Override
public void setEnabled(boolean pState)
{

	super.setEnabled(pState);

	dataLabel.setEnabled(pState);

}//end of LabelPanel::setEnabled
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// LabelPanel::setToolTipText
//
// Sets the tooltip for every GUI component in the panel to pText.
//

@Override
public void setToolTipText(String pText)
{

	super.setToolTipText(pText);

	leftLabel.setToolTipText(pText);
	dataLabel.setToolTipText(pText);
	rightLabel.setToolTipText(pText);

}//end of LabelPanel::setToolTipText
//-----------------------------------------------------------------------------


}//end of class LabelPanel
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
