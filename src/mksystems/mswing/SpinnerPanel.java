/******************************************************************************
* Title: SpinnerPanel.java
* Author: Mike Schoonover
* Date: 01/22/20
*
* Purpose:
*
* Creates a JPanel containing a description label, Spinner, and units label.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package mksystems.mswing;

import java.awt.Component;
import java.awt.event.MouseListener;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class SpinnerPanel
//
// Creates a JPanel with a description label, Spinner, and units label.
// The tooltip text for each label is set to the text passed for the label.
//

public class SpinnerPanel extends JPanel{

    protected JLabel descriptionLabel;
    public MFloatSpinner spinner;
    protected JLabel unitsLabel;

	protected double value;
	protected double min;
	protected double max;
	protected double increment;
	protected String formatPattern;
    protected int width;
	protected int height;
	protected String description;
	protected String units; String name;
	protected MouseListener mouseListener;
	
//-----------------------------------------------------------------------------
// SpinnerPanel::SpinnerPanel (constructor for doubles)
//
// Creates a MFloatSpinner for displaying float values.
//
// Both the containing panel and the TextField component will be assigned name of pName.
//
// pValue is the initial value to be displayed
// pMin is the minimum allowable value
// pMax is the maximum allowable value
// pIncrement is the amount the value is adjusted by use of the up/down arrows
// pFormatPattern is a format string specifiying how the float value is to be
//  displayed - number of digits before and after the decimal point, forced
//  forced zeroes, etc.  See help from Sun for class DecimalFormat.
//  Example formats: "#,##0.00" and "0.0000"
//
// pWidth is the user specified width for the edit box, set to -1 to use the
// default.
// pHeight is the user specified height for the edit box, set to -1 to use the
// default.
//
// See header notes at the top of the page for more info.
//

public SpinnerPanel(double pValue, double pMin,
                     double pMax, double pIncrement, String pFormatPattern,
                   int pWidth, int pHeight, String pDescription, String pUnits,
                   String pName, MouseListener pMouseListener)
{

	description = pDescription; value = pValue; min = pMin; max = pMax;
	increment = pIncrement; formatPattern = pFormatPattern;
	width = pWidth; height = pHeight; units = pUnits; name = pName;
	mouseListener = pMouseListener;

}//end of SpinnerPanel::SpinnerPanel (constructor for doubles)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpinnerPanel::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public void init()
{

	setName(name);

    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

    add(descriptionLabel = new JLabel(description));
    descriptionLabel.setToolTipText(description);

	add(Box.createHorizontalGlue());

    add (spinner = new MFloatSpinner(value, min, max, increment, formatPattern, width, height));

    spinner.setName(name);

    //watch for right mouse clicks
    setSpinnerNameAndMouseListener(spinner, spinner.getName(), mouseListener);

    add(unitsLabel = new JLabel(units));
    unitsLabel.setToolTipText(units);

}//end of SpinnerPanel::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpinnerPanel::setEnabled
//
// Sets the enabled state for both the containing panel and the Spinner.
//

@Override
public void setEnabled(boolean pState)
{

	super.setEnabled(pState);

	spinner.setEnabled(pState);

}//end of SpinnerPanel::setEnabled
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpinnerPanel::setSpinnerNameAndMouseListener
//
// A mouse listener cannot be added directly to a JSpinner or its sub-classes.
// The listener must be added to the text field inside the spinner to work
// properly.  This function also sets the name of the text field so that the
// mouse listener response method can use it.
//

private void setSpinnerNameAndMouseListener(JSpinner pSpinner, String pName,
                                                   MouseListener pMouseListener)
{

    for (Component child : pSpinner.getComponents()) {
        if (child instanceof JSpinner.NumberEditor) {
            for (Component child2 :
                  ((javax.swing.JSpinner.NumberEditor) child).getComponents()){
                if(pMouseListener != null) {
                          ((javax.swing.JFormattedTextField) child2).
                                               addMouseListener(pMouseListener);
                }
                ((javax.swing.JFormattedTextField) child2).setName(pName);
            }
        }
    }

}//end of SpinnerPanel::setSpinnerNameAndMouseListener
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SpinnerPanel::setToolTipText
//
// Sets the tooltip for every GUI component in the panel to pText.
//

@Override
public void setToolTipText(String pText)
{

	super.setToolTipText(pText);

	descriptionLabel.setToolTipText(pText);
	unitsLabel.setToolTipText(pText);
	spinner.setToolTipText(pText);

}//end of SpinnerPanel::setToolTipText
//-----------------------------------------------------------------------------

}//end of class SpinnerPanel
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
