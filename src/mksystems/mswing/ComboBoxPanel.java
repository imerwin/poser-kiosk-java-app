/******************************************************************************
* Title: ComboBoxPanel.java
* Author: Mike Schoonover
* Date: 01/25/20
*
* Purpose:
*
* Creates a JPanel containing a description label, JComboBox, and a trailing
* label to display units or other information.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package mksystems.mswing;

import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class ComboBoxPanel
//
// Creates a JPanel containing a description label, JComboBox, and a trailing
// label to display units or other information.
//
// The tooltip text for each label is set to the text passed for the label.
//

public class ComboBoxPanel extends JPanel{

    protected JLabel leftLabel;
	int leftLabelPadding;
    public JComboBox<String> comboBox;
    protected JLabel rightLabel;
	String name;
	int width, height;
	boolean editable;

	ArrayList<String> itemList;

	String leftLabelText, rightLabelText;

	protected ActionListener actionListener;
	String actionCommand;
	protected MouseListener mouseListener;

//-----------------------------------------------------------------------------
// ComboBoxPanel::ComboBoxPanel
//
/**
 * This class creates a JPanel containing a label, ComboBox, and a following label. Parameter
 * pLeftLabelText is often a description of the values while pRightLabelText is often
 * used to display the units (MHz, inches, feet, each, etc.)
 *
 * The list of items to be displayed is passed via pItemList.Both the containing panel and the
 * ComboBox component will be assigned name of pName.Parameters pWidth and pHeight set the
 * dimensions of the ComboBox...NOT the panel.The panel will end up being automatically sized to
 * hold the box and the labels.If pEditable is true, the value in the box can be edited by the
 * user.Otherwise, the user can only select entries in the list.The object designated to respond to
 * actions from the ComboBox is passed via pActionListener.The action command for the ComboBox is
 * passed via pActionCommand.The object designated to respond to mouse events from the ComboBox is
 * passed via pMouseListener.NOTE: The name of the ComboBoxPanel is set to pName.
 *
 * The name of the JComboBox in the panel is set to pName + "Component". Functions which search all
 * Components for the JComboBox should actually search for pName so that the ComboBoxPanel is found
 * instead. The JComboBox can then be accessed using:
 *
 *		{@literal JComboBox< String > comboBox = ((ComboBoxPanel)component).comboBox; }
 *
 * This is necessary because the Component cannot be cast to {@literal JComboBox< String > }
 * directly without an "unchecked cast" warning due to the nature of templates.
 *
 * @param pLeftLabelText	text for the label to be printed to the left of the ComboBox
 *							example: Speed
 * @param pLeftLabelPadding number of pixels to add after the label to adjust the alignment of the
 *							combo box
 * @param pRightLabelText	text for the label to be printed to the right of the ComobBox
 *							example: miles/hr
 * @param pItemList			the list of items to be used for the ComboBox's dropdown list
 * @param pName				the name to be used for the panel and for deriving the name of the
 *							ComboBox itself; the ComboBox's name is pName + "Component".
 *							This insures that the panel and the ComboBox have different names for
 *							search functions. The search functions will usually search for the
 *							panel name and then access the ComboBox via the panel. Accessing the
 *							ComboBox itself often results in "unchecked cast" warnings.
 * @param pEditable			selection can be changed if True; unchangeable if False
 * @param pWidth			the width in pixels of the panel
 * @param pHeight			the height in pixels of the panel
 * @param pActionCommand	the String to be used for the ActionCommand when responding to events
 * @param pActionListener	the object to call with ActionCommand when responding to events
 * @param pMouseListener	the object to call when responding to Mouse events
 *
 */

public ComboBoxPanel(String pLeftLabelText, int pLeftLabelPadding, String pRightLabelText,
								ArrayList<String> pItemList,
								String pName, boolean pEditable, int pWidth, int pHeight,
								 String pActionCommand, ActionListener pActionListener,
								MouseListener pMouseListener)
{

	leftLabelText = pLeftLabelText; leftLabelPadding = pLeftLabelPadding;
	rightLabelText = pRightLabelText; itemList = pItemList;
	name = pName; width = pWidth; height = pHeight; editable = pEditable;
	actionCommand = pActionCommand; actionListener = pActionListener;
	mouseListener = pMouseListener;

}//end of ComboBoxPanel::ComboBoxPanel (constructor for doubles)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ComboBoxPanel::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public void init()
{

	setName(name);

    setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));

    add(leftLabel = new JLabel(leftLabelText));
    leftLabel.setToolTipText(leftLabelText);

    add(Box.createRigidArea(new Dimension(leftLabelPadding,0)));

	comboBox = new JComboBox<>(itemList.toArray(new String[itemList.size()]));
    comboBox.setName(name + " Component");
	comboBox.setEditable(editable);
	comboBox.addActionListener(actionListener);
	comboBox.addMouseListener(mouseListener);
	comboBox.setMinimumSize(new Dimension(width, height));
	comboBox.setMaximumSize(new Dimension(width, height));
	comboBox.setPreferredSize(new Dimension(width, height));
	add(comboBox);

	add(Box.createRigidArea(new Dimension(5,0)));

    add(rightLabel = new JLabel(rightLabelText));
    rightLabel.setToolTipText(rightLabelText);

}//end of ComboBoxPanel::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ComboBoxPanel::setEnabled
//
// Sets the enabled state for both the containing panel and the ComboBox.
//

@Override
public void setEnabled(boolean pState)
{

	super.setEnabled(pState);

	comboBox.setEnabled(pState);

}//end of ComboBoxPanel::setEnabled
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ComboBoxPanel::setToolTipText
//
// Sets the tooltip for every GUI component in the panel to pText.
//

@Override
public void setToolTipText(String pText)
{

	super.setToolTipText(pText);

	leftLabel.setToolTipText(pText);
	rightLabel.setToolTipText(pText);
	comboBox.setToolTipText(pText);

}//end of ComboBoxPanel::setToolTipText
//-----------------------------------------------------------------------------

}//end of class ComboBoxPanel
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
