/******************************************************************************
* Title: PersistentDataElementString.java
* Author: Mike Schoonover
* Date: 01/25/20
*
* Purpose:
*
* PersistentDataElement subclass which handles Strings meant to be modified, applied, saved/loaded
* from persistent storage in a threadsafe manner in an MVC structure.
*
* See notes at the top of PersistentDataElement for details.
*
* See notes at the top of PersistentDataSet for details regarding the methodology for using these
* subclasses and adding them to a PersistentDataSet container.
* 
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

//-----------------------------------------------------------------------------

import inifile.IniFile;

//-----------------------------------------------------------------------------
// class PersistentDataElementString
//
// This class handles data which is updated, applied, loaded and saved from persistent media.
//
// Refer to the notes at the top of this page for more details.
//

public class PersistentDataElementString<tType> extends PersistentDataElement<tType>{

//-----------------------------------------------------------------------------
// PersistentDataElementString::PersistentDataElementString
//
// The pName parameter is used to associate the element with a corresponding GUI component with
// the same name.
//
// The pTag is used as the Key of a Key/Value pair when the save function is called:
//
//		pTag = "COM port", value = "COM1" -> COM port=COM1 in the file
//
// Parameter pContainer specifies a PersistentDataSet container to which the element is to be
// added unless the parameter is null. The element will add itself to the container.
//

public PersistentDataElementString(String pName, String pTag, PersistentDataSet pContainer)
{

	super(pName, pTag, pContainer);

}//end of PersistentDataElementString::PersistentDataElementString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

@Override
public synchronized void init()
{

	super.init();

}//end of PersistentDataElementString::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::loadKeyValuePair
//
// Loads the data element to the IniFile object pIniFile as a Key/Value pair.
//
// The element's tag will be used as the Key.
//
// If a matching section/tag is not found, then the element's value will remain unchanged. Thus to
// specify a default value, set the element's value to that default before calling this function.
//
// pSection will be used as the section name. If pSection == null and container != null, then the
// container's tag will be used as the section name. If both are null, then "General" will be used
// as the section name.
//
// @SuppressWarnings("unchecked") must be invoked...seems to be no way around it.
//

@Override
@SuppressWarnings("unchecked")
protected synchronized void loadKeyValuePair(IniFile pIniFile, String pSection)
{

	super.loadKeyValuePair(pIniFile, pSection);

	value = (tType) pIniFile.readString(generateSectionName(pSection), tag, (String)value);

}//end of PersistentDataElementString::loadKeyValuePair
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::saveKeyValuePair
//
// Saves the data element to the IniFile object pIniFile as a Key/Value pair with specified
// formatting.
//
// The element's tag will be used as the Key.
// 
// pSection will be used as the section name. If pSection == null and container != null, then the
// container's tag will be used as the section name. If both are null, then "General" will be used
// as the section name.
//

@Override
protected synchronized void saveKeyValuePair(IniFile pIniFile, String pSection,
																boolean pClearContainerNotSavedFlag)
{

	super.saveKeyValuePair(pIniFile, pSection, pClearContainerNotSavedFlag);

	pIniFile.writeString(generateSectionName(pSection), tag, (String)value);

}//end of PersistentDataElementString::saveKeyValuePair
//-----------------------------------------------------------------------------


}//end of class PersistentDataElementString
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
