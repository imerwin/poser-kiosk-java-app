/******************************************************************************
* Title: PersistentDataSet.java
* Author: Mike Schoonover
* Date: 01/25/20
*
* Purpose:
*
* Provides a Container for a set of PersistentDataElements. This is NOT a superclass for
* PersistentDataElements...it merely manages a collection of those objects.
*
* When each Element is added to an instance of this class (the Container), it is given a reference
* back to that Container object so the Element can set the Container's changed and notSaved flags
* when the Element itself is modified. Thus, an outside object can query the container to quickly
* determine if one or more Elements need to be applied or saved without iterating over the entire
* list.
*
* The changed and notSaved flags are separate because values are often applied immediately upon
* change but it is not always prudent to save to persistent storage every single time such a value
* is changed. The saving might only occur when the User clicks Save, closes a Window, exits the
* program, or data might be saved periodically based on a timer.
*
* All functions which get or set the changed and notSaved flags are synchronized so that multiple
* threads can safely change, apply, or save values. Values are often changed by the system GUI
* thread, sent to a remote device by another thread in order to control the rate of update, and
* saved to disk by a third thread to avoid freezing the GUI if the save process is time intensive.
*
* Using a separate thread for updating a remote device also solves the problem of flooding the
* device with useless intermittent data updates which may overrun transmission buffers or otherwise
* result in lost data. If the Update thread is configured to check the changed flags and send
* updates every .25 seconds, then the transmission rate will be reasonable. Meanwhile, if the GUI
* thread updates the value hundreds of times between transmissions, the intermittent values
* will never be sent...only the value present at the end of each .25 second period will actually be
* sent.
*
* Many other functions of this class are synchronized as well to allow for unforeseen usage cases
* where this might be necessary. For most situations, updating, applying, and saving of persistent
* data are not time critical or time intensive operations, thus the synchronization overhead is
* negligible.
*
* Any operation which changes the changed or notSaved flags must apply or save data from ALL
* Elements before clearing the respective flags. Thus during apply or save operations, all other
* get/set functions will be blocked. The saveAll function should be used which clears the
* notSaved flag after all data has been saved. For updating or saving individual elements, each
* Element should be handled and cleared individually by outside objects; each Element will notify
* its Container object that its changed or notSaved flag has been cleared and the Container will
* only clear its corresponding flag if ALL Elements in the set have cleared flags.
*
* Connecting to GUI Components
*
* Persistent data Elements are often displayed and adjusted via GUI components. To simplify the
* connection between the two while maintaining MVC structure, the name parameter of the component
* (such as provided for Java GUI components) can be set to match the name of the persistent data
* Element. When a value in a GUI component is changed by the user, the Controller object can
* respond to the change by searching for a data Element whose name matches the GUI component's
* name. The value can then be transferred accordingly.
*
* To update all Components from Elements or vice versa, the Controller can iterate through all the
* child Components of a GUI container (such as a Panel) and find the matching named Element to
* transfer the values back and forth between the appropriate objects.
*
* This methodology makes adding new Element/Component pairs more efficient. On the View side, the
* GUI component is added with an appropriate name. On the Model side, the data Element is added
* of the proper type and with a matching name. There is no need to actually connect the Component
* to the Element since the Controller does this automatically via their names.
*
* Loading and saving occur automatically if the Elements are added to a container. The Controller
* can save and load all elements in the Container using their tag values as keys for key/value
* pairs and the Container's name as the section header.
*
*
* Class Overview
*
*	PersistentDataSet (a collection of PersistentDataElement subclasses)
*
*		contains several of:
*
*	PersistentDataElement (parent class of various types of persistent data objects)
*		PersistentDataElement<Integer>	(subclass)
*		PersistentDataElement<Double>	(subclass)
*		PersistentDataElement<String>	(subclass)
*		PersistentDataElement<Boolean>	(subclass)
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

import inifile.IniFile;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class PersistentDataSet
//
// This class creates a container for a set of PersistentDataElements.
// Each element has a pointer back to this container so each can set data changed and not-saved
// flags when the element is modified.
//
// Refer to the notes at the top of this page for more details.
//

public class PersistentDataSet{


	HashMap<String, PersistentDataElement>elements;
	public synchronized HashMap<String, PersistentDataElement> getElements(){ return elements; }

	protected String name;
	public synchronized String getName(){ return name; }

	protected String tag;
	public synchronized String getTag(){ return tag; }

	protected boolean changed;
	public synchronized boolean getChanged(){ return changed; }
	//NOTE -- requestSetChanged should generally be used instead! See notes in that method header.
	public synchronized void setChanged(boolean pChanged){ changed = pChanged; }

	protected boolean notSaved;
	public synchronized boolean getNotSaved(){ return notSaved; }
	//NOTE -- requestSetNotSaved should generally be used instead! See notes in taht method header.
	public synchronized void setNotSaved(boolean pNotSaved){
		notSaved = pNotSaved; }

//-----------------------------------------------------------------------------
// PersistentDataSet::PersistentDataSet
//
// The pName parameter allows the Container to be named for any purpose.
//
// The pTag can be used as a section header name in an inifile format, such
//	as:
//
//		pTag = "User Settings" -> [User Device Settings] in the file
//
// The use of pTag for this purpose must be handled by an outside object, i.e. calling getTag and
// then saving it to the file as a section header [abcdef] where "abcdef" is the contents of
// Tag. The saveAll function of this class can then be called to store each Element after the
// section header. Alternatively, the outside object may use an entirely different methodology to
// create a section header or forgo the use of one altogether.
//

public PersistentDataSet(String pName, String pTag)
{

	name = pName; tag = pTag;

}//end of PersistentDataSet::PersistentDataSet
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public synchronized void init()
{

	elements = new HashMap<>(10);

	setChanged(false); setNotSaved(false);

}//end of PersistentDataSet::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::requestSetChanged
//
// Requests to set the changed flag.
//
// If pState is true, the flag is always set true.
//
// If pState is false, the flag is set false ONLY if all elements in the elements list have their
// corresponding flags set false.
//
// This object's (the Container) flag should always be true if any element in the list has a true
// flag.
//

public synchronized void requestSetChanged(boolean pState)
{

	if (pState){ changed = true; return; }

	boolean anyElementChanged = false;

	for(HashMap.Entry entry : elements.entrySet()){

		boolean elementChanged = ((PersistentDataElement)(entry.getValue())).getChanged();

		if (anyElementChanged = elementChanged){ break; }
	}

	setChanged(anyElementChanged);

}//end of PersistentDataSet::requestSetChanged
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::requestSetNotSaved
//
// Requests to set the notSaved flag.
//
// If pState is true, the flag is always set true.
//
// If pState is false, the flag is set false ONLY if all elements in the elements list have their
// corresponding flags set false.
//
// This object's (the Container) flag should always be true if any element in the list has a true
// flag.
//

public synchronized void requestSetNotSaved(boolean pState)
{

	if (pState){ setNotSaved(true); return; }

	boolean anyElementNotSaved = false;

	for(HashMap.Entry entry : elements.entrySet()){

		boolean elementNotSaved = ((PersistentDataElement)(entry.getValue())).getNotSaved();

		if (anyElementNotSaved = elementNotSaved){ break; }
	}

	setNotSaved(anyElementNotSaved);

}//end of PersistentDataSet::requestSetNotSaved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::addElement
//
// Adds a PersistentDataElement subclass to the elements HashMap.
//
// The elements name is stored as the key while the element is stored as the value.
//

public synchronized void addElement(PersistentDataElement pElement)
{

	elements.put(pElement.getName(), pElement);

}//end of PersistentDataSet::addElement
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::getElementByName
//
// Returns PersistentDataElement from the HashMap with name pName.
//
// Returns null if pName not present in the elements list.
//

public synchronized PersistentDataElement getElementByName(String pName)
{

	return(elements.get(pName));

}//end of PersistentDataSet::getElementByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::getStringValueByName
//
// Returns String value of PersistentDataElement with name pName stored in the HashMap.
//
// If pClearChanged is true, the changed flag will be set false and if there is a container, it
// will be requested to set its flag false (only allowed if all elements in container have cleared
// changed flags).
//
// Returns null if a PersistentDataElement with pName is not present in the elements list.
//

public synchronized String getStringValueByName(String pName, boolean pClearChanged)
{

	return((String)elements.get(pName).getValue(pClearChanged));

}//end of PersistentDataSet::getStringValueByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::getBooleanValueByName
//
// Returns Boolean value of PersistentDataElement with name pName stored in the HashMap.
//
// If pClearChanged is true, the changed flag will be set false and if there is a container, it
// will be requested to set its flag false (only allowed if all elements in container have cleared
// changed flags).
//
// Returns null if a PersistentDataElement with pName is not present in the elements list.
//

public synchronized Boolean getBooleanValueByName(String pName, boolean pClearChanged)
{

	return((Boolean)elements.get(pName).getValue(pClearChanged));

}//end of PersistentDataSet::getBooleanValueByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::getIntegerValueByName
//
// Returns Integer value of PersistentDataElement with name pName stored in the HashMap.
//
// If pClearChanged is true, the changed flag will be set false and if there is a container, it
// will be requested to set its flag false (only allowed if all elements in container have cleared
// changed flags).
//
// Returns null if a PersistentDataElement with pName is not present in the elements list.
//

public synchronized Integer getIntegerValueByName(String pName, boolean pClearChanged)
{

	return((Integer)elements.get(pName).getValue(pClearChanged));

}//end of PersistentDataSet::getIntegerValueByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::setStringValueByName
//
// Sets String value to pValue of PersistentDataElement with name pName stored in the HashMap.
//
// Having a specific function which accepts a String allows the calling object to set the value
// without an "unchecked" warning.
//
// To force the changed/notSaved flags to be set true, pass pForceChanged as true.
//
// Does nothing if a PersistentDataElement with pName is not present in the elements list.
//

public synchronized void setStringValueByName(String pName, String pValue, boolean pForceChanged)
{

	PersistentDataElement element = elements.get(pName);

	if(element != null){ element.setStringValue(pValue, pForceChanged); }

}//end of PersistentDataSet::setStringValueByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::setDoubleValueByName
//
// Sets Double value to pValue of PersistentDataElement with name pName stored in the HashMap.
//
// Having a specific function which accepts a Double allows the calling object to set the value
// without an "unchecked" warning.
//
// To force the changed/notSaved flags to be set true, pass pForceChanged as true.
//
// Does nothing if a PersistentDataElement with pName is not present in the elements list.
//

public synchronized void setDoubleValueByName(String pName, Double pValue, boolean pForceChanged)
{

	PersistentDataElement element = elements.get(pName);

	if(element != null){ element.setDoubleValue(pValue, pForceChanged); }

}//end of PersistentDataSet::setDoubleValueByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::setIntegerValueByName
//
// Sets Integer value to pValue of PersistentDataElement with name pName stored in the HashMap.
//
// Having a specific function which accepts an Integer allows the calling object to set the value
// without an "unchecked" warning.
//
// To force the changed/notSaved flags to be set true, pass pForceChanged as true.
//
// Does nothing if a PersistentDataElement with pName is not present in the elements list.
//

public synchronized void setIntegerValueByName(String pName, Integer pValue, boolean pForceChanged)
{

	PersistentDataElement element = elements.get(pName);

	if(element != null){ element.setIntegerValue(pValue, pForceChanged); }

}//end of PersistentDataSet::setIntegerValueByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::setBooleanValueByName
//
// Sets Boolean value to pValue of PersistentDataElement with name pName stored in the HashMap.
//
// Having a specific function which accepts a Boolean allows the calling object to set the value
// without an "unchecked" warning.
//
// To force the changed/notSaved flags to be set true, pass pForceChanged as true.
//
// Does nothing if a PersistentDataElement with pName is not present in the elements list.
//

public synchronized void setBooleanValueByName(String pName, Boolean pValue, boolean pForceChanged)
{

	PersistentDataElement element = elements.get(pName);

	if(element != null){ element.setBooleanValue(pValue, pForceChanged); }

}//end of PersistentDataSet::setBooleanValueByName
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------
// PersistentDataSet::loadFromFile
//
// Loads all data from file pFilename.
//

public void loadFromFile(String pFilename)
{

	IniFile configFile;

	//if the ini file cannot be opened and loaded, exit without action
	try {
		configFile = new IniFile(pFilename, "UTF-8");
		configFile.init();
	}
	catch(IOException e){
		logSevere(e.getMessage() + " - Error: 429");
		return;
	}

	loadAll(configFile);

}//end of PersistentDataSet::loadFromFile
//--------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::loadAll
//
// Loads all data elements in the list from the IniFile object pIniFile.
//
// The notSaved flag for each element and this container object will be set false.
// The changed flags for each element and this container object will be set true.
//

public synchronized void loadAll(IniFile pIniFile)
{

	for(HashMap.Entry entry : elements.entrySet()){

		((PersistentDataElement)(entry.getValue())).loadKeyValuePair(pIniFile, null);

	}

	setChanged(true);
	setNotSaved(false);

}//end of PersistentDataSet::loadAll
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::getListOfAllSettingValues
//
/**
 * Returns an ArrayList of Strings with each key/value pair from all PersistentDataElements in the
 * set.
 *
 *
 * @return	ArrayList of Strings with each key/value pair from all PersistentDataElements in the
 *			set.
 *
 */

public ArrayList<String> getListOfAllSettingValues()
{

	ArrayList<String> list = new ArrayList<>();

	for(HashMap.Entry entry : elements.entrySet()){

		PersistentDataElement element = ((PersistentDataElement) (entry.getValue()));

		String value = element.getName()
					+ "\n\t" + element.getTag()
					+ "\n\t" + element.getValueAsString();

		list.add(value);

	}

	return(list);

}//end of PersistentDataSet::getListOfAllSettingValues
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::saveAll
//
// Saves all data elements in the list to the IniFile object pIniFile.
//
// The notSaved flag for each element and this container object will be cleared.
// The changed flags will not be modified.
//
// The pClearContainerNotSavedFlag parameter is passed as false so each element does not waste
// time trying to clear this Container's notSaved flag. This function will handle the clearing
// one time after all the Elements have been saved. This is only necessary for clearing this
// flag as the Container must iterate through its elements list for every call to make sure it is
// okay to clear its notSaved flag.
//

public synchronized void saveAll(IniFile pIniFile)
{

	for(HashMap.Entry entry : elements.entrySet()){

		((PersistentDataElement) (entry.getValue())).saveKeyValuePair(pIniFile, null, false);

	}

	setNotSaved(false);

}//end of PersistentDataSet::saveAll
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of PersistentDataSet::logSevere
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataSet::logStackTrace
//
// Logs stack trace info for exception pE with pMessage at level SEVERE using
// the Java logger.
//

void logStackTrace(String pMessage, Exception pE)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage, pE);

}//end of PersistentDataSet::logStackTrace
//-----------------------------------------------------------------------------

}//end of class PersistentDataSet
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
