/******************************************************************************
* Title: PersistentDataElement.java
* Author: Mike Schoonover
* Date: 01/25/20
*
* Purpose:
*
* Superclass for subclasses which handle data meant to be modified, applied, saved/loaded from
* persistent storage in a threadsafe manner in an MVC structure.
*
* The Element is named to match the name of a corresponding GUI component so that the Controller
* can pair the two for transferring data.
*
* The tag value is used as the Key in a Key/Value pair when saving the Element's value to
* persistent storage.
*
* The name is separate from the tag so that the name may include special phrasing such as:
*		COMBOBOX ~ COM Port Selector
* In this case the name would not make a good Key; instead the tag might be "COM port" so that in
* the data file it reads:
*		COM port=COM1
*
* See notes at the top of PersistentDataSet for details regarding the methodology for using these
* subclasses and adding them to a PersistentDataSet container.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

//-----------------------------------------------------------------------------

import inifile.IniFile;

//-----------------------------------------------------------------------------
// class PersistentDataElement
//
// This class handles data which is updated, applied, loaded and saved from persistent media.
//
// Refer to the notes at the top of this page for more details.
//

public class PersistentDataElement<tType>{


	tType value;

	public tType dummyToAllowTypeDetection;

	protected String name;

	protected String tag;

	PersistentDataSet container;
	public synchronized PersistentDataSet getContainer(){ return container; }

	protected boolean changed;
	public synchronized boolean getChanged(){ return changed; }
	//NOTE -- applyChange should usually be used instead of setChanged to modify flag!
	public synchronized void setChanged(boolean pChanged){ changed = pChanged; }

	protected boolean notSaved;
	public synchronized boolean getNotSaved(){ return notSaved; }
	//NOTE -- save method should usually be used instead of setNotSaved to modify flag!
	public synchronized void setNotSaved(boolean pNotSaved){ notSaved = pNotSaved; }

//-----------------------------------------------------------------------------
// PersistentDataElement::PersistentDataElement
//
// The pName parameter is used to associate the element with a corresponding GUI component with
// the same name.
//
// The pTag is used as the Key of a Key/Value pair when the save function is called:
//
//		pTag = "COM port", value = "COM1" -> COM port=COM1 in the file
//
// Parameter pContainer specifies a PersistentDataSet container to which the element is to be
// added unless the parameter is null. The element will add itself to the container.
//

public PersistentDataElement(String pName, String pTag, PersistentDataSet pContainer)
{

	name = pName; tag = pTag; container = pContainer;

}//end of PersistentDataElement::PersistentDataElement
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElement::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public synchronized void init()
{

	if(container != null){ container.addElement(this); }

}//end of PersistentDataElement::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElement::getName
//
/**
 * Returns the element's name which is used to associate it with a GUI element.
 *
 * @return		the element's name which is used to associate it with a GUI element
 *
 */

public synchronized String getName()
{

	return(name);

}//end of PersistentDataElement::getName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElement::getTag
//
/**
 * Returns the element's tag which is used as the key when storing as a key/value pair.
 *
 * @return		the element's tag which is used as the key when storing as a key/value pair
 *
 */

public synchronized String getTag()
{

	return(tag);

}//end of PersistentDataElement::getTag
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElement::getValueAsString
//
/**
 * Returns the value as a String, regardless of its actual type.
 *
 * Does not change the state of  the changed flag, the container's changed flag, the notSaved flag
 * or the container's notSaved flag.
 *
 * @return		the value as a String
 *
 */

public synchronized String getValueAsString()
{

	return("" + value);

}//end of PersistentDataElement::getValueAsString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::setValue
//
// Stores pValue in the value variable.
//
// Will set the changed flag and the container's changed flag if container
// is present and IF the new value is different than the value already stored.
//
// Will set the notSaved flag and the container's notSaved flag if container
// is present and IF the new value is different than the value already stored.
//
// To force the changed/notSaved flags to be set true, pass pForceChanged as true.
//
// Note that .equals must be used to compare new and old values. Using the != operator will
// compare the reference addresses of the objects instead of their value.
//

public synchronized void setValue(tType pValue, boolean pForceChanged)
{

	if(pForceChanged || !pValue.equals(value)){
		changed = true;
		if(container != null) { container.requestSetChanged(true); }
		notSaved = true;
		if(container != null) { container.requestSetNotSaved(true); }
	}

	value = pValue; dummyToAllowTypeDetection = pValue;

}//end of PersistentDataSet::setValue
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::setDoubleValue
//
// Stores Double type pValue in the value variable.
//
// Will set the changed flag and the container's changed flag if container
// is present and IF the new value is different than the value already stored.
//
// Will set the notSaved flag and the container's notSaved flag if container
// is present and IF the new value is different than the value already stored.
//
// To force the changed/notSaved flags to be set true, pass pForceChanged as true.
//
// Note that .equals must be used to compare new and old values. Using the != operator will
// compare the reference addresses of the objects instead of their value.
//
// Having a specific function which accepts a Double allows the calling object to set the value
// without an "unchecked" warning. The "unchecked" is suppressed here instead of repeatedly in the
// client code.
//

@SuppressWarnings("unchecked")
public synchronized void setDoubleValue(Double pValue, boolean pForceChanged)
{

	if(pForceChanged || !pValue.equals(value)){
		changed = true;
		if(container != null) { container.requestSetChanged(true); }
		notSaved = true;
		if(container != null) { container.requestSetNotSaved(true); }
	}

	value = (tType)pValue; dummyToAllowTypeDetection = (tType)pValue;

}//end of PersistentDataSet::setDoubleValue
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::setIntegerValue
//
// Stores Integer type pValue in the value variable.
//
// Will set the changed flag and the container's changed flag if container
// is present and IF the new value is different than the value already stored.
//
// Will set the notSaved flag and the container's notSaved flag if container
// is present and IF the new value is different than the value already stored.
//
// To force the changed/notSaved flags to be set true, pass pForceChanged as true.
//
// Note that .equals must be used to compare new and old values. Using the != operator will
// compare the reference addresses of the objects instead of their value.
//
// Having a specific function which accepts an Integer allows the calling object to set the value
// without an "unchecked" warning. The "unchecked" is suppressed here instead of repeatedly in the
// client code.
//

@SuppressWarnings("unchecked")
public synchronized void setIntegerValue(Integer pValue, boolean pForceChanged)
{

	if(pForceChanged || !pValue.equals(value)){
		changed = true;
		if(container != null) { container.requestSetChanged(true); }
		notSaved = true;
		if(container != null) { container.requestSetNotSaved(true); }
	}

	value = (tType)pValue; dummyToAllowTypeDetection = (tType)pValue;

}//end of PersistentDataSet::setIntegerValue
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::setBooleanValue
//
// Stores Boolean type pValue in the value variable.
//
// Will set the changed flag and the container's changed flag if container
// is present and IF the new value is different than the value already stored.
//
// Will set the notSaved flag and the container's notSaved flag if container
// is present and IF the new value is different than the value already stored.
//
// To force the changed/notSaved flags to be set true, pass pForceChanged as true.
//
// Note that .equals must be used to compare new and old values. Using the != operator will
// compare the reference addresses of the objects instead of their value.
//
// Having a specific function which accepts a Boolean allows the calling object to set the value
// without an "unchecked" warning. The "unchecked" is suppressed here instead of repeatedly in the
// client code.
//

@SuppressWarnings("unchecked")
public synchronized void setBooleanValue(Boolean pValue, boolean pForceChanged)
{

	if(pForceChanged || !pValue.equals(value)){
		changed = true;
		if(container != null) { container.requestSetChanged(true); }
		notSaved = true;
		if(container != null) { container.requestSetNotSaved(true); }
	}

	value = (tType)pValue; dummyToAllowTypeDetection = (tType)pValue;

}//end of PersistentDataSet::setBooleanValue
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::setStringValue
//
// Stores String type pValue in the value variable.
//
// Will set the changed flag and the container's changed flag if container
// is present and IF the new value is different than the value already stored.
//
// Will set the notSaved flag and the container's notSaved flag if container
// is present and IF the new value is different than the value already stored.
//
// To force the changed/notSaved flags to be set true, pass pForceChanged as true.
//
// Note that .equals must be used to compare new and old values. Using the != operator will
// compare the reference addresses of the objects instead of their value.
//
// Having a specific function which accepts a String allows the calling object to set the value
// without an "unchecked" warning. The "unchecked" is suppressed here instead of repeatedly in the
// client code.
//

@SuppressWarnings("unchecked")
public synchronized void setStringValue(String pValue, boolean pForceChanged)
{

	if(pForceChanged || !pValue.equals(value)){
		changed = true;
		if(container != null) { container.requestSetChanged(true); }
		notSaved = true;
		if(container != null) { container.requestSetNotSaved(true); }
	}

	value = (tType)pValue; dummyToAllowTypeDetection = (tType)pValue;

}//end of PersistentDataSet::setStringValue
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::getValue
//
// Returns value variable.
//
// If pClearChanged is true, the changed flag will be set false and if there is a container, it
// will be requested to set its flag false (only allowed if all elements in container have cleared
// changed flags).
//

public synchronized tType getValue(boolean pClearChanged)
{

	if(pClearChanged){
		changed = false;
		if(container != null) { container.requestSetChanged(false); }
	}

	return(value);

}//end of PersistentDataSet::getValue
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::loadKeyValuePair
//
// Loads the data element from the IniFile object pIniFile as a Key/Value pair.
//
// The element's tag will be used as the Key, pSection will be used as the section name.
// If pSection == null and container != null, then the container's tag will be used as the section
// name. If both are null, then "General" will be used as the section name.
//
// If a matching section/tag is not found, then the element's value will remain unchanged. Thus to
// specify a default value, set the element's value to that default before calling this function.
//
// The notSaved flag for each element will be set false.
// The changed flags for each element and the container object's flag will be set true.
//
// This method should be overridden by subclasses to provide type specific behavior.
//

protected synchronized void loadKeyValuePair(IniFile pIniFile, String pSection)
{

	setChanged(true);
	setNotSaved(false);

	if(container != null){ container.requestSetChanged(true); }

}//end of PersistentDataSet::loadKeyValuePair
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::saveKeyValuePair
//
// Saves the data element to the IniFile object pIniFile as a Key/Value pair with specified
// formatting.
//
// The element's tag will be used as the Key, pSection will be used as the section name.
// If pSection == null and container != null, then the container's tag will be used as the section
// name. If both are null, then "General" will be used as the section name.
//
// The notSaved flag for the element will be set false
// The changed flags will not be modified.
//
// If pClearContainerNotSavedFlag is true and if there is a container, then the Container to which
// the element belongs will be called to clear its notSaved flag if appropriate (all elements in
// the container have cleared flags).
//
// This method should be overridden by subclasses to provide type specific behavior.
//

protected synchronized void saveKeyValuePair(IniFile pIniFile, String pSection,
																boolean pClearContainerNotSavedFlag)
{

	setNotSaved(false);

	if(pClearContainerNotSavedFlag && container != null){ container.requestSetNotSaved(false); }

}//end of PersistentDataSet::saveKeyValuePair
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PersistentDataElementString::generateSectionName
//
// Generates the name for a section in the data file based on pSection, the element tag, and
// the container's tag.
//
// pSection will be used as the section name. If pSection == null and container != null, then the
// container's tag will be used as the section name. If both are null, then "General" will be used
// as the section name.
//

protected synchronized String generateSectionName(String pSection)
{

	String section;

	if (pSection == null){
		if(container != null){
			section = container.getTag();
		} else {
			section = "General";
		}
	} else {
		section = pSection;
	}

	return(section);

}//end of PersistentDataSet::generateSectionName
//-----------------------------------------------------------------------------


}//end of class PersistentDataElement
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
