/******************************************************************************
* Title: HardwareProfile.java
* Author: Mike Schoonover
* Date: 02/13/20
*
* Purpose:
*
* Provides a Container for a set of PersistentDataElements. It extends PersistentDataSet
* in order to provide a convenient set to hold the elements in this same class.
*
* The settings in this class are Hardware Profile configuration settings...they are loaded on
* startup and do not change and are never saved back to file.
*
* These settings are specific to each machine the code is installed on, such as the Robot itself,
* the Debug Computer, the Remote Control computer, etc.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

//-----------------------------------------------------------------------------

import inifile.IniFile;
import java.io.File;

// class HardwareProfile
//
// Refer to the notes at the top of this page for more details.
//

public class HardwareProfile extends PersistentDataSet{

//-----------------------------------------------------------------------------
// HardwareProfile::HardwareProfile
//
/**
 *
 *
 * The pName parameter allows the Container to be named for any purpose.
 *
 * The pTag can be used as a section header name in an inifile format, such
 *	as:
 *
 *		pTag = "Static Settings" ~ [Static Device Settings] in the file
 *
 * See parent class for details.
 *
 * @param pName		a name for the container which can be used for any purpose
 * @param pTag		section name in the config file from which settings are to be accessed
 *
 */

public HardwareProfile(String pName, String pTag)
{

	super(pName, pTag);

}//end of HardwareProfile::HardwareProfile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// HardwareProfile::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

@Override
public synchronized void init()
{

	super.init();

	createPersistentDataSet();

}//end of HardwareProfile::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// HardwareProfile::createPersistentDataSet
//
// Creates the persistent data elements and specifieds that this object is also the
// PersistentDataSet to which they belong.
//
// Persistent data elements provide threadsafe variables meant to be adjusted,
// applied, and saved to persistent storage media. (user settings, system settings, etc.)
//
// The default values for each variable should be set here. If a matching tag is not found when
// values are loaded from file, the default will remain in place.
//

protected void createPersistentDataSet(){


	PersistentDataElementString<String> elementString;
//	PersistentDataElementBoolean<Boolean> elementBoolean;
	PersistentDataElementInteger<Integer> elementInteger;

	elementString =
				new PersistentDataElementString<>("Machine Name", "Machine Name", this);
	elementString.init(); elementString.setValue("Unkown Machine", true);

	elementString =
				new PersistentDataElementString<>("Operating System", "Operating System", this);
	elementString.init(); elementString.setValue("Unkown Operating System", true);

	elementString = new PersistentDataElementString<>(
				"Pose Recognizer Server IP Address", "Pose Recognizer Server IP address", this);
	elementString.init(); elementString.setValue("unknown", true);

	elementInteger = new PersistentDataElementInteger<>(
							"Pose Recognizer Server Port", "Pose Recognizer Server port", this);
	elementInteger.init(); elementInteger.setValue(4243, true);

}// end of HardwareProfile::createPersistentDataSet
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// HardwareProfile::loadAll
//
// Overrides to provide custom processing of data loaded from file.
//

@Override
public synchronized void loadAll(IniFile pIniFile)
{

	super.loadAll(pIniFile);

}//end of HardwareProfile::loadAll
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// HardwareProfile::formatFilePaths
//
// Formats a file path to use proper file separator, etc. If the path does not end with a file
// separator, one is appended.
//
// The data element's name for the String to be formatted should be passed via pName. The client
// code is responsible for ensuring that pName refers to a file path.
//

protected void formatFilePaths(String pName)
{

    String sep = File.separator;

	String path = getStringValueByName(pName, false);

    path = path.replace("/", sep);
    path = path.replace("\\", sep);

	//add a separator if not one already at the end
    if (!path.endsWith(File.separator)) {path += File.separator;}

	setStringValueByName(pName, path, true);

}//end of HardwareProfile::formatFilePaths
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// HardwareProfile::saveAll
//
// Overrides super.saveAll so that settings cannot be saved back to file as that operation is not
// allowed for this data set.
//

@Override
public synchronized void saveAll(IniFile pIniFile)
{

	//do nothing...saving static settings to file is not allowed

}//end of HardwareProfile::saveAll
//-----------------------------------------------------------------------------


}//end of class HardwareProfile
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
