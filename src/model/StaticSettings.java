/******************************************************************************
* Title: StaticSettings.java
* Author: Mike Schoonover
* Date: 02/13/20
*
* Purpose:
*
* Provides a Container for a set of PersistentDataElements. It extends PersistentDataSet
* in order to provide a convenient set to hold the elements in this same class.
*
* The settings in this class are static configuration settings...they are loaded on startup and do
* not change and are never saved back to file.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

//-----------------------------------------------------------------------------

import inifile.IniFile;
import java.io.File;

// class StaticSettings
//
// Refer to the notes at the top of this page for more details.
//

public class StaticSettings extends PersistentDataSet{




//-----------------------------------------------------------------------------
// StaticSettings::StaticSettings
//
// The pName parameter allows the Container to be named for any purpose.
//
// The pTag can be used as a section header name in an inifile format, such
//	as:
//
//		pTag = "Static Settings" -> [Static Device Settings] in the file
//
// See parent class for details.
//

public StaticSettings(String pName, String pTag)
{

	super(pName, pTag);

}//end of StaticSettings::StaticSettings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// StaticSettings::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

@Override
public synchronized void init()
{

	super.init();

	createPersistentDataSet();

}//end of StaticSettings::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// StaticSettings::createPersistentDataSet
//
// Creates the persistent data elements and specifieds that this object is also the
// PersistentDataSet to which they belong.
//
// Persistent data elements provide threadsafe variables meant to be adjusted,
// applied, and saved to persistent storage media. (user settings, system settings, etc.)
//
// The default values for each variable should be set here. If a matching tag is not found when
// values are loaded from file, the default will remain in place.
//

protected void createPersistentDataSet(){


	PersistentDataElementString<String> elementString;
	PersistentDataElementBoolean<Boolean> elementBoolean;
	PersistentDataElementInteger<Integer> elementInteger;

	elementString =
				new PersistentDataElementString<>("User Name", "User Name", this);
	elementString.init(); elementString.setValue("Zaphod", true);

	elementBoolean =
				new PersistentDataElementBoolean<>("Simulation Mode", "Simulation Mode", this);
	elementBoolean.init(); elementBoolean.setValue(false, true);

	elementString = new PersistentDataElementString<>(
									"Primary Data Folder Name", "Primary Data Folder Name", this);
	elementString.init(); elementString.setValue("", true);

	elementString = new PersistentDataElementString<>(
									"Backup Data Folder Name", "Backup Data Folder Name", this);
	elementString.init(); elementString.setValue("", true);

	elementString = new PersistentDataElementString<>(
	"Primary Data Folder Name for Setup Wizard", "Primary Data Folder Name for Setup Wizard", this);
	elementString.init(); elementString.setValue("", true);

	elementString = new PersistentDataElementString<>(
	  "Backup Data Folder Name for Setup Wizard", "Backup Data Folder Name for Setup Wizard", this);
	elementString.init(); elementString.setValue("", true);

	elementString = new PersistentDataElementString<>(
												"Reports Folder Name", "Reports Folder Name", this);
	elementString.init(); elementString.setValue("", true);

	elementString = new PersistentDataElementString<>(
								"Default Printer for Reports", "Default Printer for Reports", this);
	elementString.init(); elementString.setValue("", true);

	elementInteger = new PersistentDataElementInteger<>("Print Offset X", "Print Offset X", this);
	elementInteger.init(); elementInteger.setValue(0, true);

	elementInteger = new PersistentDataElementInteger<>("Print Offset Y", "Print Offset Y", this);
	elementInteger.init(); elementInteger.setValue(0, true);

}// end of StaticSettings::createPersistentDataSet
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// StaticSettings::loadAll
//
// Overrides to provide custom processing of data loaded from file.
//

@Override
public synchronized void loadAll(IniFile pIniFile)
{

	super.loadAll(pIniFile);

	formatFilePaths("Primary Data Folder Name");
	formatFilePaths("Backup Data Folder Name");

	formatFilePaths("Primary Data Folder Name for Setup Wizard");
	formatFilePaths("Backup Data Folder Name for Setup Wizard");


}//end of StaticSettings::loadAll
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// StaticSettings::formatFilePaths
//
// Formats a file path to use proper file separator, etc. If the path does not end with a file
// separator, one is appended.
//
// The data element's name for the String to be formatted should be passed via pName. The client
// code is responsible for ensuring that pName refers to a file path.
//

protected void formatFilePaths(String pName)
{

    String sep = File.separator;

	String path = getStringValueByName(pName, false);

    path = path.replace("/", sep);
    path = path.replace("\\", sep);

	//add a separator if not one already at the end
    if (!path.endsWith(File.separator)) {path += File.separator;}

	setStringValueByName(pName, path, true);

}//end of StaticSettings::formatFilePaths
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// StaticSettings::saveAll
//
// Overrides super.saveAll so that settings cannot be saved back to file as that operation is not
// allowed for this data set.
//

@Override
public synchronized void saveAll(IniFile pIniFile)
{

	//do nothing...saving static settings to file is not allowed

}//end of StaticSettings::saveAll
//-----------------------------------------------------------------------------


}//end of class StaticSettings
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
