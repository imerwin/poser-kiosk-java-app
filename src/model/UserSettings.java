/******************************************************************************
* Title: UserSettings.java
* Author: Mike Schoonover
* Date: 02/12/20
*
* Purpose:
*
* Provides a Container for a set of PersistentDataElements. It extends PersistentDataSet
* in order to provide a convenient set to hold the elements in this same class.
*
* 
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

//-----------------------------------------------------------------------------
// class UserSettings
//
// Refer to the notes at the top of this page for more details.
//

public class UserSettings extends PersistentDataSet{




//-----------------------------------------------------------------------------
// UserSettings::UserSettings
//
// The pName parameter allows the Container to be named for any purpose.
//
// The pTag can be used as a section header name in an inifile format, such
//	as:
//
//		pTag = "User Settings" -> [User Device Settings] in the file
//
// See parent class for details.
//

public UserSettings(String pName, String pTag)
{

	super(pName, pTag);

}//end of UserSettings::UserSettings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// UserSettings::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

@Override
public synchronized void init()
{

	super.init();

	createPersistentDataSet();


}//end of UserSettings::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// UserSettings::createPersistentDataSet
//
// Creates the persistent data elements and specifieds that this object is also the
// PersistentDataSet to which they belong.
//
// Persistent data elements provide threadsafe variables meant to be adjusted,
// applied, and saved to persistent storage media. (user settings, system settings, etc.)
//

protected void createPersistentDataSet(){


	PersistentDataElementString<String> elementString;
	PersistentDataElementInteger<Integer> elementInteger;
	PersistentDataElementDouble<Double> elementDouble;
	PersistentDataElementBoolean<Boolean> elementBoolean;

	elementString = new PersistentDataElementString<>("General | Robot Name",
																	"General | Robot Name", this);
	elementString.init(); elementString.setValue("", true);

	elementString = new PersistentDataElementString<>("Neck Motor | Current Level",
															"Neck Motor | Current Level", this);
	elementString.init(); elementString.setValue("", true);

}// end of UserSettings::createPersistentDataSet
//-----------------------------------------------------------------------------


}//end of class UserSettings
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
