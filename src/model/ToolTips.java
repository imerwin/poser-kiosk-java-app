/******************************************************************************
* Title: ToolTips.java
* Author: Mike Schoonover
* Date: 02/15/20
*
* Purpose:
*
* Provides a HashMap of Strings containing tooltips...the hints which appear in the
* GUI when the mouse cursor is placed over a GUI component. There may be other uses as well
*
* When the init function is called, the tooltips are loaded from a simple text file containing
* lines of key/value pairs in the form:
*
*		name=tooltip
*
* Example entries in a text file of tooltips:
*
*	Print Button=This button prints the report.
*	Exit Button=This button exits the program.
* 
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

//-----------------------------------------------------------------------------

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.logging.Level;
import java.util.logging.Logger;

// class ToolTips
//
// Refer to the notes at the top of this page for more details.
//

public class ToolTips extends HashMap<String, String>{


//-----------------------------------------------------------------------------
// ToolTips::ToolTips
//

public ToolTips()
{

}//end of ToolTips::ToolTips
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ToolTips::init
//
// Initializes new objects. Should be called immediately after instantiation.
//
// Parameter pFilename specifies the name of the file from which to load the list of tooltips.

public synchronized void init(String pFilename)
{

	loadFromFile(pFilename);

}//end of ToolTips::init
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------
// PersistentDataSet::loadFromFile
//
// Loads all data from file pFilename.
//

public void loadFromFile(String pFilename)
{

    FileInputStream fileInputStream = null;
    InputStreamReader inputStreamReader = null;
    BufferedReader in = null;

    try{

        fileInputStream = new FileInputStream(pFilename);

        inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");

        in = new BufferedReader(inputStreamReader);

        String line;

        while ((line = in.readLine()) != null){
			parseLineAndAddToMap(line);
        }
    }
    catch (FileNotFoundException e){

    }
    catch(IOException e){

    }
    finally{
        if (in != null) {try {in.close();} catch (IOException ex) {}}
        if (inputStreamReader != null) {try {inputStreamReader.close();} catch(IOException ex){}}
        if (fileInputStream != null) {try{fileInputStream.close();} catch(IOException ex){}}
    }

}//end of PersistentDataSet::loadFromFile
//--------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ToolTips::parseLineAndAddToMap
//
// Parses the key=value pair from pLine and adds the pair to the HashMap.
//

public void parseLineAndAddToMap(String pLine)
{


	String[] pair = pLine.split("=", 2);

	if(pair.length < 2){ return; }

	put(pair[0], pair[1]);

}//end of ToolTips::parseLineAndAddToMap
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ToolTips::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of ToolTips::logSevere
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// ToolTips::logStackTrace
//
// Logs stack trace info for exception pE with pMessage at level SEVERE using
// the Java logger.
//

void logStackTrace(String pMessage, Exception pE)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage, pE);

}//end of ToolTips::logStackTrace
//-----------------------------------------------------------------------------

}//end of class ToolTips
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
