/******************************************************************************
* Title: PrinterInfo.java
* Author: Mike Schoonover
* Date: 02/14/20
*
* Purpose:
*
* Provides a Container for information related to a printer.
* 
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

//-----------------------------------------------------------------------------
// class PrinterInfo
//
// Refer to the notes at the top of this page for more details.
//

public class PrinterInfo{


	protected String defaultPrinterForReports;
	public String getDefaultPrinterForReports(){ return (defaultPrinterForReports); }

	protected String destinationFilename;
	public String getDestinationFilename(){ return (destinationFilename); }
	public void setDestinationFilename(String pFilename){ destinationFilename = pFilename; }

	protected int printOffsetX;
	public int getPrintOffsetX(){ return (printOffsetX); }

	protected int printOffsetY;
	public int getPrintOffsetY(){ return (printOffsetY); }

	
	protected boolean printerFound;
	public boolean getPrinterFound(){ return (printerFound); }
	public void setPrinterFound(boolean pState){ printerFound = pState; }

//-----------------------------------------------------------------------------
// PrinterInfo::PrinterInfo
//
// Creates new object with specified printer name and media printable area.
//

public PrinterInfo(String pPrinterName, int pPrintOffsetX, int pPrintOffsetY)
{

	defaultPrinterForReports = pPrinterName;

	printOffsetX = pPrintOffsetX; printOffsetY = pPrintOffsetY;

	printerFound = false;

}//end of PrinterInfo::PrinterInfo
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PrinterInfo::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

public synchronized void init()
{

}//end of PrinterInfo::init
//-----------------------------------------------------------------------------


}//end of class PrinterInfo
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
