/******************************************************************************
* Title: MainSettings.java
* Author: Mike Schoonover
* Date: 02/12/20
*
* Purpose:
*
* Provides a Container for a set of PersistentDataElements. It extends PersistentDataSet
* in order to provide a convenient set to hold the elements in this same class.
*
* The settings in this class are main settings for the program which are not specific to any
* one job or file.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package model;

//-----------------------------------------------------------------------------
// class MainSettings
//
// Refer to the notes at the top of this page for more details.
//

public class MainSettings extends PersistentDataSet{




//-----------------------------------------------------------------------------
// MainSettings::MainSettings
//
// The pName parameter allows the Container to be named for any purpose.
//
// The pTag can be used as a section header name in an inifile format, such
//	as:
//
//		pTag = "Main Settings" -> [Main Device Settings] in the file
//
// See parent class for details.
//

public MainSettings(String pName, String pTag)
{

	super(pName, pTag);

}//end of MainSettings::MainSettings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainSettings::init
//
// Initializes new objects. Should be called immediately after instantiation.
//

@Override
public synchronized void init()
{

	super.init();

	createPersistentDataSet();

}//end of MainSettings::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainSettings::createPersistentDataSet
//
// Creates the persistent data elements and specifies that this object is also the
// PersistentDataSet to which they belong.
//
// Persistent data elements provide threadsafe variables meant to be adjusted,
// applied, and saved to persistent storage media. (user settings, system settings, etc.)
//

protected void createPersistentDataSet()
{

	addNewStringPersistentDataSetElement("Current Filename", "Current Filename", this);


	addNewStringPersistentDataSetElement(
			"Main Head Pi Backpack COM Port List ComboBox", "Main Head Pi Backpack COM Port", this);

	addNewStringPersistentDataSetElement("Front Upper Horizontal LIDAR COM Port List ComboBox",
													"Front Upper Horizontal LIDAR COM Port", this);

	addNewStringPersistentDataSetElement("Front Upper Horizontal LIDAR Scan Mode List ComboBox",
												"Front Upper Horizontal LIDAR Scan Mode", this);

}// end of MainSettings::createPersistentDataSet
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainSettings::addNewStringPersistentDataSetElement
//
/**
 * Creates a String persistent data element with initial value of "".
 *
 * @param pName			the name by which the element is found when it is to be accessed
 * @param pTag			the key value used when the element is stored to file such as: key=value
 * @param pContainer	the parent container to which this element is to be added
 *
 */

protected void addNewStringPersistentDataSetElement(
										String pName, String pTag, PersistentDataSet pContainer)
{

	PersistentDataElementString<String> elementString;

	elementString =
				new PersistentDataElementString<>(pName, pTag, pContainer);

	elementString.init(); elementString.setValue("", true);

}// end of MainSettings::addNewStringPersistentDataSetElement
//-----------------------------------------------------------------------------


}//end of class MainSettings
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
