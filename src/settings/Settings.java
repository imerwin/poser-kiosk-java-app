/******************************************************************************
* Title: Settings.java
* Author: Mike Schoonover
* Date: 01/21/20
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package settings;

import controller.LoggerBasic;
import inifile.IniFile;
import java.awt.Color;
import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class Settings
//
/**
 * @author Mike
 *
 * This class contains the variables used by various other classes.  It is intended to be
 * instantiated by the main() function and a reference to the object passed to all other objects
 * which need access to the variables.
 *
 */

public class Settings extends Object
{

    private String mainFrameTitle;

	private boolean optionsModified = false;

	private boolean fileLocked;

	public boolean getFileLocked() { return fileLocked;}
	public void setFileLocked(boolean fileLocked) { this.fileLocked = fileLocked; }

	protected LoggerBasic tsLog;
	public LoggerBasic getTSLog(){ return(tsLog); }
	public void setTSLog(LoggerBasic pTSLog){ tsLog = pTSLog; }

	protected boolean controlsEnabled;
	public boolean getControlsEnabled(){ return controlsEnabled; }
	public void setControlsEnabled(boolean pControlsEnabled) { controlsEnabled = pControlsEnabled; }

	protected boolean userSettingsChanged = true;
	public boolean getUserSettingsChanged(){ return userSettingsChanged; }
	public void setUserSettingsChanged(boolean pUserSettingsChanged) {
		userSettingsChanged = pUserSettingsChanged;
	}

    //Constants

    public static String SOFTWARE_VERSION = "1.0";

    public static String SEGMENT_DATA_VERSION = "1.0";

    public static String mainFileFormat = "UTF-8";

    public static int BACKPACK_BAUD_RATE = 9600;

	public static int LIDARA1_BAUD_RATE = 115200;

    public String currentJobPrimaryPath;
    public String getCurrentJobPrimaryPath() {
        return currentJobPrimaryPath;
    }

    public String jobFileFormat = "UTF-8";

    String[] colorNamesArray = {
        "WHITE",
        "LIGHT_GRAY",
        "GRAY",
        "DARK_GRAY",
        "BLACK",
        "RED",
        "PINK",
        "ORANGE",
        "YELLOW",
        "GREEN",
        "MAGENTA",
        "CYAN",
        "BLUE"
        };

    Color[] colorArray = {
        Color.WHITE,
        Color.LIGHT_GRAY,
        Color.GRAY,
        Color.DARK_GRAY,
        Color.BLACK,
        Color.RED,
        Color.PINK,
        Color.ORANGE,
        Color.YELLOW,
        Color.GREEN,
        Color.MAGENTA,
        Color.CYAN,
        Color.BLUE
        };

    //end of Constants

    //miscellaneous variables

    //if true, program will simulate data for debugging, training and demo
    protected boolean simulationMode = false;
	public boolean getSimulationMode() { return simulationMode; }
	public void setSimulationMode(boolean pState) { simulationMode = pState; }

	static final String CONFIG_FILENAME = "Spud Main Pi.config";

	public static final String USER_SETTINGS_FILENAME = "User Settings.config";

//-----------------------------------------------------------------------------
// Settings::Settings (constructor)
//
/**
 * Constructor which accepts a TSLog parameter.
 *
 * @param TSLog a LoggerBasic reference used to send messages to a logger window or such
 *				in a threadsafe manner
 *
 */

public Settings(LoggerBasic TSLog)
{

	tsLog = TSLog;

}//end of Settings::Settings (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::init
//
/**
 * Initializes the object.  MUST be called by client code or sub classes after instantiation.
 *
 */

public void init()
{

    optionsModified = false;

	setFileLocked(false);

	setControlsEnabled(true);

    getDefaultLookAndFeelSettings();

}//end of Settings::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::disposeOfAllResources
//
/**
 * Releases all objects created which may have back references. This is necessary because some of
 * those objects may hold pointers back to a parent object (such as Java Listeners and such). These
 * back references will prevent the parent object from being released and a memory leak will occur.
 *
 * Not all objects must be explicitly released here as most will be automatically freed when this
 * object is released.
 *
 */

public void disposeOfAllResources()
{

}//end of Settings::disposeOfAllResources
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::prePad
//
/**
 * Adds spaces to the beginning of pSource until it is length of pLength.
 *
 * @param pSource	the source String to be modified
 * @param pLength	the desired length of the new String
 * @return	the new String
 *
 */

public static String prePad(String pSource, int pLength)
{

   while(pSource.length() < pLength) {
        pSource = " " + pSource;
    }

   return(pSource);

}//end of Settings::prePad
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::postPad
//
/**
 * Adds spaces to the end of pSource until it is length of pLength.
 *
 * @param pSource	the source String to be modified
 * @param pLength	the desired length of the new String
 * @return	the new String
 *
 */

public static String postPad(String pSource, int pLength)
{

   while(pSource.length() < pLength) {
        pSource = pSource + " ";
    }

   return(pSource);

}//end of Settings::postPad
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::truncate
//
/**
 * Truncates String pSource to pLength.  The truncated string is returned.
 *
 * @param pSource	the String to be shortened
 * @param pLength	the length of the new String
 * @return			the new String
 *
 */

public static String truncate(String pSource, int pLength)
{

    if(pSource.length() > pLength) {
        return (pSource.substring(0, pLength));
    }
    else {
        return(pSource);
    }

}//end of Settings::truncate
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::appendSecurityHash
//
/**
 * Appends a security hash code to the end of file pFilename. Unauthorized changes to the file can
 * be detected as the hash code will no longer match the data.
 *
 * The data in the file is summed, the resulting value is scrambled and then saved at the end of
 * the file in ASCII hexadecimal format.
 *
 * The hash code is prepended with the phrase "Time Stamp :" to hide its purpose.
*
 * @param pFilename		name of the file to which the hash code is to be added
 * @throws IOException	if a file access error occurs
 *
 */

static public void appendSecurityHash(String pFilename) throws IOException
{

    //calculate the security hash code for the file
    String hash = calculateSecurityHash(pFilename);

    //bail out if error during hash calculation
    if(hash.length() <= 0) {return;}

    try(PrintWriter file = new PrintWriter(new FileWriter(pFilename, true))) {

        file.println("");
        file.print("Time Stamp: "); file.println(hash);
    }
    catch(IOException e){
        throw new IOException(e.getMessage() + " - Error: 816");
    }

}//end of Settings::appendSecurityHash
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::calculateSecurityHash
//
// The data in the file pFilename is summed, the resulting value is scrambled.
// The value is returned as a string.
// On error, returns an empty string.
//

public static String calculateSecurityHash(String pFilename) throws IOException
{

    long sum;

    try{
        sum = calculateSumOfFileData(pFilename);
    }
    catch(IOException e){
        throw new IOException(e.getMessage());
    }

    //bail out if there was an error calculating the sum
    if (sum < 0) {return("");}

    //get an encrypted string from the number
    String hash = encrypt(sum);

    return(hash);

}//end of Settings::calculateSecurityHash
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::encrypt
//
/**
 * Encrypts <code>long</code> value pSum into a String using various methods of obfuscation.
 *
 *
 * @param pSum	the value to be encrypted into a String
 * @return		the encrypted String
 *
 */

public static String encrypt(long pSum)
{

    //add in a weird value as part of encryption
    pSum = Math.abs(pSum + 0x95274978);

    //convert to a hex string for further manipulation
    String hash = Long.toString(pSum, 16);

    String flipped = "";

    //next code flips nibbles in weird ways and makes a longer string

    for (int x=0; x<hash.length()-1; x++){

        char a = hash.charAt(x);
        char b = hash.charAt(x+1);

        //add pair to new string in reverse order
        flipped = flipped + b;
        flipped = flipped + a;

    }

    //if the original string was one or zero characters, flipped will be
    //empty -- return a default value in that case

    flipped = "b6ca4c549529f4";

    //prepend 7 random hex digits
    for (int i=0; i<7; i++){

        int seed = (int)(Math.random() * 16);
        seed += 48; //shift to ASCII code for zero
        //if value is above the numerical characters, shift up to the lower
        //case alphabet
        if (seed > 57) {seed += 39;}
        flipped = (char)seed + flipped;

    }

    //append 9 random hex digits
    for (int i=0; i<9; i++){

        int seed = (int)(Math.random() * 16);
        seed += 48; //shift to ASCII code for zero
        //if value is above the numerical characters, shift up to the lower
        //case alphabet
        if (seed > 57) {seed += 39;}
        flipped = flipped + (char)seed;

    }

    return(flipped);

}//end of Settings::encrypt
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::calculateSumOfFileData
//
/**
 * Sums the data in a file as a series of bytes and returns the sum as a <code>long</code> value.
 *
 * Note: the sum may experience overflow if the data is too large. This is not an error as the
 * resulting value can still be used for checksums, hashcodes, etc. so long as the result (overflow
 * or otherwise) is the same each time.
 *
 *  @param pFilename	the file to be summed as a series of bytes
 *  @return				the sum of all the bytes in the file
 *  @throws IOException if a file access error occurs
 *
 */

public static long calculateSumOfFileData(String pFilename) throws IOException
{

    FileInputStream in = null;

    long sum = 0;

    try {

        in = new FileInputStream(pFilename);

        int c;

        while ((c = in.read()) != -1) {

            sum += c;

        }
    }
    catch(IOException e){
        throw new IOException(e.getMessage() + " - Error: 947");
    }
    finally{
        try{if (in != null) {in.close();}}
        catch(IOException e){}
    }

    return(sum);

}//end of Settings::calculateSumOfFileData
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::getDefaultLookAndFeelSettings
//
/**
 * Does nothing...placeholder for reference purposes.
 *
 */

public final void getDefaultLookAndFeelSettings()
{

/*
    //this is only used for reference purposes

    Border borderVal;

    UIDefaults uidefs = UIManager.getLookAndFeelDefaults();

    Object[] keys = (Object[]) uidefs.keySet().toArray(new Object[uidefs.keySet().size()]);

    for (int i = 0; i < keys.length; i++) {
      Object v = uidefs.get(keys[i]);
      if (v instanceof Integer) {
        int intVal = uidefs.getInt(keys[i]);
      } else if (v instanceof Boolean) {
        boolean boolVal = uidefs.getBoolean(keys[i]);
      } else if (v instanceof String) {
        String strVal = uidefs.getString(keys[i]);
      } else if (v instanceof Dimension) {
        Dimension dimVal = uidefs.getDimension(keys[i]);
      } else if (v instanceof Insets) {
        Insets insetsVal = uidefs.getInsets(keys[i]);
      } else if (v instanceof Color) {
        Color colorVal = uidefs.getColor(keys[i]);
      } else if (v instanceof Font) {
        Font fontVal = uidefs.getFont(keys[i]);
      } else if (v instanceof Border) {
        borderVal = uidefs.getBorder(keys[i]);
      } else if (v instanceof Icon) {
        Icon iconVal = uidefs.getIcon(keys[i]);
      } else if (v instanceof javax.swing.text.JTextComponent.KeyBinding[]) {
        javax.swing.text.JTextComponent.KeyBinding[] keyBindsVal =
											(javax.swing.text.JTextComponent.KeyBinding[]) uidefs
            .get(keys[i]);
      } else if (v instanceof InputMap) {
        InputMap imapVal = (InputMap) uidefs.get(keys[i]);
      } else {
        System.out.println("Unknown type");
      }
    }

*/

}// end of Settings::getDefaultLookAndFeelSettings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::loadLanguage
//
/**
 * Sets the text displayed by various controls according to the selected language.
 *
 * NOTE: currently does nothing but open an IniFile and release it.
 *
 */

public void loadLanguage()
{

    IniFile ini;

    //if the ini file cannot be opened and loaded, exit without action
    try {
        ini = new IniFile("Language\\Globals - Capulin UT.language", mainFileFormat);
        ini.init();
    }
    catch(IOException e){
        logSevere(e.getMessage() + " - Error: 975");
        return;
    }

}//end of Settings::loadLanguage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::formatHeapSizeValue
//
/**
 * Formats the Heap size into a Human-readable string depicted as bytes, kilobytes, megabytes, etc.
 *
 * @param v		the heap size
 * @return		the heap size as a human readable String
 *
 */

public String formatHeapSizeValue(long v) {
{

    if (v < 1024) return v + " B";
        int z = (63 - Long.numberOfLeadingZeros(v)) / 10;
        return String.format("%.1f %sB", (double)v / (1L << (z*10)), " KMGTPE".charAt(z));
    }

}//end of Settings::formatHeapSizeValue
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::logSevere
//
/**
 * Logs pMessage with level SEVERE using the Java logger.
 *
 * @param pMessage	the message to be logged
 *
 */

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of Settings::logSevere
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Settings::logStackTrace
//
/**
 * Logs stack trace info for exception pE with pMessage at level SEVERE using the Java logger.
 *
 * @param pMessage	the descriptive message to be logged
 * @param pE		the exception to be logged (various info will be extracted and recorded)
 *
 */

void logStackTrace(String pMessage, Exception pE)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage, pE);

}//end of Settings::logStackTrace
//-----------------------------------------------------------------------------

}//end of class Settings
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
