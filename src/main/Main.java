/******************************************************************************
* Title: Main.java - Main Source File for a program based on the
*                                              ModelViewController Template
* Author: Mike Schoonover
* Date: 11/15/13
*
* Purpose:
*
* This application is based on the ModelViewController Template. For detailed
* info on the purpose of the program, view the comments at the top of the
* controller.MainController class.
*
* This class does nothing more than create an instance of
* controller.MainController and pass control to it.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package main;

//-----------------------------------------------------------------------------


import controller.MainController;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.logging.FileHandler;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class Main
//

public class Main{

static MainController controller;

static final int ERROR_LOG_MAX_SIZE = 10000;

//-----------------------------------------------------------------------------
// Main::createController
//
// This method creates an instance of the MainController class which will then
// take over.
//
// Since it will generally cause the creation of the GUI and show it, for
// thread safety, this method should be invoked from the event-dispatching
// thread.  This is usually done by using invokeLater to schedule this function
// to be called from inside the event- dispatching thread.  This is necessary
// because the main function is not operating in the event-dispatching thread.
// See the main function for more info.
//

private static void createController()
{

    //create the program's controller which will create all other objects
    controller = new MainController();
    controller.init();

}//end of Main::createController
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Main::deleteFileIfOverSizeLimit
//
// If file pFilename is larger than pLimit, the file is deleted.
//

private static void deleteFileIfOverSizeLimit(String pFilename, int pLimit)
{

    //delete the logging file if it has become too large

    Path p1 = Paths.get(pFilename);

    try {
        if (Files.size(p1) > pLimit){
            Files.delete(p1);
        }
    }
    catch(NoSuchFileException nsfe){
        //do nothing if file not found -- will be recreated as needed
    }
    catch (IOException e) {
        //do nothing if error on deletion -- will be deleted next time
        logStackTrace("Error: 2488", e);
    }

}//end of Main::deleteFileIfOverSizeLimit
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Main::logStackTrace
//
// Logs stack trace info for exception pE with pMessage at level SEVERE using
// the Java logger.
//

private static void logStackTrace(String pMessage, Exception pE)
{

    Logger.getLogger("").log(Level.SEVERE, pMessage, pE);

}//end of Main::logStackTrace
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Main::main
//

public static void main(String[] args)
{

    //Schedule a job for the event-dispatching thread:
    //creating and showing this application's GUI.

    javax.swing.SwingUtilities.invokeLater(() -> {
		createController();
	});


	setupJavaLogger();

}//end of Main::main
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Main::setupJavaLogger
//
// Prepares the Java logging system for use. Output is directed to a file.
//
// Each time the method is called, it checks to see if the file is larger
// than the maximum allowable size and deletes the file if so.
//

private static void setupJavaLogger()
{

    String logFilename = "Java Logger File.txt";

    //prevent the logging file from getting too big
    deleteFileIfOverSizeLimit(logFilename, ERROR_LOG_MAX_SIZE);

    //remove all existing handlers from the root logger (and thus all child
    //loggers) so the output is not sent to the console

    Logger rootLogger = Logger.getLogger("");
    Handler[] handlers = rootLogger.getHandlers();
    for(Handler handler : handlers) {
        rootLogger.removeHandler(handler);
    }

    //add a new handler to send the output to a file

    Handler fh;

    try{

        //write log to logFilename, 10000 byte limit on each file, rotate
        //between two files, append the the current file each startup

        fh = new FileHandler(logFilename, 10000, 2, true);

        //direct output to a file for the root logger and  all child loggers
        Logger.getLogger("").addHandler(fh);

        //use simple text output rather than default XML format
        fh.setFormatter(new SimpleFormatter());

        //record all log messages
        Logger.getLogger("").setLevel(Level.WARNING);

    }
    catch(IOException e){
        logStackTrace("Error: 2539", e);
    }

}//end of Main::setupJavaLogger
//-----------------------------------------------------------------------------


}//end of class Main
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Useful debugging code

//displays message on bottom panel of IDE
//System.out.println("File not found");

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
