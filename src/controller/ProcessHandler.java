/******************************************************************************
* Title: Process.java
* Author: Mike Schoonover
* Date: 4/13/22
*
* Purpose:
*
* This is an interface for processes which perform continuous tasks. The main loop should call
* the process() method repeatedly to all the Process to perform its work.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package controller;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// interface ProcessHandler
//

public interface ProcessHandler
{

	public boolean process();

	public String getProcessName();

	public void resetProcess(boolean pStopNVLCPlayer);

	public void stopProcess();

}//end of interface ProcessHandler
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
