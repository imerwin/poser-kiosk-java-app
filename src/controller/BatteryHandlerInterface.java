/******************************************************************************
* Title: BatteryHandlerInterface.java
* Author: Mike Schoonover
* Date: 9/18/22
*
* Purpose:
*
* This file contains the interface definition for classes which handle battery data processing,
* display, warnings, etc.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package controller;

//-----------------------------------------------------------------------------
// interface BatteryHandlerInterface
//
//

public interface BatteryHandlerInterface {

	public void handleNewVoltageValue(byte[] pValue);


}//end of interface BatteryHandlerInterface
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
