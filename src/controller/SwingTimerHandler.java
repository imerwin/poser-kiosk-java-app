/******************************************************************************
* Title: SwingTimerHandler.java
* Author: Mike Schoonover
* Date: 10/23/22
*
* Purpose:
*
* This class encapsulates a Java Swing Timer and implements the TimerInterface to provide a
* consistent look and feel.
*
* This class implements Java's ActionListener interface and captures the Timer's ActionEvent calls
* and transposes them to EventInfo objects and passes them on to this object's EventHandler. This
* is done to make the timer handling generic and less specific to Java classes such as Swing.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class SwingTimerHandler
//
/**
*
* This class encapsulates a Java Swing Timer and implements the TimerInterface to provide a
* consistent look and feel.
*
* This class implements Java's ActionListener interface and captures the Timer's ActionEvent calls
* and transposes them to EventInfo objects and passes them on to this object's EventHandler. This
* is done to make the timer handling generic and less specific to Java classes such as Swing.
*
*/

public class SwingTimerHandler extends Object implements ActionListener, TimerInterface{

	public static final String OBJECT_NAME = "SwingTimerHandler";

	EventHandler eventHandler;

	protected javax.swing.Timer timer;

//-----------------------------------------------------------------------------
// SwingTimerHandler::SwingTimerHandler (constructor)
//
/**
 * Constructor which creates a Swing Timer with the specified delay and Event/Action Command.
 *
 * Does not start the timer.
 *
 * @param pEventCommand			the String to be used by the timer when triggering an ActionEvent
 *								this phrase will also be used as the EventCommand passed back to
 *								the EventHandler
 * @param pDelayMS				the timer delay between calls in milliseconds
 * @param pEventHandler			the EventHandler object to which timer calls are to be passed
 *
 */

public SwingTimerHandler(String pEventCommand, int pDelayMS, EventHandler pEventHandler)
{

	eventHandler = pEventHandler;

	timer = new javax.swing.Timer(100, this);
    timer.setActionCommand (pEventCommand);

}//end of SwingTimerHandler::SwingTimerHandler (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SwingTimerHandler::start
//
/**
 * Starts the timer using the initial delay for the first firing and the between-event delay
 * for successive firings.
 *
 * When the timer is created, the initial delay and the between-event delay values are the same
 * unless setInitialDelay has been called.
 *
 */

@Override
public void start()
{

    timer.start();

}//end of SwingTimerHandler::start
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SwingTimerHandler::stop
//
/**
 * Stops the timer.
 *
 */

@Override
public void stop()
{

    timer.stop();

}//end of SwingTimerHandler::stop
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SwingTimerHandler::isRunning
//
/**
 * Returns true if the timer is running, false if not.
 *
 * @return	true if the timer is running, false if not
 *
 */

@Override
public boolean isRunning()
{

    return(timer.isRunning());

}//end of SwingTimerHandler::isRunning
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SwingTimerHandler::restart
//
/**
  * Restarts the timer.
  *
  * Stops the timer, clears all pending firings, and restarts using the initial delay.
  *
  */

@Override
public void restart()
{

    timer.restart();

}//end of SwingTimerHandler::restart
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SwingTimerHandler::setDelay
//
/**
 * Sets the between-event delay period. When a timer is created, the initial delay and the
 * between-event delays are set the same.
 *
 * This does not affect the initial delay value.
 *
 * When the timer is started, it will wait for the initial delay period before the first firing.
 * From thereon, the between-event delay value is used between each successive firings.
 *
 * @param pDelayMS	the delay in milliseconds from start to first triggering
 *
 */

@Override
public void setDelay(int pDelayMS)
{

    timer.setDelay(pDelayMS);

}//end of SwingTimerHandler::setDelay
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SwingTimerHandler::setInitialDelay
//
/**
 * Sets the initial delay. When a timer is created, the initial delay and the between-event delays
 * are set the same.
 *
 * This does not affect the between-event delay value.
 *
 * When the timer is started, it will wait for the initial delay period before the first firing.
 * From thereon, the between-event delay value is used between each successive firings.
 *
 * @param pDelayMS	the delay in milliseconds from start to first triggering
 *
 */

@Override
public void setInitialDelay(int pDelayMS)
{

    timer.setInitialDelay(pDelayMS);

}//end of SwingTimerHandler::setInitialDelay
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SwingTimerHandler::actionPerformed
//
/**
 * Responds to events and passes them on to the eventHandler.
 *
 */

@Override
public void actionPerformed(ActionEvent e)
{

    eventHandler.handleEvent(new EventInfo(e.getActionCommand(), ""));

}//end of SwingTimerHandler::actionPerformed
//-----------------------------------------------------------------------------

}//end of class SwingTimerHandler
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
