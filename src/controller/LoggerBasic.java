/******************************************************************************
* Title: LoggerBasic.java
* Author: Mike Schoonover
* Date: 12/30/21
*
* Purpose:
*
* This interface handles logging messages. The messages can be handled in non-threadsafe or
* threadsafe manner depending on the supplied class.
*
* The implementing classes can display the messages via the GUI, print them to console, save
* them to disk, ignore them, etc.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package controller;

import java.util.ArrayList;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// interface LoggerBasic
//

public interface LoggerBasic
{

public void logMessage(String pMessage);

public void logMessageThreadSafe();

public void separate();

public void date();

public void section();

public void saveToFile(String pFilenameSuffix);

public void saveToFileThreadSafe();

public void displayErrorMessage(String pMessage);

public void appendString(String pText);

public void appendArrayListOfStrings(ArrayList<String> pStrings);

public void appendLine(String pText);

public void appendToErrorLogFile(String pMessage);

}//end of interface LoggerBasic
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
