/******************************************************************************
* Title: Agenda.java
* Author: Mike Schoonover
* Date: 8/25/22
*
* Purpose:
*
* This is an interface for Agenda classes. Agendas are modes where the robot performs some type
* of continuous action, usually to perform a task or achieve a goal in response to stimuli.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package controller;

//-----------------------------------------------------------------------------


//-----------------------------------------------------------------------------
// interface Agenda
//

public interface Agenda
{

	public void init();

	public void loadConfiguration(String pFilename);

	public boolean process();

	public String getProcessName();


}//end of interface Agenda
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
