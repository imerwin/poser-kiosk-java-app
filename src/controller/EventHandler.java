/******************************************************************************
* Title: EventHandler.java
* Author: Mike Schoonover
* Date: 12/30/13
*
*/

//-----------------------------------------------------------------------------

package controller;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// interface EventHandler
//
/**
 * This interface is intended for objects which can receive events from other objects. An
 * EventInfo object is passed from the sender to the receiver via the handleEvent method.
 *
 *
 * @author Mike
 */

public interface EventHandler {

    public String handleEvent(EventInfo pEventInfo);
 
}// end of interface EventHandler
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------