/******************************************************************************
* Title: PlotInterface.java
* Author: Mike Schoonover
* Date: 11/06/20
*
* Purpose:
*
* This file contains the interface definition for Plot functions such as setting color, drawing,
* etc.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package controller;

//-----------------------------------------------------------------------------

import java.awt.Color;
import java.awt.Rectangle;

//-----------------------------------------------------------------------------
// interface PlotInterface
//
// Defines functions to allow different objects to call functions in other
// objects.
//

public interface PlotInterface {

	public void clearPanel(Color pColor);

	public void setUseBufferedImage(boolean pState);

	public void applyBufferedImage();

	public void drawPixel(int pXPos, int pYPos, Color pColor);

	public void drawText(String pText, double pX, double pY);

	public void drawLineDouble(double pX1, double pY1, double pX2, double pY2, float pLineWidth);

	public void drawRectangle(int pXPos, int pYPos, int pWidth, int pHeight, Color pColor);

	public void drawEllipseDouble(double pX,double pY,double pWidth,double pHeight,boolean pFill,
																				float pLineWidth);

	public void setColor(Color pColor);

	public void setColorRGBAlpha(int pRed, int pGreen, int pBlue, int pAlpha);

	public Rectangle getBounds();

	public void update();

	public void setOptions(int pOption1, int pOption2);

	public void copyArea(int pX, int pY, int pWidth, int pHeight, int pDx, int pDy);

}//end of interface PlotInterface
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
