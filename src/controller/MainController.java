/******************************************************************************
* Title: MainController.java
* Author: Mike Schoonover
* Date: 11/15/12
*
* Purpose:
*
* This class is the Main Controller in a Model-View-Controller architecture.
* It creates the Model and the View.
* It tells the View to update its display of the data in the model.
* It handles user input from the View (button pushes, etc.)*
* It tells the Model what to do with its data based on these inputs and tells
*   the View when to update or change the way it is displaying the data.
*
* There may be many classes in the controller package which handle different
* aspects of the control functions.
*
* In this implementation:
*   the Model knows about the View
*   the View knows about the Controller
*   the Controller knows about the Model and the View and interacts with both
*
* The View sends messages to the Controller in the form of action messages
* to an EventHandler object -- in this case the Controller is designated to the
* View as the EventHandler.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package controller;

import remoteDeviceHandlers.PacketTool;
import remoteDeviceHandlers.RS232Link;
import remoteDeviceHandlers.SimulatorOfRS232Link;
import inifile.IniFile;
import java.awt.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.ListIterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.*;
import java.io.File;
import java.net.InetAddress;
import java.net.UnknownHostException;
import model.MainSettings;
import model.Options;
import model.PersistentDataElement;
import model.PersistentDataSet;
import model.PrinterInfo;
import model.StaticSettings;
import model.UserSettings;
import model.HardwareProfile;
import model.HardwareProfileSelection;
import remoteDeviceHandlers.DeviceIdentifierEnum;
import remoteDeviceHandlers.EthernetLink;
import remoteDeviceHandlers.PoseRecognizerHandler;
import settings.Settings;
import view.MainView;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class MainController
//

public class MainController implements EventHandler, Runnable
{

	protected MainView mainView;

	String operatingSystem;

	ArrayList<ProcessHandler> processList;

	PoseRecognizerHandler poseRecognizerHandler;

	Thread mainThread;

	TimerInterface mainTimer;

	protected boolean blockGUIChangeResponse;

	protected boolean prevNotSavedFlagState;

	protected boolean guiPaintingEnabled;

	protected Settings settings;

	protected MainSettings mainSettings;

	protected UserSettings userSettings;

	protected StaticSettings staticSettings;

	protected HardwareProfileSelection hardwareProfileSelection;

	protected HardwareProfile hardwareProfile;

	PrinterInfo printerInfo;

	boolean startupActionsCompleted;

    protected Options options;

    protected final Boolean blinkStatusLabel = false;

    protected String errorMessage;

    protected SwingWorker workerThread;

    protected final DecimalFormat decimalFormat1Dec = new DecimalFormat("#.0");
	protected final DecimalFormat decimalFormat3Dec = new DecimalFormat("0.000");

    int heartBeat = 0;

	SimpleDateFormat dateFormat;

	String soundCardName = "sysdefault:sndrpihifiberry"; //debug mks -- load this from config file

    protected Font tSafeFont;
    protected String tSafeText;

    protected int displayUpdateTimer = 0;
    protected int guiUpdateTimer = 0;

    protected String XMLPageFromRemote;

    protected boolean shutDown;

    protected final JFileChooser fileChooser = new JFileChooser();

    protected final String newline = "\n";

    protected static final int GUI_UPDATE_RATE = 10;

    LoggerBasic tsLog;

	boolean trackImageTarget;

	//todo mks -- read these from config file

	static final String ARM_JRK_G2_SERIAL_NUMBER = "00399988";

	static final String ARM_SHOULDER_ROTATE_RIGHT_TIC_T500_SERIAL_NUMBER = "00409089";

	static final int THIS_DEVICE_ID = DeviceIdentifierEnum.POSER_KIOSK_PI.getValue();

	static final int POSE_RECOGNIZER_SERVER_ID =
											DeviceIdentifierEnum.POSE_RECOGNIZER_SERVER.getValue();

    //bits in the Robot Status Display Select byte

    static final int CHECKSUM_ERROR_CNT = 0x01;
    static final int PACKET_RCVD_CNT = 0x02;

	//the timer seems a bit flaky when running on the Pi 4b+
	//BLINK_DURATION <=100 seems to cause a lot of missed blinks
	//even value of 300 is flaky - not sure if missing blinks or timer is wonky since it often
	//seems to take 4 or 5 seconds between blinks when period is set to 3 seconds

	static final int AVERAGE_BLINK_PERIOD = 3000;
	static final int RATE_OF_QUICK_BLINKS = 2;
	static final int BLINK_DURATION = 100;

//-----------------------------------------------------------------------------
// MainController::MainController (constructor)
//

public MainController()
{

}//end of MainController::MainController (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

public void init()
{

	dateFormat = new SimpleDateFormat("ddMMyy-HHmmss-SSS");

	processList = new ArrayList<>();

	settings = new Settings(tsLog); settings.init();

	createPersistentDataSetForHardwareProfileSelection();

	createPersistentDataSetForHardwareProfile();

	createPersistentDataSetForStaticSettings();

	createPersistentDataSetForMainSettings();

	createPersistentDataSetForUserSettings();

	loadStaticSettingsFromDefaultFile();

	loadMainSettingsFromDefaultFile();

	blockGUIChangeResponse = true;

	prevNotSavedFlagState = false;

	guiPaintingEnabled = true;

	shutDown = false;

	startupActionsCompleted = false;

	trackImageTarget = false;

	mainView = new MainView(this, settings);
	mainView.init();

	mainTimer = new SwingTimerHandler("Main Timer", 300, this);
	mainTimer.start();

    tsLog = mainView.createLogger();

	settings.setTSLog(tsLog);
    tsLog.appendLine("Hello, "  + staticSettings.getStringValueByName("User Name", true) + "!");

    options = new Options();

    mainThread = new Thread(this); mainThread.start();

    // mainView.setupAndStartSwingTimers();

}// end of MainController::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::setupPoseRecognizerHandler
//
/**
 *
 * Creates and initializes the poseRecognizerHandler object which handles interaction with the
 * Pose Recognition server daemon.
 *
 */

protected void setupPoseRecognizerHandler()
{

	//debug mks ~ don't have simulator for EthernetLink yet
	EthernetLink ethernetLink = settings.getSimulationMode() ?
														new EthernetLink() : new EthernetLink();
	ethernetLink.init(tsLog);

	PacketTool packetTool = new PacketTool(tsLog);
	packetTool.init(THIS_DEVICE_ID);

	poseRecognizerHandler = new PoseRecognizerHandler(this, tsLog);
	poseRecognizerHandler.init(POSE_RECOGNIZER_SERVER_ID, packetTool, ethernetLink);

	connectToPoseRecognizerServer();

	processList.add(poseRecognizerHandler);


	boolean useBufferedImage = true;

	PlotInterface poserCanvas =
		mainView.getPoserDisplay().displayBasicGraphicsPanel("Poser Canvas", useBufferedImage, "");

	poseRecognizerHandler.setGraphicsPanel(poserCanvas);

}// end of MainController::setupPoseRecognizerHandler
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::connectPoseRecognizerServer
//
/**
 * Establishes a connection with the Pose Recognizer server program.
 *
 * @return true if connection was successful, false on failure
 *
 */

protected boolean connectToPoseRecognizerServer()
{

	poseRecognizerHandler.disconnect();

	InetAddress ipAddr;

	String remoteName = "Pose Recognizer Server";

	int portNum = hardwareProfile.getIntegerValueByName(remoteName + " Port", true);

	try{

		String ipAddrString =
						hardwareProfile.getStringValueByName(remoteName + " IP Address", true);

		ipAddr = InetAddress.getByName(ipAddrString);

	}catch(UnknownHostException e){
		tsLog.appendLine("Error: " + remoteName + " IP Address invalid!");
		logSevere(e.getMessage() + " - Error: 303");
		return(false);
	}

	int status = poseRecognizerHandler.connect(ipAddr, portNum);

	if(status == -1) { return(false); }	else { return(true); }

}//end of MainController::connectconnectPoseRecognizerServer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::createPersistentDataSetForStaticSettings
//
// Creates the Static Settings persistent data elements and containers to handle them.
//
// Persistent data elements provide threadsafe variables meant to be adjusted,
// applied, and saved to persistent storage media. (user settings, system settings, etc.)
//

protected void createPersistentDataSetForStaticSettings(){

	staticSettings = new StaticSettings("Main Settings", "Main Configuration");
	staticSettings.init();

}// end of MainController::createPersistentDataSetForStaticSettings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::createPersistentDataSetForHardwareProfileSelection
//
/**
 * Creates the HardwareProfileSelection persistent data elements and containers to handle it.
 *
 * The only setting in this class is the Hardware Profile choice...it is loaded on startup and does
 * not change and is never saved back to file.
 *
 * Even though there is only one entry, a PersistentDataElement Container is used for convenience
 * and consistency.
 *
 * Persistent data elements provide threadsafe variables meant to be adjusted,
 * applied, and saved to persistent storage media. (user settings, system settings, etc.)
 *
 */

protected void createPersistentDataSetForHardwareProfileSelection(){

	hardwareProfileSelection =
						new HardwareProfileSelection("Hardware Profile Selection", "Profiles");
	hardwareProfileSelection.init();

}// end of MainController::createPersistentDataSetForHardwareProfileSelection
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::createPersistentDataSetForMainSettings
//
// Creates the Main Settings persistent data elements and containers to handle them.
//
// Persistent data elements provide threadsafe variables meant to be adjusted,
// applied, and saved to persistent storage media. (user settings, system settings, etc.)
//

protected void createPersistentDataSetForMainSettings(){

	mainSettings = new MainSettings("Main Settings", "General");
	mainSettings.init();

}// end of MainController::createPersistentDataSetForMainSettings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::createPersistentDataSetForUserSettings
//
// Creates the User Settings persistent data elements and containers to handle them.
//
// Persistent data elements provide threadsafe variables meant to be adjusted,
// applied, and saved to persistent storage media. (user settings, system settings, etc.)
//

protected void createPersistentDataSetForUserSettings(){

	userSettings = new UserSettings("User Settings", "User Settings");
	userSettings.init();

}// end of MainController::createPersistentDataSetForUserSettings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::createPersistentDataSetForHardwareProfile
//
/** Creates the Hardware Profile persistent data elements and containers to handle them.
 *
 * Persistent data elements provide threadsafe variables meant to be adjusted,
 * applied, and saved to persistent storage media. (user settings, system settings, etc.)
 *
 */

protected void createPersistentDataSetForHardwareProfile(){


	hardwareProfile = new HardwareProfile("Hardware Profile", "General");
	hardwareProfile.init();

}// end of MainController::createPersistentDataSetForHardwareProfile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::handleEvent
//
/**
 * Responds to events received from an EventInfo sender. The EventInfo.command String will be
 * parsed to determine the appropriate action and the EventInfo.data String will be parsed or
 * handed off to another object for processing.
 *
 * @param pEI	a reference to an EventInfo object specifying command and data strings
 * @return		a String containing information to be sent back to the sender
 *
 */

@Override
public String handleEvent(EventInfo pEI)
{

	String command = pEI.getCommand();

    if ("Main Timer".equals(command)) { doTimerActions(); return(null); }

	//String data = pEI.getData(); //not used

//	MouthDisplay mouthDisplay = mainView.getMouthDisplay();



    if ("Main Window is Closing".equals(command)){
		return(handleMainWindowClosing());
	}

    if ("Display Log Window".equals(command)) {
		//DO NOT store this command in pagesDisplayedLIFO.add(command) as it opens a window
//		mainView.setLogWindowVisibility(true); return(null);
	}

//    if ("Hide Log Window".equals(command)) { mainView.setLogWindowVisibility(false); return(null); }

    if ("Display Help".equals(command)) {
		//DO NOT store this command in pagesDisplayedLIFO.add(command) as it opens a window
		displayHelp(); return(null);
	}

    if ("Display About".equals(command)) {
		//DO NOT store this command in pagesDisplayedLIFO.add(command) as it opens a window
//		displayAboutWindow(); return(null);
	}

    if (command.startsWith("Display Status")) { return(null); }

	return(null);

}//end of MainController::handleEvent
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::stopProcess
//
/**
 * If a process whose name contains pProcessNamePhrase is running, stops all copies of it.
 *
 * @param pProcessName	phrase to search the names for in the process list
 *
 * @return	true if the process was running and is now stopped, false if it was not running
 *
 */

boolean stopProcess(String pProcessNamePhrase)
{

	ListIterator iter;

	boolean atLeastOneCopyWasRunning = false;


	for (iter = processList.listIterator(); iter.hasNext(); ){

		ProcessHandler process = ((ProcessHandler)iter.next());

		if(process.getProcessName().contains(pProcessNamePhrase)){

			process.stopProcess();
			iter.remove();

			atLeastOneCopyWasRunning = true;

		}

	}

	return(atLeastOneCopyWasRunning);

}//end of MainController::stopProcess
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::checkIfProcessRunning
//
/**
 * If at least one process whose name contains pProcessNamePhrase is running, returns true.
 * Returns false otherwise.
 *
 * @param pProcessName	phrase to search the names for in the process list
 *
 * @return	true if the process is running, false otherwise
 *
 */

boolean checkIfProcessRunning(String pProcessNamePhrase)
{

	ListIterator iter;

	boolean atLeastOneCopyWasRunning = false;

	for (iter = processList.listIterator(); iter.hasNext(); ){

		ProcessHandler process = ((ProcessHandler)iter.next());

		if(process.getProcessName().contains(pProcessNamePhrase)){
			atLeastOneCopyWasRunning = true;
		}
	}

	return(atLeastOneCopyWasRunning);

}//end of MainController::checkIfProcessRunning
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::stopProcessAndExecuteEvent
//
/**
 * Stops process specified in the pCommand string and then executes the Event also specified as
 * part of the same string.
 *
 * No error results if the process is not currently running. If the Event Command portion is empty
 * or does not match any commands handled by this class, it will be ignored.
 *
 * pCommand		the String containing the name of the process to kill and the Event to execute
 *				example command (leading and trailing spaces for each will be ignored):
 *
 *				Stop Process and Execute Event ` Name of Process to Kill ` Event Command String
 *
 *				Each part is separated by a back-tick '`'.
 *
 */

void stopProcessAndExecuteEvent(String pCommand)
{

	String[] split = pCommand.split("`");

	if (split.length >= 2){ stopProcess(split[1].trim()); }

	if (split.length >= 3){
		handleEvent(new EventInfo(split[2].trim(), "") );
	}

}//end of MainController::MainController::stopProcessAndExecuteEvent
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::shutDownLinux
//
// Issues command to shut down Linux in 3 seconds, then closes this program immediately.
//

protected void shutDownLinux()
{



}//end of MainController::shutDownLinux
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::doTimerActions
//
// Performs actions driven by the timer.
//
// Not used for accessing network -- see run function for details.
//

public void doTimerActions()
{

	if(!startupActionsCompleted) { doStartupActions(); }

	handleProcessList();

}//end of MainController::doTimerActions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::handleProcessList
//
/**
 * Calls ProcessHandler.process() on all ProcessHandlers in processList to all them to perform
 * their work.
 *
 * If process() returns false, that Process is removed from processList and will not be called
 * any more.
 *
 */

public void handleProcessList()
{

	ListIterator iter;

	for (iter = processList.listIterator(); iter.hasNext(); ){

		ProcessHandler process = ((ProcessHandler)iter.next());

		boolean keepAlive = process.process();

		if(!keepAlive){ iter.remove(); }

	}

}//end of MainController::handleProcessList
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::doStartupActions
//
// Performs start up actions to be performed after the GUI has been set up.
//
// This should be called once by a Swing timer so that the GUI has a chance to completely paint and
// the user won't be looking at an incomplete GUI while waiting for start up actions to finish.
//
// userSettings are loaded here in case they take a long time so the GUI is already displayed.

protected void doStartupActions()
{

	loadHardwareProfileSelectionAndInitialize();

	loadHardwareProfileAndInitialize();

	loadUserDataFileAndInitialize();

	setupPoseRecognizerHandler();

	startupActionsCompleted = true;

}//end of MainController::doStartupActions
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------
// MainController::loadStaticSettingsFromDefaultFile
//
/**
 * Loads static data from the default main settings file "Main Settings.ini".
 *
 * Then loads Hardware Profile settings from file "profile selection.config".
 *
 * This is an interesting case of on persistent data set loading data from two separate files. This
 * can be used to create a cascading load effect (like CSS) where subsequent files can provide
 * new values to override
 *
 *
 */

public void loadStaticSettingsFromDefaultFile()
{

	staticSettings.loadFromFile("Spud Head Turn Controller.config");

	staticSettings.loadFromFile("profile selection.config");

	settings.setSimulationMode(staticSettings.getBooleanValueByName("Simulation Mode", true));

}//end of MainController::loadStaticSettingsFromDefaultFile
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// MainController::loadMainSettingsFromDefaultFile
//
// Loads main data from the default main settings file "Main Settings.ini".
//
// Sets any variables which depend on the data.
//

public void loadMainSettingsFromDefaultFile()
{

	mainSettings.loadFromFile("Main Settings.ini");

}//end of MainController::loadMainSettingsFromDefaultFile
//--------------------------------------------------------------------------

//--------------------------------------------------------------------------
// MainController::saveMainSettingsToDefaultFile
//
// Saves main data to the default main settings file "Main Settings.ini".
//

public void saveMainSettingsToDefaultFile()
{

	IniFile configFile;

	//if the ini file cannot be opened and loaded, exit without action
	try {
		configFile = new IniFile("Main Settings.ini", "UTF-8");
		configFile.init();
	}
	catch(IOException e){
		logSevere(e.getMessage() + " - Error: 790");
		return;
	}

	mainSettings.saveAll(configFile);

	configFile.save();

}//end of MainController::saveMainSettingsToDefaultFile
//--------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::loadUserDataFileAndInitialize
//
// Loads user data file specified in settings.currentFilename, displays info messages, performs all
// setup appropriate for a file load.
//

protected void loadUserDataFileAndInitialize()
{

	loadUserSettings();

	blockGUIChangeResponse = true;
	//mainView.copyPersistentDataElementValuesToGUIComponents(userSettings);
	blockGUIChangeResponse = false;

}//end of MainController::loadUserDataFileAndInitialize
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::broadCastPersistentDataElements
//
// Sets various values from the persistent data set in objects which shouldn't have a link to
// the PersistentDataSet or which are time critical and should not search the set and parse
// the value repeatedly.
//
// This should be called whenever the persistent data set is modified, such as when loaded from
// disk or updated from the GUI.
//

protected void broadCastPersistentDataElements()
{

}//end of MainController::broadCastPersistentDataElements
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::displayHelp
//
// Displays help information.
//

protected void displayHelp()
{



}//end of MainController::displayHelp
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::run
//
// This is the part which runs as a separate thread.  The actions of accessing
// remote devices occur here.  If they are done in a timer call instead, then
// buttons and displays get frozen during the sometimes lengthy calls to access
// the network.
//
// NOTE:  All functions called by this thread must wrap calls to alter GUI
// components in the invokeLater function to be thread safe.
//

@Override
public void run()
{

    while(!shutDown){

        if(threadSleep(100)) { return; }

    }

}//end of MainController::run
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::threadSleep
//
// Calls the Thread.sleep function. Placed in a function to avoid the
// "Thread.sleep called in a loop" warning -- yeah, it's cheezy.
//
// Returns true if InterruptedException caught...this usually means some other thread wants to
// shut down this thread.
//

public boolean threadSleep(int pSleepTime)
{

    try {Thread.sleep(pSleepTime);} catch (InterruptedException e) { return(true); }

	return(false);

}//end of MainController::threadSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::displayErrorMessage
//
// Displays an error dialog with message pMessage.
//

public void displayErrorMessage(String pMessage)
{



}//end of MainController::displayErrorMessage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::waitSleep
//
// Sleeps for pTime milliseconds.
//

public void waitSleep(int pTime)
{

    try {Thread.sleep(pTime);} catch (InterruptedException e) { }

}//end of MainController::waitSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::shutDown
//
// Performs shut down procedures.
//
// If multiple threads are running, it may be prudent to signal them to shut down here and wait
// until they do so before returning.
//
// If changed data is unsaved, user is given the chance to save it or cancel the closing.
// If the filename is illegal, a warning is displayed and closing is canceled.
//
// Returns true if it is okay to shut down.
// Returns false if shut down should be aborted.
//

public boolean shutDown()
{

	//mainView.stopAllTimers();
	shutDown = true;
	while (mainThread.isAlive()){ mainThread.interrupt(); }

	disposeOfAllResources();

	return(true);

}//end of MainController::shutDown
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::saveAll
//
// Saves all main settings and user data to file. Also creates and sets the current filename.
//
// If any of the Serial Number, Element Size, Nominal Frequency, Manufacturer entries contains
// characters not allowed for a filename, an error message will be displayed and the data will
// not be saved.
//
// Returns true if user data save was successful, false otherwise. Failure could mean that the
// filename could not be created from the supplied data entries.
//

protected boolean saveAll()
{

	boolean userDataSaved = saveAllUserData();

	saveMainSettingsToDefaultFile();

	return(userDataSaved);

}//end of MainController::saveAll
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::saveAllUserData
//
/**
* Saves all user data to file. Also creates and sets the current filename.
*
*
*
* @return
*	Returns true if user data save was successful, false otherwise. Failure could mean that the
*	filename could not be created from the supplied data entries or the user declined to overwrite
*	an existing file.
*
*/

protected boolean saveAllUserData()
{

	if(!checkExistingFilenameAndAskForConfirmation(
									mainSettings.getStringValueByName("Current Filename", false))){
		tsLog.appendLine("");
		tsLog.appendLine("Error: data file NOT saved!");
		return(false);
	}

	saveUserDataFile();

	tsLog.appendLine("");
	tsLog.appendLine("data saved.");

	return(true);

}//end of MainController::saveAllUserData
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::checkExistingFilenameAndAskForConfirmation
//
/**
 * Checks to see if any data entries used to create the filename have been modified since the last
 * file load or save by comparing with shadow variables. Then checks to see if a file with name
 * pFilename already exists. If so, displays confirmation window to allow user choice of
 * overwriting the existing file or canceling the operation.
 *
 * If user accepts overwrite, the current values of the filename source entries will be copied to
 * their shadow variables so repeated saves can be made to the new file without confirmation. If the
 * entries are then changed again, a new check for an existing file with the new name will be
 * performed to alert the user if such is found.
 *
 * If user declines to overwrite, the entries which were detected as having been modified will have
 * '???' appended and a message displayed to alert user of this action.
 *
 * @param pFilename the name to check for an already existing file

 * @return true if okay to save, false if user has declined to overwrite existing file
 *
 */

protected boolean checkExistingFilenameAndAskForConfirmation(String pFilename)
{


	if (!checkIfFileExists(pFilename)){ return(true); }


	//wip mks -- what do some stuff here

	//String filename = createFilenameFromDataEntries();


	return(true);

}//end of MainController::checkExistingFilenameAndAskForConfirmation
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::checkIfFileExists
//
/**
 * Checks to see if a file exists with the name pFilename. This function will create the full path
 * to the file by prepending the path to the data folder and appending the proper filename
 * extension.
 *
 * @param pFilename name of the file to be checked...this is the filename only, not the full path
 * @return true if a file exists with the name pFilename, false if such file does not exist
 *
 */

protected boolean checkIfFileExists(String pFilename)
{

	String fullPath = staticSettings.getStringValueByName("Primary Data Folder Name", true);

	fullPath = fullPath + File.separator + pFilename + ".txt";

    File presetFile = new File (fullPath);

    return(presetFile.exists());

}//end of MainController::checkIfFileExists
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::compareStringWithPersistentUserSettingByName
//
/**
 * Compares String pSource with the String value retrieved from userSettings having the name
 * pNameOfPersistentDataElement.
 *
 * @param pSource the String to compare
 * @param pNameOfPersistentDataElement the name of the data element with which to compare
 * @return true if the Strings match, false if not
 *
 */

protected boolean compareStringWithPersistentUserSettingByName(String pSource,
															String pNameOfPersistentDataElement)
{

	return(!pSource.equals(
		getStringValueFromPersistentDataSet(userSettings, pNameOfPersistentDataElement, false)));

}//end of MainController::compareStringWithPersistentUserSettingByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::appendPhraseToPersistentDataElementStringByName
//
/**
 * Appends a phrase to a String value specified by name which is stored in a persistent data
 * element contained in a set.
 *
 * @param pPhrase the phrase to be appended
 * @param pPersistentDataSet the set containing the target persistent data element
 * @param pNameOfPersistentDataElement the name of the target data element in the set
 *
 * @return true if the target element was found, false if not found
 *
 */

protected boolean appendPhraseToPersistentDataElementStringByName(String pPhrase,
						PersistentDataSet pPersistentDataSet, String pNameOfPersistentDataElement)
{

	String original =
	   getStringValueFromPersistentDataSet(pPersistentDataSet, pNameOfPersistentDataElement, false);

	if(original == null){ return(false); }

	setStringValueInPersistentDataSet(pPersistentDataSet, pNameOfPersistentDataElement,
																		original + pPhrase, true);

	return(true);

}//end of MainController::appendPhraseToPersistentDataElementStringByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::validateFilename
//
// Checks pString for characters which are not allowed for file or folder names in Windows.
//
// The "matches" function for the String class could not be used since it compares the entire
// string - Internet search suggests using a Whitelist rather than a Blacklist.
//
// Returns true if the string is valid for use, false otherwise.
//

boolean validateFilename(String pString)
{

    return(
            !pString.contains("<") &&
            !pString.contains(">") &&
            !pString.contains("/") &&
            !pString.contains("?") &&
            !pString.contains(":") &&
            !pString.contains("\"") &&
            !pString.contains("\\") &&
            !pString.contains("|") &&
            !pString.contains("*"));

}//end of MainController::validateFilename
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::loadUserSettings
//
/**
 * Loads from the current filename all persistent data elements in the userSettings collection
 * from disk in ini file format: sections and key/values
 *
 * Broadcasts relevant settings to various objects which need the information.
 *
 * Copies the user data entries which are used to create the filename to shadow variables for each
 * of those entries so they can be used later to detect changes to those values.
 *
 */


public void loadUserSettings()
{

	userSettings.loadFromFile(Settings.USER_SETTINGS_FILENAME);

	broadCastPersistentDataElements();

}//end of MainController::loadUserSettings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::loadHardwareProfileSelectionAndInitialize
//
// Loads the Hardware Profile Selection, performs all setup appropriate for a file load.
//

protected void loadHardwareProfileSelectionAndInitialize()
{

	loadHardwareProfileSelection();

	//do anything appropriate after loading of hardware profile

}//end of MainController::loadHardwareProfileSelectionAndInitialize
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------
// MainController::loadHardwareProfileSelection
//
/**
 * Loads Hardware Profile settings from file "profile selection.config".
 *
 */

public void loadHardwareProfileSelection()
{

	hardwareProfileSelection.loadFromFile("profile selection.config");

}//end of MainController::loadHardwareProfileSelection
//--------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::loadHardwareProfileAndInitialize
//
// Loads user data file specified in settings.currentFilename, displays info messages, performs all
// setup appropriate for a file load.
//

protected void loadHardwareProfileAndInitialize()
{

	String selectedHardwareProfile =
							hardwareProfileSelection.getStringValueByName("Hardware Profile", true);

	loadHardwareProfile(selectedHardwareProfile);

	//do anything appropriate after loading of hardware profile

	String machineName = hardwareProfile.getStringValueByName("Machine Name", true);

	tsLog.appendLine("\n*** hardware profile for: " + machineName + " ***");

	operatingSystem = hardwareProfile.getStringValueByName("Operating System", true);

	tsLog.appendLine("\n\nOperating System: " + operatingSystem);

}//end of MainController::loadHardwareProfileAndInitialize
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::loadHardwareProfile
//
/**
 * Loads from the Hardware Profile file specified by pCurrentHardwareProfile all persistent data
 * elements in the userSettings collection from disk in ini file format: sections and key/values
 *
 */


void loadHardwareProfile(String pCurrentHardwareProfile)
{

	String filepath = "hardware profiles/" + pCurrentHardwareProfile + "/HardwareProfile.config";

	hardwareProfile.loadFromFile(filepath);

}//end of MainController::loadUserHardwareProfile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainSettings::createCurrentFilenameFullPath
//
// Creates the full path to the current file by combining the data file path and the current
// filename.
//

public String createCurrentFilenameFullPath()
{

	String path = staticSettings.getStringValueByName("Primary Data Folder Name", true);


	path = path + File.separator + mainSettings.getStringValueByName("Current Filename", true)
   																						+ ".txt";

	return(path);

}// end of MainSettings::createCurrentFilenameFullPath
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::loadUserSettingsHelper
//
// Loads user settings.
//

public void loadUserSettingsHelper()
{

	loadUserSettings();

}//end of MainController::loadUserSettingsHelper
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::saveUserDataFile
//
/**
 * Saves all persistent data elements in the userSettings set to disk in ini file format:
 *		sections and key/values.
 *
 * Updates the shadow variables for the entries used to create the filename so changes to those
 * entries can be detected.
 *
*/

public void saveUserDataFile()
{

	IniFile userSettingsFile = new IniFile(Settings.USER_SETTINGS_FILENAME, "UTF-8");

	try{
		userSettingsFile.init();
	}catch(IOException e){}

	userSettings.saveAll(userSettingsFile);

	userSettingsFile.save();

}//end of MainController::saveUserDataFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::playWAVFile
//
/**
 * Invokes Linux aplay to play a WAV file on the default output.
 *
 * IMPORTANT: use full path...cannot use path relative to Java program because the aplay program
 *			  doesn't know that path.
 *
 * The -Dsoftvol option is used to force aplay to use the software volume control. This volume
 * can be set in Linux by: alsamixer --device=hw
 *		(adjust the PCM control..other controls may be leftover from previous configs)
 *
 * @param pFilepath		the path to the WAV file to be played, example:
 *							/home/pi/speech/test.wav
 *
 */

public void playWAVFile(String pFilepath)
{
		executeProcessCommand(
			"use_aplay_to_play_wav_file", "bash", "-c",
			"aplay -Dsoftvol \"" + pFilepath + "\"");

}//end of MainController::playWAVFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::executeSystemCommand
//
/**
 * Executes command specified by pCommand as a System Command. The commands are system dependent:
 * for example, "shutdown" works on Linux but might not work on other systems.
 *
 * see https://stackoverflow.com/questions/3403226/how-to-run-linux-commands-in-java
 *
 *
 * @param pCommand	the command to be executed
 * @return			true on success, false on failure
 *
 */

boolean executeSystemCommand(String pCommand)
{

//	String s;
	Process p;

	try {

		p = Runtime.getRuntime().exec(pCommand);


		/* use this code to read in Standard Output from the system

		BufferedReader br = new BufferedReader(new InputStreamReader(p.getInputStream()));

		while ((s = br.readLine()) != null) { System.out.println("line: " + s); }
		p.waitFor();
		System.out.println ("exit: " + p.exitValue());

		*/

		p.destroy();

	} catch (IOException e) {}

	return(true);

}//end of MainController::executeSystemCommand
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::executeProcessCommand
//
/**
 * Executes a system command using ProcessBuilder. A new thread is created to handle the task.
 * This seems to have some flexibility over Runtime.getRuntime().exec(pCommand).
 *
 * Displays errors and messages returned from the shell in the Log window.
 *
 * This code was extended from https://github.com/Harium/espeak-java
 *
 * @param pThreadName	the name for the Linux thread
 * @param pCommand		the list of strings to be passed as a command line
 *
 */

private void executeProcessCommand(final String pThreadName, final String ... pCommand) {

	new Thread(new Runnable() {

		@Override
		public void run() {

			ProcessBuilder b = new ProcessBuilder(pCommand);
			b.redirectErrorStream(true);

			try {

				Process process = b.start();

				readErrors(process);
				process.waitFor();
				process.destroy();

			} catch (IOException | InterruptedException e) {

				tsLog.appendLine("Error: " + e.getMessage());

				}

		}

		private void readErrors(Process process) throws IOException {
			BufferedReader in = new BufferedReader(new InputStreamReader(process.getInputStream()));
			String line;
			while ((line = in.readLine()) != null) {
				tsLog.appendLine(line);
			}
		}
	}, pThreadName).start();

}//end of MainController::executeProcessCommand
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::getStringValueFromPersistentDataSet
//
// Retrieves the value of the data element in PersistentDataSet pDataSet which has the name pName
// and returns its value as a String.
//
// If pClearChangedFlag is true, then the changed flag for the data element is set false, otherwise
// it is not modified.
//

public String getStringValueFromPersistentDataSet(PersistentDataSet pDataSet, String pElementName,
																		boolean pClearChangedFlag)
{

	HashMap<String, PersistentDataElement> elements = pDataSet.getElements();

	String value = (String)elements.get(pElementName).getValue(pClearChangedFlag);

	return(value);

}//end of MainController::getStringValueFromPersistentDataSet
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::setStringValueInPersistentDataSet
//
// Sets the value to pNewValue of the data element in PersistentDataSet pDataSet which has the
// name pName.
//
// If pForceChanged is true, the changed flag will be set regardless of whether the new value is
// different from the old value.
//
// Returns false if the element with pName was not found or was not a String type value.
//

public boolean setStringValueInPersistentDataSet(PersistentDataSet pDataSet, String pElementName,
													        String pNewValue, boolean pForceChanged)
{

	HashMap<String, PersistentDataElement> elements = pDataSet.getElements();

	PersistentDataElement element = elements.get(pElementName);

	if (element == null){ return(false); }

	String className = element.getClass().getName();

	if (!className.equals("model.PersistentDataElementString")){ return(false); }

	element.setStringValue(pNewValue, pForceChanged);

	return(true);

}//end of MainController::setStringValueInPersistentDataSet
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::handleMainWindowClosing
//
// Handles actions necessary when the main window is closing.
//
// Returns "OK to Close Window" if closing the window is approved.
// Returns "Abort Close Operation" if closing the window is to be aborted.
//

public String handleMainWindowClosing()
{

	boolean okToShutDown = shutDown();

	if(okToShutDown){
		return("OK to Close Window");
	} else{
		return("Abort Close Operation");
	}

}//end of MainController::handleMainWindowClosing
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::disposeOfAllResources
//
/**
 * Releases all objects created which may have back references. This is
 * necessary because some of those objects may hold pointers back to a parent
 * object (such as Java Listeners and such). These back references will prevent
 * the parent object from being released and a memory leak will occur.
 *
 * Not all objects must be explicitly released here as most will be
 * automatically freed when this object is released.
 *
 */

public void disposeOfAllResources()
{

	//mainView.disposeOfAllResources();

}//end of MainController::disposeOfAllResources
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of MainController::logSevere
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::logStackTrace
//
// Logs stack trace info for exception pE with pMessage at level SEVERE using
// the Java logger.
//

void logStackTrace(String pMessage, Exception pE)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage, pE);

}//end of MainController::logStackTrace
//-----------------------------------------------------------------------------

}//end of class MainController
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// Useful debugging code

// - sets to a light green
//setBackground(new Color(153, 204, 0));
//end

// - sets to red
//setBackground(new Color(255, 0, 0));
//end

//displays message on bottom panel of IDE
//System.out.println("File not found");

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
