/**************************************************************************************************
* Title: TimerInterface
* Author: Mike Schoonover
* Date: 10/23/22
*
* Purpose:
*
* This file contains the interface definition for classes which handle timers. The implementing
* class can either be a GUI safe timer such as Java's Swing Timers or a GUI unsafe timer such as
* a thread.
*
* GUI safe timers allow for the GUI to be accessed to during a timer call. GUI unsafe timers
* require any GUI accesses to be wrapped in invokeLater or similar.
*
* Even if the program is not using Java Swing for the GUI, the Swing Timers are still very
* convenient.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-------------------------------------------------------------------------------------------------

package controller;

//-------------------------------------------------------------------------------------------------
// interface TimerInterface
//
/**
 * This file contains the interface definition for classes which handle timers. The implementing
 * class can either be a GUI safe timer such as Java's Swing Timers or a GUI unsafe timer such as
 * a thread.
 *
 * GUI safe timers allow for the GUI to be accessed to during a timer call. GUI unsafe timers
 * require any GUI accesses to be wrapped in invokeLater or similar.
 *
 * Even if the program is not using Java Swing for the GUI, the Swing Timers are still very
 * convenient.
 *
 */

public interface TimerInterface{

	public void start();

	public void stop();

	public void restart();

	public boolean isRunning();

	public void setInitialDelay(int pDelayMS);

	public void setDelay(int pDelayMS);

}//end of interface TimerInterface
//-------------------------------------------------------------------------------------------------
//-------------------------------------------------------------------------------------------------
