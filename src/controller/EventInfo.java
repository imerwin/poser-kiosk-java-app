/******************************************************************************
* Title: EventInfo.java
* Author: Mike Schoonover
* Date: 02/19/20
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package controller;

//-----------------------------------------------------------------------------
// class EventInfo
//
/**
 * This class provides a container for information about an event to be handled by an EventHandler.
 * It has a command String and a data String. The receiver parses the data from the data String
 * based on the command in the command String.
 *
 * A sender should create an instance of EventInfo to pass to an EventHandler object's handleEvent
 * function.
 *
 * If an EventHandler does not recognize the command in the command string, it can pass the
 * EventInfo object on to other objects.
 * 
 * @author Mike Schoonover
 *
 */

public class EventInfo{


	private String command;

	public String getCommand() { return command; }
	public void setCommand(String command) { this.command = command; }

	private String data;

	public String getData() { return data; }
	public void setData(String data) { this.data = data; }


//-----------------------------------------------------------------------------
// EventInfo::EventInfo (constructor)
//
/**
 * This constructor takes a reference to a String containing a "command" and a reference to a
 * String containing "data".
 *
 * The "command" is any arbitrary text which is known to both the sender and the receiver of the
 * EventInfo object as describing an action to be performed.
 *
 * The "data" is a collection of text and numbers in text format in any arbitrary arrangement.
 * Both the sender and receiver are expected to use a format known to each of them.
 *
 * Example:
 *
 *	pCommand = "Set Gain and Offset"
 *	pData = "1.0, 2.3"
 *		or
 *	pData = "Gain = 1.0 | Offset = 2.3"
 *		or
 *	...many other possible formats...
 *
 * The receiver should know how to split, trim, convert the values in the data String as required.
 *
 * @param pCommand	the "command" from sender to receiver in text
 * @param pData		the "data" from sender to receiver in text
 *
 */

public EventInfo(String pCommand, String pData)
{
	
	command = pCommand; data = pData;

}//end of EventInfo::EventInfo (constructor)
//-----------------------------------------------------------------------------

}//end of class EventInfo
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
