/******************************************************************************
* Title: PacketTool.java
* Author: Mike Schoonover
* Date: 11/05/20
*
* Purpose:
*
* This class handles formatting, reading, and writing of packets.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

import controller.LoggerBasic;
import java.io.*;
import java.net.*;
import java.nio.ByteBuffer;
import java.util.logging.Level;
import java.util.logging.Logger;


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class PacketTool
//
//

public class PacketTool extends Object{

	int thisDeviceIdentifier;

	boolean headerValid;

	PacketTypeEnum pktType;

	int numPktDataBytes;

	int numDataBytesPlusChecksumByte;

	int pktChecksum;

	int destDeviceIdentifierFromReceivedPkt;

	int sourceDeviceIdentifierFromReceivedPkt;

	int resyncCount;

	private final PacketTypeEnum[] pktTypes;

    String simulationDataSourceFilePath = "";

    LoggerBasic tsLog;

    boolean reSynced;
    int reSyncCount = 0, reSyncSkip = 0, reSyncPktID;

    Socket socket = null;
    byte[] inBuffer = new byte[1024];
    byte[] outBuffer = new byte[1024];
    DataOutputStream byteOut = null;
	PushbackInputStream byteIn = null;

    public final static int TIMEOUT = 50;
    int timeOutProcess = 0; //use this one in the packet process functions

	public PacketTypeEnum getPktType(){ return(pktType); }

	public int getNumPktDataBytes(){ return(numPktDataBytes); }

	public int getSourceDeviceIdentifierFromReceivedPkt(){
		return (sourceDeviceIdentifierFromReceivedPkt);
	}

	public byte[] getPktDataBuffer() { return(inBuffer); }

//-----------------------------------------------------------------------------
// PacketTool::Board (constructor)
//

public PacketTool(LoggerBasic pThreadSafeLogger)
{

	tsLog = pThreadSafeLogger;

	//get the list of enum values once and store it as the call is expensive to repeat
	pktTypes = PacketTypeEnum.values();

}//end of PacketTool::Board (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PacketTool::init
//
/**
 * Initializes the object.Must be called immediately after instantiation.
 *
 * @param pThisDeviceIdentifier	the identifier for this device on the network
 *
 */

public void init(int pThisDeviceIdentifier)
{

	thisDeviceIdentifier = pThisDeviceIdentifier;

	resyncCount = 0;

	headerValid = false;

	pktType = PacketTypeEnum.NO_PKT;

}// end of PacketTool::init
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::setStreams
//
/**
 * Sets the input and output streams for the communication port.
 *
 * @param pInputStream			InputStream for the communications port
 * @param pOutputStream			OutputStream for the communications port
 *
 */

public void setStreams(InputStream pInputStream, OutputStream pOutputStream)
{

	byteIn =  new PushbackInputStream(pInputStream);
	byteOut = new DataOutputStream(pOutputStream);

}// end of PacketTool::setStreams
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::checkForPacketReady
//
/**
 * Checks to see if a full packet has been received. Returns true if a complete packet has been
 * received, the checksum is valid, and the packet is addressed to this device (host computer).
 *
 * If ready, the packet type can be accessed by calling getPktType(). The number of data bytes in
 * the packet can be retrieved by calling getNumPktDataBytes(). The data bytes buffer can be
 * accessed by calling getPktDataBuffer().
 *
 * If the checksum for a packet is invalid, function will return false.
 *
 * If the function returns false for any reason the state of the pktType, packet data buffer,
 * and numPktDatabytes are undefined in that case.
 *
 * If enough bytes are waiting in the receive buffer to form a packet header, those bytes are
 * retrieved and the header is analyzed. If enough bytes are waiting in the receive buffer to
 * complete the entire packet based on the number of data bytes specified in the header, those
 * bytes are retrieved and this function returns true if the checksum is valid.
 *
 * If a packet header can be read but the full packet is not yet available, the header info is
 * stored for use in succeeding calls which will keep checking for enough bytes to complete the
 * packet. The function will return false until the full packet has been read.
 *
 * If 0xaa,0x55 is not found when the start of a header is expected, the buffer will be stripped of
 * bytes until it is empty or 0xaa is found. The stripped bytes will be lost forever.
 *
 * NOTE		This function should be called often to prevent serial buffer overflow!
 *
 * @return	true if a full packet with valid checksum is ready, false otherwise
 *
 */

boolean checkForPacketReady()
{

	if(byteIn == null){ return(false); }

	if (!headerValid){
		checkForPacketHeaderAvailable();
		if(!headerValid) { return(false); }
	}

	try{

		if(byteIn.available() < numDataBytesPlusChecksumByte){ return(false); }

		headerValid = false; //reset for next header search since this one is now handled

		byteIn.read(inBuffer, 0, numDataBytesPlusChecksumByte);

	}
    catch(IOException e){
		headerValid = false;
        logSevere(e.getMessage() + " - Error: 218");
    }

	if(destDeviceIdentifierFromReceivedPkt != DeviceIdentifierEnum.ANY_DEVICE.getValue() &&
	   destDeviceIdentifierFromReceivedPkt != thisDeviceIdentifier){
		return(false);
	}

	for(int i=0; i<numDataBytesPlusChecksumByte; i++){ pktChecksum += inBuffer[i]; }

	if ((pktChecksum & 0xff) != 0) { return(false); }

	return(true);

}// end of PacketTool::checkForPacketReady
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::checkForPacketHeaderAvailable
//
/**
 * Checks to see if a header is available in the com buffer. At least 6 bytes must be available
 * and the first two bytes must b 0xaa,0x55.
 *
 * If a valid header is found, other functions can detect this state by checking if
 * (headerValid == true).
 *
 * If a valid header is found, destDeviceIdentifierFromReceivedPkt,
 * sourceDeviceIdentifierFromReceivedPkt, pktType, and numPktDataBytes will be set to the values
 * specified in the header. The bytes in the header will be summed and stored in pktCheckSum.
 *
 * The numPktDataBytes value retrieved from the packet is a 16 bit unsigned integer. Java does not
 * have unsigned values, but the 16 bit unsigned value fits into a Java integer as an
 * always-positive number.
 *
 * For convenience, numDataBytesPlusChecksumByte will be set to (numPktDataBytes + 1).
 *
 * If 0xaa,0x55 is not found when the start of a header is expected, the buffer will be stripped of
 * bytes until it is empty or 0xaa is found. The stripped bytes will be lost forever. The next
 * call to checkForPacketReady will then attempt to read the header or toss more bytes if
 * more invalid data has been received by then.
 *
 * There is no way to verify the header until the entire packet is read. The packet checksum
 * includes the header, so at that time the entire packet can be validated or tossed.
 *
 * Note that line destDeviceIdentifierFromReceivedPkt = byteIn.read() does not cast byteIn.read()
 * to a byte as the client code expects it to be unsigned. Casting as a byte would result in
 * the value being a signed integer rather than an unsigned integer. It still works for the
 * purposes of the checksum. The same is done for the index byte read to set the pktType value.
 *
 */

void checkForPacketHeaderAvailable()
{

	try {

		if(byteIn.available() < 7){ return; }

		pktChecksum = 0;

		if((byte)byteIn.read() != (byte)0xaa){ resync(); return; }

		pktChecksum += (byte)0xaa;

		if((byte)byteIn.read() != (byte)0x55){ resync(); return; }

		pktChecksum += (byte)0x55;

		destDeviceIdentifierFromReceivedPkt = byteIn.read();

		pktChecksum += destDeviceIdentifierFromReceivedPkt;

		sourceDeviceIdentifierFromReceivedPkt = (byte)byteIn.read();

		pktChecksum += sourceDeviceIdentifierFromReceivedPkt;

		pktType = pktTypes[byteIn.read()];

		pktChecksum += pktType.getValue();

		byte numPktDataBytesMSB = (byte)byteIn.read();

		pktChecksum += numPktDataBytesMSB;

		byte numPktDataBytesLSB = (byte)byteIn.read();

		pktChecksum += numPktDataBytesLSB;

		numPktDataBytes = (int)(((numPktDataBytesMSB<<8) & 0xff00) + (numPktDataBytesLSB & 0xff));

		numDataBytesPlusChecksumByte = numPktDataBytes + 1;

		headerValid = true;

	}catch(IOException e){
		headerValid = false;
        logSevere(e.getMessage() + " - Error: 305");
    }

}// end of PacketTool::checkForPacketHeaderAvailable
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::resync
//
/**
 * Reads and tosses bytes from byteIn until 0xaa is found or the buffer is empty. If found, the
 * 0xaa byte is left in the buffer to be read by the next attempt to read the header.
 *
 * Note that a common reason a resync will miss a good packet(s) after a bad one is if the bad
 * packet header specified too many data bytes. This will cause part of the following packet(s) to
 * be read erroneously which will then render them tossed away or invalid due to only a portion of
 * the packet being left in the buffer.
 *
 * todo MKS ~ should have checksum on header itself to prevent wiping out one or more packets
 *  following a packet which specifies larger number of data bytes than is actually in the packet.
 *  This is especially true now that the number of data bytes value is 16 bits...a LOT of data
 *  could be wiped out.
 *
 * Increments resyncCount.
 *
 */

void resync()
{

	resyncCount++;

	try{
		while(byteIn.available() > 0){
			if(peekForValue((byte)0xaa)){ return;}
		}
	}catch(IOException e){
        logSevere(e.getMessage() + " - Error: 333");
    }

}// end of PacketTool::resync
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::peekForValue
//
/**
 * Extracts the next byte from byteIn.
 *
 * If the extracted byte matches pTargetValue, the byte is pushed back onto the buffer and will
 * again be available for reading or peeking; the method returns true.
 *
 * If the extracted byte does not match pTargetValue, the byte is not pushed back onto the buffer;
 * method returns false.
 *
 * @param pTargetValue	the value to match with the byte extracted from buffer.
 *
 * @return				true if the extracted value matches pTargetValue, false otherwise.
 *
 */

boolean peekForValue(byte pTargetValue)
{

	try{

		byte peekVal;

		if ((peekVal = (byte)byteIn.read()) == pTargetValue){
			byteIn.unread(peekVal);
			return(true);
		}
		else {
			return(false);
		}

	}catch(IOException e){
        logSevere(e.getMessage() + " - Error: 358");
		return(false);
    }

}// end of PacketTool::peekForValue
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::prepareHeader
//
/**
 * Sets up a valid packet header at the beginning of pOutBuffer. The header includes:
 *
 *	0xaa, 0x55, <dest device identifier>, <this device identifier>, <packet type>,
 *					 <number of data bytes in packet (MSB)> <number of data bytes in packet (LSB)>
 *
 * The number of data bytes excludes this header and the checksum. Example full packet:
 *
 *	0xaa, 0x55, 1, 0, 1, 4, 5, 1, 2, 3, 4,..., 0x??
 *
 *	where:
 *		0xaa, 0x55 are identifier bytes used in all packet headers
 *		1 is the destination device's identifier (1 for Backpack Device)
 *		0 is this device's identifier (0 for HOST computer)
 *		1 is the packet type (will vary based on the type of packet)
 *		4 is the number of data bytes ~ upper byte of int
 *		5 is the number of data bytes ~ lower byte of int
 *		1,2,3,4,... are the data bytes
 *		0x?? is the checksum for all preceding header and data bytes
 *
 * Note that this function only sets up the header in the buffer, the data bytes and checksum must
 * be added by the calling function.
 *
 * @param pOutBuffer		the buffer in which to store the header
 * @param pDestIdentifier	the identifier of the destination device
 * @param pPacketType		the packet type
 * @param pNumDataBytes		the number of data bytes that will later be added to the packet by
 *							client code
 *
 * @return					the number of values added to the buffer; the index of next empty spot
 *
 *
 */

int prepareHeader(byte[] pOutBuffer, int pDestAddress, PacketTypeEnum pPacketType,int pNumDataBytes)
{

	int x = 0;

	pOutBuffer[x++] = (byte)0xaa;

	pOutBuffer[x++] = (byte)0x55;

	pOutBuffer[x++] = (byte)pDestAddress;

	pOutBuffer[x++] = (byte)thisDeviceIdentifier;

	pOutBuffer[x++] = (byte)(pPacketType.getValue());

	pOutBuffer[x++] = (byte) ((pNumDataBytes >> 8) & 0xff);

	pOutBuffer[x++] = (byte) (pNumDataBytes & 0xff);

	return(x);

}//end of PacketTool::prepareHeader
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::sendBytes
//
/**
 * Sends a variable number of bytes (one or more) to the remote device, prepending a valid header
 * and appending the appropriate checksum.
 *
 * Does nothing if comPort not open.
 *
 * @param pDestAddress		the address of the destination device
 * @param pPacketType		the packet type

 * @param pBytes			the list of bytes to be sent
 *
 * @return					true on success, false on failure
 *
 */

boolean sendBytes(int pDestAddress, PacketTypeEnum pPacketType, byte... pBytes)
{

	if(byteOut == null){ return(false); }

	int numBytes = pBytes.length;

	if(numBytes > 65535){ numBytes = 65535; }

	int x = prepareHeader(outBuffer, pDestAddress, pPacketType, numBytes);

	int i;

    for(i=0; i<numBytes; i++){ outBuffer[x++] = pBytes[i]; }

	int checksum = 0;

    for(int j=0; j<x; j++){ checksum += outBuffer[j]; }

    //calculate checksum and put at end of buffer
    outBuffer[x++] = (byte)((0x100 - (byte)(checksum & 0xff) & 0xff));

    //send packet to remote
    if (byteOut != null) {
        try{
              byteOut.write(outBuffer, 0 /*offset*/, x);
              byteOut.flush();
        }
        catch (IOException e) {
            logSevere(e.getMessage() + " - Error: 422");
			return(false);
        }
    }

	return(true);

}//end of PacketTool::sendBytes
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::sendShortInts
//
/**
 * Sends a variable number of short integers (one or more) to the remote device, prepending a valid
 * header and appending the appropriate checksum. The values are sent Big Endian.
 *
 * NOTE: Actually accepts ints but only sends the lower 16 bits.
 *
 * Does nothing if comPort not open.
 *
 * @param pDestAddress		the address of the destination device
 * @param pPacketType		the packet type
 * @param pInts				the list of values to be sent
 *
 * @return					true on success, false on failure
 *
 */

boolean sendShortInts(int pDestAddress, PacketTypeEnum pPacketType, int... pInts)
{

	if(byteOut == null){ return(false); }

	int numValues = pInts.length;

	int numBytes = numValues * 2;

	int x = prepareHeader(outBuffer, pDestAddress, pPacketType, numBytes);

    for(int i=0; i<numValues; i++){
		outBuffer[x++] = (byte) ((pInts[i] >> 8) & 0xff);
		outBuffer[x++] = (byte) (pInts[i] & 0xff);
	}

	int checksum = 0;

    for(int j=0; j<x; j++){ checksum += outBuffer[j]; }

    //calculate checksum and put at end of buffer
    outBuffer[x++] = (byte)(0x100 - (byte)(checksum & 0xff));

    //send packet to remote
    if (byteOut != null) {
        try{
              byteOut.write(outBuffer, 0 /*offset*/, x);
              byteOut.flush();
        }
        catch (IOException e) {
            logSevere(e.getMessage() + " - Error: 512");
			return(false);
        }
    }

	return(true);

}//end of PacketTool::sendShortInts
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::sendIntegersPacket
//
 /**
 *
 * Sends a variable number of two-byte signed integers to the remote device, prepending a valid
 * header and appending the appropriate checksum.
 *
 * All integers must be -32768<value<32767 or function will return with error.
 *
 * The integers are sent using Big Endian order (MSB first).
 *
 * If pDuplexMode is true, each integer will be sent twice to allow for verification by the
 * receiver. Each integer will be immediately followed by a copy of the integer.
 *
 * Does nothing if comPort not open.
 *
 * @param pDestAddress		the address of the destination device
 * @param pPacketType		the packet type
 * @param pDuplexMode		if true then each value will be sent twice
 * @param pValues			the values to be sent; each must be -32768<value<32767; the maximum
 *							number of values which can be sent is 127 if pDuplexMode = false
 *							and 63 if pDuplexMode = true
 *
 * @return					true on success; false on send failure or if any value is out of
 *							range or if too many values specified
 *
 */

boolean sendIntegersPacket(int pDestAddress, PacketTypeEnum pPacketType, boolean pDuplexMode,
																				 int... pValues)
{

	if(byteOut == null){ return(false); }

	int numValues = pValues.length;

	byte [] buf = null;

	if(!pDuplexMode){
		if(numValues > 30000){ return(false); }
		buf = new byte[numValues * 2];
	}

	if(pDuplexMode){
		if(numValues > 15000){ return(false); }
		buf = new byte[numValues * 4];
	}

	int x = 0;

	for(int i=0; i<numValues; i++){

		buf[x++] = (byte) ((pValues[i] >> 8) & 0xff);
		buf[x++] = (byte) (pValues[i] & 0xff);

		if(pDuplexMode){
			buf[x++] = (byte) ((pValues[i] >> 8) & 0xff);
			buf[x++] = (byte) (pValues[i] & 0xff);
		}

	}

	boolean status = sendBytes(pDestAddress, pPacketType, buf);

	return(status);

}//end of PacketTool::sendIntegersPacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::sendDuplexIntegersPacket
//
 /**
 * Convenience method to call sendIntegersPacket with pDuplexMode set true.
 *
 * Sends a variable number of two-byte signed integers to the remote device, prepending a valid
 * header and appending the appropriate checksum.
 *
 * All integers must be -32768<value<32767 or function will return with error.
 *
 * The integers are sent using Big Endian order (MSB first).
 *
 * Each integer will be sent twice to allow for verification by the receiver. Each integer will be
 * immediately followed by a copy of the integer.
 *
 * Does nothing if comPort not open.
 *
 * @param pDestAddress		the address of the destination device
 * @param pPacketType		the packet type
 * @param pValues			the values to be sent; each must be -32768<value<32767; the maximum
 *							number of values which can be sent is 127 if pDuplexMode = false
 *							and 63 if pDuplexMode = true
 *
 * @return					true on success; false on send failure or if any value is out of
 *							range or if too many values specified
 *
 */

boolean sendDuplexIntegersPacket(int pDestAddress, PacketTypeEnum pPacketType, int... pValues)
{

	boolean status = sendIntegersPacket(pDestAddress, pPacketType, true, pValues);

	return(status);

}//end of PacketTool::sendDuplexIntegersPacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::parseShortIntegerFromPacket
//
 /**
 * Extracts a two-byte signed integer from the current packet data in inBuffer. The integer is
 * reconstructed from the two data bytes in the packet starting at index pStartIndex.
 *
 * The integer is parsed using Big Endian order (MSB first).
 *
 * @param pStartIndex		index of the first byte of the integer
 *
 * @return					the integer value
 *
 */

int parseShortIntegerFromPacket(int pStartIndex)
{

	int x = pStartIndex;

	int value = (short)((inBuffer[x++]<<8) & 0xff00) + (inBuffer[x++] & 0xff);

	return(value);

}//end of PacketTool::parseShortIntegerFromPacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::parseShortIntegerFromDuplexPacket
//
 /**
 * Extracts a two-byte signed integer from the current packet data in inBuffer. The integer is
 * reconstructed from the first two data bytes in the packet.
 *
 * This method expects the value to be stored twice in the packet for verification (duplex).
 *
 * The integer is parsed using Big Endian order (MSB first).
 *
 * Does NOT compare duplex packets for matching values. TODO MKS -- do the verification
 *
 * @return					the integer value
 *
 */

//todo mks ~ this needs to be replaced with parseIntegerFromDuplexPacket from Backpack C++ code?
// rename that function to *ShortInteger* to match this one as it is two bytes

int parseShortIntegerFromDuplexPacketTODOMKS()
{

    int value = (int)((inBuffer[0]<<8) & 0xff00) + (int)(inBuffer[1] & 0xff);

	return(value);

}//end of PacketTool::parseShortIntegerFromDuplexPacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::parseLittleEndianFloatFromPacket
//
 /**
 * Extracts a four-byte float from the current packet data in inBuffer. The float is
 * reconstructed from the four data bytes in the packet starting at index pStartIndex.
 *
 * The byte order is flipped to work with Python on Intelx86 / AMD64. (little endian) and ARM
 * (almost always little endian).

 *
 * @param pStartIndex		index of the first byte of the float
 *
 * @return					the float value
 *
 */

// todo mks -- this has not actually been tested!

float parseLittleEndianFloatFromPacket(int pStartIndex)
{

	int x = pStartIndex;

	byte[] floatBytes = new byte[4];

	floatBytes[3] = inBuffer[x++]; floatBytes[2] = inBuffer[x++];
	floatBytes[1] = inBuffer[x++]; floatBytes[0] = inBuffer[x++];

	float value = ByteBuffer.wrap(floatBytes).getFloat();

	return(value);

}//end of PacketTool::PacketTool::parseLittleEndianFloatFromPacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::parseBigEndianFloatFromPacket
//
 /**
 * Extracts a four-byte float from the current packet data in inBuffer. The float is
 * reconstructed from the four data bytes in the packet starting at index pStartIndex.
 *
 * The float is parsed using Big Endian order (MSB first).
 *
 * @param pStartIndex		index of the first byte of the float
 *
 * @return					the float value
 *
 */

// todo mks -- this has not actually been tested!

float parseBigEndianFloatFromPacket(int pStartIndex)
{

	int x = pStartIndex;

	byte[] floatBytes = new byte[4];

	floatBytes[0] = inBuffer[x++]; floatBytes[1] = inBuffer[x++];
	floatBytes[2] = inBuffer[x++]; floatBytes[3] = inBuffer[x++];

	float value = ByteBuffer.wrap(floatBytes).getFloat();

	return(value);

}//end of PacketTool::PacketTool::parseBigEndianFloatFromPacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::waitForNumberOfBytes
//
/**
 * Waits until pNumBytes number of data bytes are available in the socket or until the specified
 * number of milliseconds pTimeOutMillis has passed.
 *
 * @param pNumBytes			the number of bytes to wait for
 * @param pTimeOutMillis	the maximum number of milliseconds to wait
 * @return					the number of bytes available or -1 if time out occurred or com error
 *
 */

int waitForNumberOfBytes(int pNumBytes, int pTimeOutMillis)
{

	long startTime = System.currentTimeMillis();

	try{
		while((System.currentTimeMillis() - startTime) < pTimeOutMillis){
			if (byteIn.available() >= pNumBytes) {return(byteIn.available());}
		}
	}catch (IOException e) {
		logSevere(e.getMessage() + " - Error: 528");
		return(-1);
    }

	return(-1);

}//end of PacketTool::waitForNumberOfBytes
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PacketTool::readBytes
//
/**
 * Retrieves pNumBytes number of data bytes from the stream and stores them in inBuffer.
 * Returns the number of characters placed in the buffer. A 0 means no valid data was found.
 * Will timeout based on previous call to setTimeout(SERIAL_TIMEOUT_MILLIS).
 *
 * @param pNumBytes	number of bytes to read
 * @return			number of bytes retrieved from the socket; if the attempt times out returns 0
 *
 */

int readBytes(int pNumBytes)
{

	try{
		return(byteIn.read(inBuffer, 0, pNumBytes));
	} catch (IOException e) {
		logSevere(e.getMessage() + " - Error: 556");
		return(0);
    }

}//end of PacketTool::readBytes
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PacketTool::processDataPackets
//
// The amount of time the function is to wait for a packet is specified by
// pTimeOut.  Each count of pTimeOut equals 10 ms.
//
// See processOneDataPacket notes for more info.
//

public int processDataPackets(boolean pWaitForPkt, int pTimeOut)
{

    int x = 0;

    //process packets until there is no more data available

    // if pWaitForPkt is true, only call once or an infinite loop will occur
    // because the subsequent call will still have the flag set but no data
    // will ever be coming because this same thread which is now blocked is
    // sometimes the one requesting data

    //wip mks -- is the above true? explain better or change the functionality

    if (pWaitForPkt) {
        return processOneDataPacket(pWaitForPkt, pTimeOut);
    }
    else {
        while ((x = processOneDataPacket(pWaitForPkt, pTimeOut)) != -1){}
    }

    return x;

}//end of PacketTool::processDataPackets
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PacketTool::processOneDataPacket
//
// This function processes a single data packet if it is available.  If
// pWaitForPkt is true, the function will wait until data is available.
//
// This function should be overridden by sub-classes to provide specialized
// functionality.
//

public int processOneDataPacket(boolean pWaitForPkt, int pTimeOut)
{

    return(0);

}//end of PacketTool::processOneDataPacket
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PacketTool::waitSleep
//
// Sleeps for pTime milliseconds.
//

public void waitSleep(int pTime)
{

    try {Thread.sleep(pTime);} catch (InterruptedException e) { }

}//end of PacketTool::waitSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PacketTool::logStatus
//
// Writes various status and error messages to the log window.
//

public void logStatus(LoggerBasic pLogger)
{

}//end of PacketTool::logStatus
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PacketTool::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of PacketTool::logSevere
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PacketTool::logStackTrace
//
// Logs stack trace info for exception pE with pMessage at level SEVERE using
// the Java logger.
//

void logStackTrace(String pMessage, Exception pE)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage, pE);

}//end of PacketTool::logStackTrace
//-----------------------------------------------------------------------------

}//end of class PacketTool
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
