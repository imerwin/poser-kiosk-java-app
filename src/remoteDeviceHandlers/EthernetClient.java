/******************************************************************************
* Title: EthernetClient.java
* Author: Mike Schoonover
* Date: 11/06/20
*
* Purpose:
*
* This file contains the interface definition for Ethernet Clients.  This interface provides basic
* methods for connecting, monitoring, sending, receiving, processing, and closing the connection.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

//-----------------------------------------------------------------------------

import controller.LoggerBasic;
import java.awt.Point;
import java.net.InetAddress;
import javax.swing.JPanel;

//-----------------------------------------------------------------------------
// interface EthernetClient
//
/**
 * This file contains the interface definition for Ethernet Clients.  This interface provides basic
 * methods for connecting, monitoring, sending, receiving, processing, and closing the connection.
*
*/

public interface EthernetClient extends Runnable {

	public void init(JPanel pViewPanel, String pStreamIPAddress, int pStreamPortNum,
																			int pFramesPerSecond);

	public Point getTargetXY();

	public void stopThread();

	//public int connectToRemote();

	//public boolean closeConnectionToRemote();

	public void disposeOfAllResources();

}//end of interface EthernetClient
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
