
package remoteDeviceHandlers;

public enum MotorCurrentLevelEnum{

	PWM(0),
	MA_500(1),
	MA_1000(2),
	MA_1500(3),
	MA_2000(4);

	private final int value;

	private MotorCurrentLevelEnum(int pValue) { value = pValue; }

    public int getValue(){ return(value); }

};
