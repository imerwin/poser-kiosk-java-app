/******************************************************************************
* Title: SimulatorOfRS232Link.java
* Author: Mike Schoonover
* Date: 01/20/2020
*
* Purpose:
*
* This class handles simulation of the link to the device via RS232 connections.
*
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

//-----------------------------------------------------------------------------

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import controller.LoggerBasic;

//-----------------------------------------------------------------------------
// class SimulatorOfRS232Link
//
//
//

public class SimulatorOfRS232Link extends RS232Link {


static final String SIMULATION_DATA_FILENAME = "Simulation Data/5 Mhz.txt";

static final int MAX_WAVEFORM_DATAPOINTS = 10000;

double samplingFrequencyFromFile;

//-----------------------------------------------------------------------------
// SimulatorOfRS232Link::SimulatorOfRS232Link (constructor)
//

public SimulatorOfRS232Link()
{

	super();

}//end of SimulatorOfRS232Link::SimulatorOfRS232Link (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SimulatorOfRS232Link::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

@Override
public void init(LoggerBasic pTSLog)
{

	super.init(pTSLog);

	samplingFrequencyFromFile = 0.0;

	loadSimulationDataFromFile(SIMULATION_DATA_FILENAME);

}// end of SimulatorOfRS232Link::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SimulatorOfRS232Link::loadSimulationDataFromFile
//
// Loads simulation data from file.
//

public void loadSimulationDataFromFile(String pFilename)
{

    FileInputStream fileInputStream = null;
    InputStreamReader inputStreamReader = null;
    BufferedReader simIn = null;
	String line;
	double amplitude;

    try{

        fileInputStream = new FileInputStream(pFilename);

        inputStreamReader = new InputStreamReader(fileInputStream, "UTF-8");

        simIn = new BufferedReader(inputStreamReader);

        while ((line = simIn.readLine()) != null){

			//store sim data here

        }

    }
    catch (FileNotFoundException e){

        //logSevere(e.getMessage() + " - Error: 109");

    }
    catch(IOException e){
        //logSevere(e.getMessage() + " - Error: 113");
        return;
    }
    finally{
        try{ if (simIn != null) {simIn.close();} } catch(IOException e){}
        try{ if (inputStreamReader != null) {inputStreamReader.close();} } catch(IOException e){}
        try{ if (fileInputStream != null) {fileInputStream.close();} } catch(IOException e){}
    }

}// end of SimulatorOfRS232Link::loadSimulationDataFromFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// SimulatorOfRS232Link::sendInitCommands
//

@Override
public void sendInitCommands(){



}// end of SimulatorOfRS232Link::sendInitCommands
//-----------------------------------------------------------------------------

}//end of class SimulatorOfRS232Link
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
