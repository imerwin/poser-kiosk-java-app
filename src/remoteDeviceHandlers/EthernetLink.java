/******************************************************************************
* Title: EthernetLink.java
* Author: Mike Schoonover
* Date: 07/12/2020
*
* Purpose:
*
* This class handles the link to a remote device via Ethernet connections.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

//-----------------------------------------------------------------------------

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import controller.LoggerBasic;


// Place serial library in lib folder in application's root folder:
//
//	myAppRootFolder/lib/jSerialComm-*.*.*.jar
//
// Open Project Properties/Libraries/Compile:
//
//	next to Classpath, click '+' sign
//	click "Add JAR/Folder"
//	browse to and select jSerialComm-*.*.*.jar
//
// WARNING: make sure you browse to the lib folder of THIS program's root folder...Netbeans will
// usually default to the lib folder of the previous program to which the libary was last added,
// in which case a full path will be used:
//
//	c://Users/Mike/JavaApp/lib/jSerialComm-*.*.*.jar			----> BAD!
//	lib/jSerialComm-*.*.*.jar									----> GOOD!!!
//

import com.fazecast.jSerialComm.SerialPort;
import java.net.InetSocketAddress;
import java.net.SocketAddress;

//-----------------------------------------------------------------------------
// class EthernetLink
//
//
//

public class EthernetLink implements Communications{

    Socket socket = null;
    BufferedReader in = null;
    byte[] inBuffer;
    byte[] outBuffer;
    DataOutputStream byteOut = null;
    DataInputStream byteIn = null;

    public InetAddress ipAddr;
    String ipAddrS;

    public final static int PORT_TIMEOUT = 50;

	int numBytesRead;

	StringBuilder inString = new StringBuilder(1024);

	boolean simulate = false;

	LoggerBasic tsLog;

	static final int LINE_TERMINATOR = 0x0a;

	static final int READ_BYTES_LIMIT = 10000;

	static final int NUM_BYTES_TO_READ_FOR_CLEARING = 100000;

	static final int SOCKET_CONNECT_TIMEOUT = 5000;

	static final int DEVICE_RESPONSE_TIMEOUT = 5000;

	static final int READ_LOOP_TIMEOUT = 50;

	static final int READ_SLEEP_MS = 10;


//-----------------------------------------------------------------------------
// EthernetLink::EthernetLink (constructor)
//

public EthernetLink()
{

}//end of EthernetLink::EthernetLink (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

@Override
public void init(LoggerBasic pThreadSafeLogger)
{

    tsLog = pThreadSafeLogger; numBytesRead = 0;

}// end of EthernetLink::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::connectToRemote
//
/**
 * Establishes a connection with the remote device.
 *
 * @param pIPAddr			the IP address of the remote device
 * @param pPort				the port on the remote device
 * @return					1 on success, -1 on failure to connect with device
 *
 */

@Override
public int connectToRemote(InetAddress pIPAddr, int pPort)
{

    ipAddr = pIPAddr; ipAddrS = ipAddr.toString();

    if (ipAddrS == null || ipAddr == null){
        tsLog.logMessage("Error 119: IP Address is null.\n");
        return(-1);
    }

    try {

        tsLog.logMessage("Connecting to device " + ipAddrS + "...\n");

		socket = new Socket();
		socket.setReuseAddress(true);
		SocketAddress endPoint = new InetSocketAddress(ipAddr, pPort);

        if (!simulate) {
			socket.connect(endPoint, SOCKET_CONNECT_TIMEOUT);
		} else {
            //UTSimulator utSimulator = new UTSimulator(indexNum, simChassisNum,
            //  simSlotNum, backplaneSimulator, ipAddr, 23, mainFileFormat,
            //  simulationDataSourceFilePath, getMaxNumberOfClockPositions());
            //utSimulator.init();

            //socket = utSimulator;
        }

        //set amount of time in milliseconds that a read from the socket will
        //wait for data - this prevents program lock up when no data is ready
        socket.setSoTimeout(PORT_TIMEOUT);

        // the buffer size is not changed here as the default ends up being
        // large enough - use this code if it needs to be increased
        //socket.setReceiveBufferSize(10240 or as needed);

        //allow verification that the hinted size is actually used
        tsLog.logMessage("Device " + ipAddrS + " receive buffer size: " +
                                      socket.getReceiveBufferSize() + "...\n");

        //allow verification that the hinted size is actually used
        tsLog.logMessage("Device " + ipAddrS + " send buffer size: " +
                                        socket.getSendBufferSize() + "...\n");

        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));

        byteOut = new DataOutputStream(socket.getOutputStream());
        byteIn = new DataInputStream(socket.getInputStream());

    }
    catch (UnknownHostException e) {
        logSevere(e.getMessage() + " - Error: 160");
        tsLog.logMessage("Unknown host: Device " + ipAddrS + ".\n");
        return(-1);
    }
    catch (IOException e) {
        logSevere(e.getMessage() + " - Error: 165");
        tsLog.logMessage("Couldn't get I/O for Device " + ipAddrS + "\n");
        tsLog.logMessage("--" + e.getMessage() + "--\n");
        return(-1);
    }

/*
    try {
        //display the greeting message sent by the remote
        tsLog.logMessage("Device " + ipAddrS + " says " + in.readLine() + "\n");
    }
    catch(IOException e){
        logSevere(e.getMessage() + " - Error: 748");
    }
*/

	tsLog.logMessage("Device " + ipAddrS + " is connected.\n");

	return(1);

}// end of EthernetLink::connectToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::connectToRemote
//
/**
 * Connects to remote device using a com port. Not used in this class.
 *
 *
 * @param pSelectedCOMPort	the name of the Serial COM port to be used for the connection
 * @param pBaudRate			baud rate for the connection
 * @return					always returns -1 which designates failure
 *
 */

@Override
public int connectToRemote(String pSelectedCOMPort, int pBaudRate)
{

	return(-1);

}// end of EthernetLink::connectToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::closeConnectionToRemote
//
/**
 * Closes the com port connection. Also sets the DTR line low.
 *
 * @return	true if port successfully closed, false if not
 *
 */

public boolean closeConnectionToRemote()
{

    try{

        if (byteOut != null) {byteOut.close();}
        if (byteIn != null) {byteIn.close();}
        if (in != null) {in.close();}
        if (socket != null) {socket.close();}

    }
    catch(IOException e){
        logSevere(e.getMessage() + " - Error: 257");
		return(false);
    }

	return(true);

}// end of EthernetLink::closeConnectionToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::disposeOfAllResources
//
// Releases all objects created which may have back references. This is
// necessary because some of those objects may hold pointers back to a parent
// object (such as Java Listeners and such). These back references will prevent
// the parent object from being released and a memory leak will occur.
//
// Not all objects must be explicitly released here as most will be
// automatically freed when this object is released.
//

@Override
public void disposeOfAllResources()
{


}//end of EthernetLink::disposeOfAllResources
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::setPortOptions
//
/**
 * Sets various options for comPort such as timeouts, baud rate, stop bit, parity, flow control.
 *
 * Not used by this class
 *
 * @param pBaudRate		the baud rate for the serial com port
 *
 */

void setPortOptions(int pBaudRate)
{

}// end of EthernetLink::setPortOptions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getInputStream
//
/**
 * Returns an InputStream for the serial port.
 *
 * @return InputStream for the serial port.
 *
*/

public InputStream getInputStream()
{

	return(byteIn);

}// end of EthernetLink::getInputStream
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getOutputStream
//
/**
 * Returns an OutputStream for the serial port.
 *
 * @return OutputStream for the serial port.
 *
*/

public OutputStream getOutputStream()
{

	return(byteOut);

}// end of EthernetLink::getOutputStream
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::sendString
//
// Sends a String to the remote device. A LINE_TERMINATOR value is sent as the last byte, so it
// should not be added to the String.
//

public void sendString(String pString)
{

	if(byteOut == null){ return; }

	try {

		byteOut.writeBytes(pString + LINE_TERMINATOR);

	} catch (IOException e) {
		logSevere(e.getMessage() + " - Error: 332");
	}

}// end of EthernetLink::sendString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::readStringResponse
//
// Calls readStringResponse(READ_BYTES_LIMIT).
//
// See readStringResponse(int pMaxNumBytes) for details.
//

public String readStringResponse()
{

	return(readStringResponse(READ_BYTES_LIMIT));

}// end of EthernetLink::readStringResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::readStringResponse
//
/**
 * Reads a character response from the remote device. Characters will be read until pMaxNumBytes
 * have been read, LINE_TERMINATOR is received to indicate the end of the response, or timeout
 * occurs. The response will be returned as a String. If timeout before any characters received, the
 * String will have length of 0. If timeout before LINE_TERMINATOR is received, the last character
 * of the String will not be LINE_TERMINATOR.
 *
 * timeout in milliseconds ~= READ_LOOP_TIMEOUT * READ_SLEEP_MS
 *
 * NOTE: this function is not normally used when data packets are used to communicate. It might
 * be used to receive an initial greeting, but that could also be done using packets.
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @return				the string read - terminated if terminator received before timeout or
 *						pMaxNumBytes characters read; empty string if timeout occurs before any
 *						characters received; null if an error occurs
 *
 */

public String readStringResponse(int pMaxNumBytes)
{

	if(byteIn == null){ return(""); }

	int timeoutLoopCounter = 0;
	boolean done = false;
	byte[] inByte = new byte[1];

	inString.setLength(0);

	try {


		while (!done && timeoutLoopCounter++ < READ_LOOP_TIMEOUT){

			while(!done && byteIn.available() != 0){

				byteIn.read(inByte, 0, 1);

				inString.append((char)inByte[0]);

				timeoutLoopCounter = 0;

				if(inString.length() == pMaxNumBytes){ done = true; break; }

				if(inByte[0] == LINE_TERMINATOR){ done = true; }
			}

			if(!done) { threadSleep(READ_SLEEP_MS); }

		}
	}
	catch(IOException e){
        logSevere(e.getMessage() + " - Error: 411");
		return(null);
	}

	return(inString.toString());

}// end of EthernetLink::readStringResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::clearReceiveBuffer
//
/**
 * Clears the receive buffer by reading NUM_BYTES_TO_READ_FOR_CLEARING number of bytes or until a
 * timeout occurs.
 *
 * @return number of bytes cleared, -1 on error
 *
 */

@Override
public int clearReceiveBuffer()
{

	try{
		if(byteIn.available() <= 0){ return(0); }

	}
	catch(IOException e){
        logSevere(e.getMessage() + " - Error: 411");
		return(-1);
	}

	readBytesResponse(NUM_BYTES_TO_READ_FOR_CLEARING);

	return(numBytesRead);

}// end of EthernetLink::clearReceiveBuffer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::readBytesResponse
//
/**
 * Reads a byte block response from the remote device. Characters will be read until pMaxNumBytes
 * have been read or timeout occurs.
 *
 * The response will be returned as a byte array. If timeout before any characters received, the
 * byte array will be empty.
 *
 * The number of bytes read will be stored in class member variable numBytesRead.
 *
 * timeout in milliseconds ~= READ_LOOP_TIMEOUT * READ_SLEEP_MS
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @return				byte array containing bytes read from stream; may be empty
 *						the number of bytes read is stored in class member variable numBytesRead
 *
 */

@Override
public byte[] readBytesResponse(int pMaxNumBytes)
{

	if(byteIn == null){ return(null); }

	int timeoutLoopCounter = 0;
	boolean done = false;
	byte[] inByte = new byte[1];
	byte[] inBytes = new byte[pMaxNumBytes];

	int i = 0;

	try {

		while (!done && timeoutLoopCounter++ < READ_LOOP_TIMEOUT){

			while(!done && byteIn.available() != 0){

				byteIn.read(inByte, 0, 1);

				inBytes[i++] = inByte[0];

				timeoutLoopCounter = 0;

				if(i == pMaxNumBytes){ done = true; break; }

			}

			if(!done) { threadSleep(READ_SLEEP_MS); }

		}
	}
	catch(IOException e){
        logSevere(e.getMessage() + " - Error: 502");
		return(null);
	}

	numBytesRead = i;

	return(inBytes);

}// end of EthernetLink::readBytesResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::convertBytesToCommaDelimitedDecimalString
//
// Returns the bytes in pBuffer as a comma-delimited string of decimal numbers.
//

public String convertBytesToCommaDelimitedDecimalString(byte[] pBuffer)
{

	inString.setLength(0);

	for(byte b : pBuffer){
		inString.append(b);
		inString.append(',');
	}

	return(inString.toString());

}// end of EthernetLink::convertBytesToCommaDelimitedDecimalString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getListOfSerialComPorts
//
// Retrieves from the operating system a list of the available serial com ports.
//
// Returns an array of SerialPort objects.
//

public SerialPort[] getListOfSerialComPorts()
{

	return(SerialPort.getCommPorts());

}// end of EthernetLink::getListOfSerialComPorts
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getListOfSerialComPortNames
//
// Retrieves from the operating system a list of names of the available serial com ports. These
// will be in the form of "COM1", "COM25", etc.
//
// Returns a ListArray<String> object.
//

public ArrayList<String> getListOfSerialComPortNames()
{

	SerialPort[] serialPorts = getListOfSerialComPorts();

	ArrayList<String> names = new ArrayList<>(serialPorts.length);

	for(SerialPort port : serialPorts){ names.add(port.getSystemPortName()); }

	return(names);

}// end of EthernetLink::getListOfSerialComPortNames
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::getIndexOfSerialPortSpecifiedByName
//
// Finds the index of the serial port with a name matching pName
//
// Returns the index of the port.
// If no match is found, returns -1.
//

protected int getIndexOfSerialPortSpecifiedByName(String pName)
{

	ArrayList<String> ports =  getListOfSerialComPortNames();

	if(ports.isEmpty()){
		tsLog.appendLine("no serial port found!");
		return(-1);
	}

	if(!ports.contains(pName)){
		tsLog.appendLine(pName + " serial port not found!");
		return(-1);
	}

	return(ports.indexOf(pName));

}// end of EthernetLink::getIndexOfSerialPortSpecifiedByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::sendInitCommands
//

public void sendInitCommands(){



}// end of EthernetLink::sendInitCommands
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::threadSleep
//
// Calls the Thread.sleep function. Placed in a function to avoid the
// "Thread.sleep called in a loop" warning -- yeah, it's cheezy.
//
// Returns true if InterruptedException caught...this usually means some other thread wants to
// shut down this thread.
//

public boolean threadSleep(int pSleepTime)
{

    try {Thread.sleep(pSleepTime);} catch (InterruptedException e) { return(true); }

	return(false);

}//end of EthernetLink::threadSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// EthernetLink::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of EthernetLink::logSevere
//-----------------------------------------------------------------------------


	@Override
	public void open() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public void close() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public byte readByte() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public byte[] readBytes(int pNumBytes) {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public byte peek() {
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int readBytesResponse(long pMaxNumBytes, byte[] pByteArray, IntParameter pNumBytesRead){
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int sendBytes(byte[] pBytes, int pNumBytes){
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public int waitForData(long pNumBytesToWaitFor, long pTimeOut, IntParameter pNumBytesAvailable){
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean setDTR(){
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean clearDTR(){
		throw new UnsupportedOperationException("Not supported yet.");
	}

	@Override
	public boolean getIsConnected(){
		throw new UnsupportedOperationException("Not supported yet.");
	}

}//end of class EthernetLink
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
