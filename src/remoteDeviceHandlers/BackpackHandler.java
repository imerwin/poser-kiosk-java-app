/******************************************************************************
* Title: BackpackHandler.java
* Author: Mike Schoonover
* Date: 11/05/20
*
* Purpose:
*
* This class handles interfacing with the Backpack controller.
*
* This class requires EthernetLink and related objects which it uses to communicate with the device.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

import java.nio.charset.Charset;
import java.util.ArrayList;
import controller.LoggerBasic;
import controller.ProcessHandler;
import java.util.logging.Level;
import java.util.logging.Logger;


//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class BackpackHandler
//
/**
 * This class handles interfacing with the Backpack controller.
 *
 * The class requires RS232Link and related objects which it uses to communicate with the
 * Backpack.
 *
 */

public class BackpackHandler extends Object implements ProcessHandler{

	String processName;

	@Override
	public String getProcessName(){ return(processName); }

	int remoteDeviceIdentifier;

    String simulationDataSourceFilePath = "";

    LoggerBasic tsLog;

	PacketTool packetTool;

	RS232Link rs232Link;

	final Charset UTF8_CHARSET = Charset.forName("UTF-8");

	boolean waitingForACKPkt;

	@Override
	public void resetProcess(boolean pStopNVLCPlayer){};

	@Override
	public void stopProcess(){};

	public void setWaitingForACKPkt(boolean pState) { waitingForACKPkt = pState; }

	public boolean getWaitingForACKPkt() { return(waitingForACKPkt); }

	public static final String OBJECT_NAME = "BackpackHandler";

//-----------------------------------------------------------------------------
// BackpackHandler::BackpackHandler (constructor)
//

public BackpackHandler(LoggerBasic pThreadSafeLogger)
{

	tsLog = pThreadSafeLogger;

}//end of BackpackHandler::BackpackHandler (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::init
//
/**
 * Initializes the object.Must be called immediately after instantiation.
 *
 * @param pRemoteDeviceIdentifier	the identifier of the remote device
 * @param pPacketTool				used to create outgoing and parse incoming data packets.
 * @param pRS232Link				used to communicate with the remote device (the Backpack).
 *
 */

public void init(int pRemoteDeviceIdentifier, PacketTool pPacketTool, RS232Link pRS232Link)
{

	processName = "Backpack Handler";

	remoteDeviceIdentifier = pRemoteDeviceIdentifier;

	packetTool = pPacketTool; rs232Link = pRS232Link;

	setWaitingForACKPkt(false);

}// end of BackpackHandler::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::connect
//
/**
 * Establishes a connection with the remote device.
 *
 * @param pComPortName	the name of the com port as reported by the operating system
 * @param pBaudRate		the baud rate to be used for the connection.
 *
 * @return				1 on success, -1 on failure to connect
 *
 */

public int connect(String pComPortName, int pBaudRate)
{

	tsLog.appendLine(""); tsLog.appendLine("connecting to Backpack at " + pBaudRate + " baud...");

	int status = rs232Link.connectToRemote(pComPortName, pBaudRate);

	if(status == -1) { return(status); }

	packetTool.setStreams(rs232Link.getInputStream(), rs232Link.getOutputStream());

	packetTool.sendBytes(remoteDeviceIdentifier, PacketTypeEnum.GET_DEVICE_INFO, (byte)0x00);


	//debug mks
	waitSleep(2000);

//	packetTool.sendDuplexIntegersPacket(remoteDeviceIdentifier,
//								PacketTypeEnum.SET_NECK_MOTOR_DRIVE_CURRENT_LEVEL,
//										MotorCurrentLevelEnum.MA_500.getValue(), 1); //debug mks

//	packetTool.sendDuplexIntegersPacket(remoteDeviceIdentifier,
//								PacketTypeEnum.SET_NECK_MOTOR_DRIVE_CURRENT_LEVEL,
//										MotorCurrentLevelEnum.MA_1000.getValue(), -1); //debug mks

//	packetTool.sendDuplexIntegersPacket(remoteDeviceIdentifier,
//						PacketTypeEnum.SET_NECK_MOTOR_DRIVE_CURRENT_LEVEL,
//										MotorCurrentLevelEnum.MA_1500.getValue(), 32767); //debug mks

//	packetTool.sendDuplexIntegersPacket(remoteDeviceIdentifier,
//						PacketTypeEnum.SET_NECK_MOTOR_DRIVE_CURRENT_LEVEL,
//										MotorCurrentLevelEnum.MA_2000.getValue(), -32768); //debug mks

//	packetTool.sendDuplexIntegersPacket(remoteDeviceIdentifier,
//								PacketTypeEnum.SET_NECK_MOTOR_DRIVE_CURRENT_LEVEL,
//										MotorCurrentLevelEnum.PWM.getValue(), 0x1234); //debug mks


	//debug mks end

	return(status);

}//end of BackpackHandler::connect
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::disconnect
//
/**
 * Closes the connection with the remote device.
 *
 * @return	true if port successfully closed, false if not
 *
 */

public boolean disconnect()
{

	return(rs232Link.closeConnectionToRemote());

}//end of BackpackHandler::disconnect
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::getListOfSerialComPortNames
//
/**
 * Returns a list of the names of available RS232 com ports.
 *
 * @return	list of the names of available RS232 com ports.
 *
 */

public ArrayList<String> getListOfSerialComPortNames()
{

	return(rs232Link.getListOfSerialComPortNames());

}//end of BackpackHandler::getListOfSerialComPortNames
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// BackpackHandler::process
//
/**
 * Handles communications and actions regarding the remote device. This function should be called
 * often to allow continuous processing.
 *
 * @return	true if process is to be kept active, false if process is to be halted
 *
 */

@Override
public boolean process()
{

	handleCommunications();

	return(true);

}//end of BackpackHandler::process
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// BackpackHandler::handleCommunications
//
/**
 * Handle communication with the remote device.
 *
 */

void handleCommunications()
{

	boolean packetReady = packetTool.checkForPacketReady();

	if(packetReady){ handlePacket(); }

}//end of BackpackHandler::handleCommunications
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// BackpackHandler::handlePacket
//
/**
 * Handles a packet received from the remote device.
 *
 */

void handlePacket()
{

	PacketTypeEnum pktType = packetTool.getPktType();

	switch (pktType) {

		case ACK_PKT : setWaitingForACKPkt(false); return;

		case LOG_MESSAGE :logMessageFromPacket(); return;

		default:

			return;
		}

}//end of BackpackHandler::handlePacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// BackpackHandler::logMessageFromPacket
//
/**
 * Logs a message received in a packet from a remote device. The message C-string is
 * extracted from PacketTool.pktDataBuffer and converted to a String. The C-string should have
 * a null terminator in the packet which is ignored when converting to a string.
 *
 */

public void logMessageFromPacket()
{

	int numPktDataBytes = packetTool.getNumPktDataBytes();

	byte[] buf = packetTool.getPktDataBuffer();

	String message = new String(buf, 0, numPktDataBytes-1);

	tsLog.appendLine(message);

}//end of BackpackHandler::logMessageFromPacket
//--------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::startRandomHeadTurning
//
/**
 * Sends command packet to remote device to start random turning of the head.
 *
 */

public void startRandomHeadTurning()
{

	packetTool.sendBytes(remoteDeviceIdentifier, PacketTypeEnum.START_RANDOM_NECK_TURNS,(byte)0xbb);

}//end of BackpackHandler::startRandomHeadTurning
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::stopRandomHeadTurning
//
/**
 * Sends command packet to remote device to stop random turning of the head.
 *
 */

public void stopRandomHeadTurning()
{

	packetTool.sendBytes(remoteDeviceIdentifier, PacketTypeEnum.STOP_RANDOM_NECK_TURNS, (byte)0xbb);

}//end of BackpackHandler::stopRandomHeadTurning
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::startFullHeadTurning
//
/**
 * Sends command packet to remote device to start full turning of the head. The head will be
 * turned back and forth from its extreme positions.
 *
 */

public void startFullHeadTurning()
{

	packetTool.sendBytes(remoteDeviceIdentifier, PacketTypeEnum.START_FULL_NECK_TURNS, (byte)0xbb);

}//end of BackpackHandler::startFullHeadTurning
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::stopFullHeadTurning
//
/**
 * Sends command packet to remote device to start full turning of the head.
 *
 */

public void stopFullHeadTurning()
{

	packetTool.sendBytes(remoteDeviceIdentifier, PacketTypeEnum.STOP_FULL_NECK_TURNS, (byte)0xbb);

}//end of BackpackHandler::stopFullHeadTurning
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::turnHeadSpecifiedStepsAndSpeed
//
/**
 * Sends command packet to remote device to command turning of head by a specified number of
 * steps at a specified speed.
 *
 * @param pNumSteps			number of steps to turn the neck - positive turns left, negative turns
 *							right
 *
 * @param pNeckStepDelayMS	number of milliseconds to delay between each transition of the stepper
 *							motor driver step pulse; controls speed
 *
 */

public void turnHeadSpecifiedStepsAndSpeed(int pNumSteps, int pNeckStepDelayMS)
{

	packetTool.sendDuplexIntegersPacket(remoteDeviceIdentifier,
							PacketTypeEnum.TURN_NECK_SPECIFIED_STEPS, pNumSteps, pNeckStepDelayMS);

}//end of BackpackHandler::turnHeadSpecifiedStepsAndSpeed
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::setNeckMotorCurrentLevel
//
/**
 * Sets the I1/I2 pins of the motor driver to select the desired motor drive current level. One of
 * the preset values can be specified or any number from 0~255 can be supplied instead to PWM
 * I1 to specify a specific level.
 *
 * To specify one of the preset levels, pLevel is passed as MotorCurrentLevel::MA_500,
 * MotorCurrentLevel::MA_1000, MotorCurrentLevel::MA_1500, OR MotorCurrentLevel::MA_2000.
 *
 * To specify a PWM value, pLevel is passed as MotorCurrentLevel::PWM and PWMDutyCycle is passed
 * as 0~255, where 0 is minimum current (0 Amps) and 255 is maximum current (~2 Amps).
 *
 *
 *		I1		I2		Current Limit
 *		-----------------------------
 *		Z		Z		0.5 A
 *		Low		Z		1 A
 *		Z		Low		1.5 A
 *		Low		Low		2 A
 *		PWM		Low		variable
 *
 * @param pLevel		a MotorCurrentLevel enum value which specifies one of a set of possible
 *						drive current levels or PWM if the level is to be set by PWM output
 * @param pPWMDutyCycle	the value to be used as the PWM output (0~255) to the motor driver to
 *						set the current drive level; only applies if pLevel = PWM
 *
 * @return				0 if no errors; error code on error
 *
 */

public int setMotorDriveCurrentLevel(MotorCurrentLevelEnum pLevel, int pPWMDutyCycle)
{

	packetTool.sendDuplexIntegersPacket(remoteDeviceIdentifier,
			PacketTypeEnum.SET_NECK_MOTOR_DRIVE_CURRENT_LEVEL, pLevel.getValue(), pPWMDutyCycle);

	return(0);

}//end of BackpackHandler::setNeckMotorCurrentLevel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::waitSleep
//
// Sleeps for pTime milliseconds.
//

public void waitSleep(int pTime)
{

    try {Thread.sleep(pTime);} catch (InterruptedException e) { }

}//end of BackpackHandler::waitSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::logStatus
//
// Writes various status and error messages to the log window.
//

public void logStatus(LoggerBasic pLogger)
{

}//end of BackpackHandler::logStatus
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of BackpackHandler::logSevere
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BackpackHandler::logStackTrace
//
// Logs stack trace info for exception pE with pMessage at level SEVERE using
// the Java logger.
//

void logStackTrace(String pMessage, Exception pE)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage, pE);

}//end of BackpackHandler::logStackTrace
//-----------------------------------------------------------------------------

}//end of class BackpackHandler
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
