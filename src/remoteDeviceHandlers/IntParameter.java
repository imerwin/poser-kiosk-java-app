/******************************************************************************
* Title: IntParameter.java
* Author: Mike Schoonover
* Date: 10/14/21
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

//-----------------------------------------------------------------------------
// Class IntParameter
//
/**
 * This class encapsulates an int primitive. It is useful for passing as a parameter so that a
 * value can be returned by a method. Unlike the Integer class, this class is mutable.
 *
 * @author Mike Schoonover
 *
 */

public class IntParameter{

	public int i;

//-----------------------------------------------------------------------------
// IntParameter::IntParameter (constructor)
//
/**
 * Constructor which sets the int primitive to zero.
 *
 */

public IntParameter()
{

	i = 0;

}//end of IntParameter::IntParameter (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// IntParameter::IntParameter (constructor)
//
/**
 * Constructor which accepts an initial value.
 *
 * @param pValue	the initial value for the int primitive
 *
 */

public IntParameter(int pValue)
{

	i = pValue;

}//end of IntParameter::IntParameter (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// IntParameter::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

public void init()
{


}// end of IntParameter::init
//-----------------------------------------------------------------------------

}//end of class IntParameter
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
