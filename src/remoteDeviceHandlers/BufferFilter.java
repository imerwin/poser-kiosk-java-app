/******************************************************************************
* Title: BufferFilter.java
* Author: Mike Schoonover
* Date: 07/03/22
*
* Purpose:
*
* This Generic class buffers data and provides filter functions.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

import java.util.ArrayList;
import java.util.ListIterator;


//-----------------------------------------------------------------------------
// class BufferFilter
//
/**
 *
 * This Generic class buffers data and provides filter functions.
 *
 * When a new value is added, the oldest value is removed and tossed and the new value is added to
 * the end. There is no actual "shift" code...the act of putting the new value in and removing the
 * oldest as performed by ArrayList emulates the "shift".
 *
 * @param <tType>	the type of object ~ Double, Integer
 *
 */

public class BufferFilter<tType extends Comparable<tType>> extends Object{


int	 bufferSize;

ArrayList<tType> buffer;

tType defaultValue;

//-----------------------------------------------------------------------------
// BufferFilter::BufferFilter (constructor)
//
/**
 * Constructor for a BufferFilter object.
 *
 * @param pBufferSize	the size for the buffer, it will never contain more elements than this
 *
 */


public BufferFilter(int pBufferSize)
{

	bufferSize = pBufferSize;

	buffer = new ArrayList<>(pBufferSize);

}//end of BufferFilter::BufferFilter (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BufferFilter::init
//
/**
 * Initializes the object. Must be called immediately after instantiation.
 *
 * @param pDefaultValue	the value that is to be returned when an operation does not return an
 *						actual result:
 *						for tType of Double, this is usually Double.MAX_VALUE
 *						for tType of Integer, this is usually Integer.MAX_VALUE
 *						etc.
 *
 */

public void init(tType pDefaultValue)
{

	defaultValue = pDefaultValue;

}// end of BufferFilter::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BufferFilter::addNewAndTossOldest
//
/**
 * Adds a new value to the buffer and removes the oldest value from the buffer if the buffer is
 * overfilled (size {@literal >} bufferSize). The removed value is returned. If the buffer was not
 * yet filled and no value is removed, returns defaultValue as specified in init() call.
 *
 * @param pValue	the value to be added to the buffer
 *
 * @return	the oldest value which was just removed from the buffer or defaultValue if the
 *			buffer was not yet full and thus no value was removed; defaultValue is specified
 *			in the call to init()
 *
 */

public tType addNewAndTossOldest(tType pValue)
{

	buffer.add(pValue);

	if(buffer.size() > bufferSize){
		return( buffer.remove(0) );
	}else{
		return(defaultValue);
	}

}// end of BufferFilter::addNewAndTossOldest
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BufferFilter::checkForRepeatValueSequence
//
/**
 *
 * Checks if a sequence of identical values exists at the input end of the buffer...i.e. the most
 * recent entries. The required length of the sequence is pSequenceLength.
 *
 * NOTE: this only looks for a sequence among the most recent entries, working backwards towards
 *		the oldest. It does not look for a valid sequence anywhere in the buffer.
 *
 * This is used to detect when a value has been steady for a specified number of entries and acts
 * as a filtering mechanism.
 *
 *
 * @param pSequenceLength	the length required for a sequence of identical values to be accepted
 *
 * @return	if a sequence of identical values of length pSequenceLength was detected, returns the
 *			value, if a sequence was not found, returns defaultValue as specified in init() call
 *
 */

public tType checkForRepeatValueSequence(int pSequenceLength)
{

	int duplicateCount = 0;
	tType duplicate = defaultValue;

	ListIterator<tType> listIter = buffer.listIterator(buffer.size());

	while (listIter.hasPrevious()) {

		tType value = listIter.previous();

		if(duplicateCount == 0){
			duplicate = value;
			duplicateCount++;
		}else{
			if (duplicate.compareTo(value) != 0 ) { break; }
			duplicateCount++;
		}

		if(duplicateCount == pSequenceLength){ break; }

	}

	if(duplicateCount == pSequenceLength){
		return(duplicate);
	}else{
		return(defaultValue);
	}

}// end of BufferFilter::addNewAndTossOldest
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// BufferFilter::getLastSequenceAsString
//
/**
 *
 * Returns the trailing sequence of values in the buffer as a String ("x, y, z"). The length of
 * the sequence is specified by pSequenceLength
 *
 *
 * @param pSequenceLength	the length of the sequence to include in the String
 *
 * @return		a String containing the last pSequenceLength number of values in the buffer
 *				the last value in the buffer will be at the beginning of the String
 *
 */

public String getLastSequenceAsString(int pSequenceLength)
{

	int count = 0;
	String sequence = "";

	ListIterator<tType> listIter = buffer.listIterator(buffer.size());

	while (listIter.hasPrevious()) {

		tType value = listIter.previous();

		if (value.compareTo(defaultValue) == 0 ){
			defaultValue = defaultValue;
		}

		String valueString = "" + value;

		if (valueString.length() > 5){
			sequence = sequence + '-' + ",";
		}else{
			sequence = sequence + valueString + ",";
		}

		count++;
		if(count == pSequenceLength){ break; }

	}

	return(sequence);

}// end of BufferFilter::getLastSequenceAsString
//-----------------------------------------------------------------------------

}//end of class BufferFilter
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
