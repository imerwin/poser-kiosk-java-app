/******************************************************************************
* Title: RS232Link.java
* Author: Mike Schoonover
* Date: 01/20/2020
*
* Purpose:
*
* This class handles the link to a remote device via RS232 connections.
*
* This is typically accomplished by using a USB to RS232 converter which will appear as a COM port
* to the program.
*
*
* Serial USB Port Behavior on AdaFruit GrandCentral with SAMD51 Processor
*
* The Grand Central has a "native" USB interface and it behaves differently than the USB interface
* on other boards.
*
* The interface seems to be designed to handle data in "packets"...a single byte or multiple bytes
* which are part of a single write to the port. New packets will be buffered but not transmitted
* through to the Arduino code until that code completely reads any previous packet.
*
* CAUTION: In some cases, such as when using jSerialComm on Windows, the port will be closed if
* three packet writes are attempted before the first packet is read on the Arduino side. The
* CTS/RTS flow control option does not seem to solve the problem, and bytesAwaitingWrite() always
* returns 0 so it is of no use here.
*
* It does not seem possible for the host to detect the overrun condition. This can be solved by
* halting any packet transmissions until the Arduino code sends back an ACK of some sort to signal
* that it has read the previous packet.
*
* If the host sends makes any write, whether it be a single byte or multiple bytes (a packet), no
* further data will be seen by the Arduino until it reads all the bytes from that packet write
* whether it be one byte or multiple.
*
* If more writes are made before the Arduino reads the first packet entirely, the Arduino will not
* see them. If more packets are sent they will be buffered somewhere, but Serial.available() will
* not change to reflect the new bytes until the Arduino has read all of the previous packet.
*
* -- Arduino IDE Serial Monitor Behavior --
*
* If three packet writes are made before the Arduino reads the first one, the Serial monitor will
* appear to lock up. More data can be sent and will be buffered, but the Send window won't show the
* characters typed.
*
* -- jSerialComm Behavior --
*
* If jSerialComm attempts to make three writes before the Arduino can read the first packet, the
* port will be closed! Thus,
*
*
* -- Arduino IDE Serial Monitor Test --
*
* Results of sending 'abc' followed by 'def' followed by 'ghi' before first packet is read:
*
*	Arduino will see 3 bytes via Serial.available()
*	As it reads each byte from the first packet, will see: 2, 1 available
*	After third byte of first packet read, Serial.available will then show: 3 available
*	As it reads each byte from the second packet, will see: 2, 1 available
*	After third byte of second packet read, Serial.available will then show: 3 available
*	As it reads each byte from third packet, will see: 2, 1, 0 available
*
* Note that the monitor window will appear to lock up after sending the third packet.
*
* -- 64 Byte Block Behavior --
*
* If 64 bytes are sent in one write, they are buffered but not made available to the Arduino code.
* Sending another byte will cause all 65 to appear to the Arduino code; if instead two more bytes
* are sent it will cause all 67 to appear to the Arduino code, and so forth. The buffering is
* probably done in the processor's USB module and its associated memory.
*
* Thus if 64 bytes are sent they are buffered but not transferred; if a second group of 64 are sent
* they are also buffered along with the first batch but not transferred; same is true for a third
* batch of 64; if a fourth batch of 64 is sent all 256 bytes are then available to the Arduino code.
*
* If 256 bytes are sent at once, they are immediately available to the Arduino code.
*
* If ANY batch sent has more or less than 64 bytes, that batch and the preceding ones are
* immediately made available to the Arduino code.
*
* It is unknown if this behavior is a bug, a side effect, or intentional behavior meant to improve
* the efficiency of transferring large data blocks such as when the Arduino IDE programs the device
* with user code.
*
* -- Why? --
*
* Most Arduino compatible devices use a USB-to-serial converter to allow the host to program and
* communicate with the device. One one end, the host computer appears to be talking to a Serial COM
* port and on the other end the Arduino's main processor appears to be talking to a traditional
* RS-232 style connection (although the voltage levels are digital and NOT the higher RS-232
* levels). On older Arduinos, an FTDI chip was included on the board to perform the conversion.
* Later Arduinos used a second Atmel chip in addition to the main processor to serve the same
* purpose as some Atmel chips are able to interface with USB.
*
* These boards are fairly straightforward. The USB-to-serial converter provides TX, RX, DTR, CTS,
* RTS, and other lines to inputs of the board's main processor. Typically, those inputs are
* connected to dedicated serial modules which are built into the chip.
*
* On the SAMD51 style boards, the USB is connected straight to the main processor. The TX, RX, DTR,
* CTS, RTS lines are not actually visible outside the chip...and may not even exist. The processor's
* code reads the USB data directly and does whatever needs to be done with it inside the chip.
* Since it is never transmitted between chips, the data does not have to be re-transmitted using
* the traditional serial lines. Indeed, the USB receiver can implement any manner of behavior and
* apparently mimics a serial port only to the minimal degree.
*
* However, it should be noted that to the host the USB connection appears to be a Serial COM port,
* except with some unusual behavior as outlined in this text. The USB driver still mimics this
* behavior because the Arduino IDE and a great amount of legacy code expects that type of interface.
* Note that is is possible to program the SAMD51 so that it appears as a USB device such as a
* keyboard, mouse, or other USB aware equipment. In that case, the software on the host would
* interface with the USB driver as a USB device instead of it mimicking a Serial COM port.
*
* -- Suggested Programming Technique --
*
* If the Arduino constantly reads the serial port in a loop and transfers all data to a buffer
* then the above behavior becomes inconsequential. This works so long as the code loop is fast
* enough such that the host never sends three packets before they can be read by the Arduino.
*
* This is not always feasible. A better solution is to send data in packets from the host. The
* host then disallows sending of packets until an ACK of some sort is received from the Arduino.
* The ACK method is necessary because the host has no way of knowing if the Arduino has read
* previously sent packets...and if it has not then sending more packets may cause the port to be
* closed on some systems.
*
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

//-----------------------------------------------------------------------------

import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import controller.LoggerBasic;

// Place serial library in lib folder in application's root folder:
//
//	myAppRootFolder/lib/jSerialComm-*.*.*.jar
//
// Open Project Properties/Libraries/Compile:
//
//	next to Classpath, click '+' sign
//	click "Add JAR/Folder"
//	browse to and select jSerialComm-*.*.*.jar
//
// WARNING: make sure you browse to the lib folder of THIS program's root folder...Netbeans will
// usually default to the lib folder of the previous program to which the libary was last added,
// in which case a full path will be used:
//
//	c://Users/Mike/JavaApp/lib/jSerialComm-*.*.*.jar			----> BAD!
//	lib/jSerialComm-*.*.*.jar									----> GOOD!!!
//

import com.fazecast.jSerialComm.SerialPort;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class RS232Link
//
//
//

public class RS232Link implements Communications{

	SerialPort comPort;

	int numBytesRead;

	byte[] inByte = new byte[1];
    byte[] inBuffer = new byte[10];
    byte[] outBuffer = new byte[10];

	StringBuilder inString = new StringBuilder(1024);

	boolean simulate = false;

	LoggerBasic tsLog;

	//the timeout used for reading and writing of the serial port
	static final int PORT_TIMEOUT = 500; //debug mks -- set back to 250

	//multiply READ_LOOP_TIMEOUT * READ_SLEEP_MS to get time out in millseconds
	//these values are used in a loop
	static final int READ_LOOP_TIMEOUT = 50;
	static final int READ_SLEEP_MS = 10;

	static final int LINE_TERMINATOR = 0x0a;

	static final int READ_BYTES_LIMIT = 10000;

	static final int NUM_BYTES_TO_READ_FOR_CLEARING = 100000;

	static final int DEVICE_RESPONSE_TIMEOUT = 5000;

	static final int SUCCESS = 0;
	static final int PORT_CLOSED = 1;
	static final int PORT_ERROR = 2;
	static final int TIMEOUT = 3;

//-----------------------------------------------------------------------------
// RS232Link::RS232Link (constructor)
//

public RS232Link()
{

}//end of RS232Link::RS232Link (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

@Override
public void init(LoggerBasic pThreadSafeLogger)
{

    tsLog = pThreadSafeLogger; comPort = null; numBytesRead = 0;

}// end of RS232Link::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::connectToRemote
//
/**
 * Establishes a connection with the remote device.
 *
 *
 * @param pSelectedCOMPort	the name of the Serial COM port to be used for the connection
 * @param pBaudRate			baud rate for the connection
 * @return					1 on success, -1 on failure to connect with device
 *
 */

@Override
public int connectToRemote(String pSelectedCOMPort, int pBaudRate)
{

	closeConnectionToRemote();

	int portIndex = getIndexOfSerialPortSpecifiedByName(pSelectedCOMPort);

	if(portIndex == -1){ return(-1); }

	comPort = SerialPort.getCommPorts()[portIndex];

	if(comPort == null){
		tsLog.appendLine("");
		tsLog.appendLine("ERROR: cannot open COM Port!!!");
		tsLog.appendLine("");
		return(-1);
	}

	setPortOptions(pBaudRate);

	comPort.setDTR(); //establishes connection with device's com port

	boolean status = comPort.openPort();

/*

	long markTime = System.currentTimeMillis();

	while(comPort.bytesAvailable() <= 0){
		if ((System.currentTimeMillis() - markTime) > DEVICE_RESPONSE_TIMEOUT){
			comPort.closePort();
			tsLog.appendLine("ERROR: Connection failed as device did not respond.");
			return(-1);
		}
	}

	comPort.readBytes(inBuffer, 1);

	if(inBuffer[0] != 'A'){
		comPort.closePort();
		tsLog.appendLine("ERROR: Connection failed as device did not return proper greeting.");
		return(-1);
	}

*/

	tsLog.appendLine("Baud Rate = " + pBaudRate + "...");

	tsLog.appendLine("Device is connected.\n");

	return(1);

}// end of RS232Link::connectToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::connectToRemote
//
/**
 * Connects to remote device using an IP address. Not used in this class.
 *
 * @param pIPAddr			the IP address of the remote device
 * @param pPort				the port on the remote device
 * @return					always returns -1 which designates failure
 *
 */

@Override
public int connectToRemote(InetAddress pIPAddr, int pPort)
{

	return(-1);

}// end of RS232Link::connectToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::getIsConnected
//
/**
 * Returns the connected/unconnected state of the communication link.
 *
 * @return	true if connected, false if not connected
 *
 */

@Override
public synchronized boolean getIsConnected()
{

	if(comPort != null && comPort.isOpen()) {
		return(true);
	}else{
		return(false);
	}

}// end of RS232Link::getIsConnected
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::closeConnectionToRemote
//
/**
 * Closes the com port connection. Also sets the DTR line low.
 *
 * @return	true if port successfully closed, false if not
 *
 */

@Override
public synchronized boolean closeConnectionToRemote()
{

	if(comPort == null || !comPort.isOpen()) { return(true); }

	comPort.clearDTR(); //breaks connection with device's com port

	return(comPort.closePort());

}// end of RS232Link::closeConnectionToRemote
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::setDTR
//
/**
 * Sets the serial port DTR line.
 *
 * @return	true if operation is successful
 *
 */

@Override
public boolean setDTR()
{

	if(comPort == null || !comPort.isOpen()) { return(false); }

	return(comPort.setDTR());

}// end of RS232Link::setDTR
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::clearDTR
//
/**
 * Clears the serial port DTR line.
 *
 * @return	true if operation is successful
 *
 */

@Override
public boolean clearDTR()
{

	if(comPort == null || !comPort.isOpen()) { return(false); }

	return(comPort.clearDTR());

}// end of RS232Link::clearDTR
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::disposeOfAllResources
//
// Releases all objects created which may have back references. This is
// necessary because some of those objects may hold pointers back to a parent
// object (such as Java Listeners and such). These back references will prevent
// the parent object from being released and a memory leak will occur.
//
// Not all objects must be explicitly released here as most will be
// automatically freed when this object is released.
//

@Override
public void disposeOfAllResources()
{


}//end of RS232Link::disposeOfAllResources
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::setPortOptions
//
/**
 * Sets various options for comPort such as timeouts, baud rate, stop bit, parity, flow control.
 *
 * @param pBaudRate		the baud rate for the serial com port
 *
 */

void setPortOptions(int pBaudRate)
{

	comPort.setComPortTimeouts(SerialPort.TIMEOUT_NONBLOCKING, 0, 0);

	comPort.setComPortParameters(pBaudRate, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);

	comPort.setFlowControl(SerialPort.FLOW_CONTROL_DISABLED);

}// end of RS232Link::setPortOptions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::getInputStream
//
/**
 * Returns an InputStream for the serial port.
 *
 * @return InputStream for the serial port.
 *
*/

public InputStream getInputStream()
{

	return(comPort.getInputStream());

}// end of RS232Link::getInputStream
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::getOutputStream
//
/**
 * Returns an OutputStream for the serial port.
 *
 * @return OutputStream for the serial port.
 *
*/

public OutputStream getOutputStream()
{

	return(comPort.getOutputStream());

}// end of RS232Link::getOutputStream
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::sendString
//
// Sends a String to the remote device. A LINE_TERMINATOR value is sent as the last byte, so it
// should not be added to the String.
//

public void sendString(String pString)
{

	if(comPort == null){ return; }

	try {

		byte tempBuffer[] = pString.getBytes();

		comPort.writeBytes(tempBuffer, tempBuffer.length);

		outBuffer[0] = LINE_TERMINATOR;
		comPort.writeBytes(outBuffer, 1);

	} catch (Exception e) {
		logSevere(e.getMessage() + " - Error: 422");
	}

}// end of RS232Link::sendString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::sendBytes
//
/**
 *
 * Sends an array of bytes to the remote device.
 *
 * @param pBytes	array of bytes to be sent
 * @param pNumBytes	the number of bytes to send
 *
 * @return			The number of bytes successfully written, or -1 if there was an error writing
 *					to the port.
 *
 */

@Override
public int sendBytes(byte[] pBytes, int pNumBytes)
{

	if(comPort == null){ return(-1); }

	try {

		return(comPort.writeBytes(pBytes, pNumBytes));

	} catch (Exception e) {
		logSevere(e.getMessage() + " - Error: 536");
		return(-1);
	}

}// end of RS232Link::sendBytes
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::readStringResponse
//
// Calls readStringResponse(READ_BYTES_LIMIT).
//
// See readStringResponse(int pMaxNumBytes) for details.
//

public String readStringResponse()
{

	return(readStringResponse(READ_BYTES_LIMIT));

}// end of RS232Link::readStringResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::waitForData
//
/**
 * Waits until a specified number of bytes are available for reading. Will stop waiting if
 * specified time out duration is reached. Returns the number of bytes available via an IntParameter
 * object. Returns a success/error code.
 *
 * @param pNumBytesToWaitFor	the number of bytes to wait for before returning
 * @param pTimeOut				the maximum number of milliseconds to wait
 * @param pNumBytesAvailable	returns the number of bytes available or zero on time out or error
 * @return						success/error code: SUCCESS, PORT_CLOSED, PORT_ERROR, TIMEOUT
 *
 */

@Override
public int waitForData(long pNumBytesToWaitFor, long pTimeOut, IntParameter pNumBytesAvailable)
{

	pNumBytesAvailable.i = 0;

	if(comPort == null){ return (PORT_CLOSED); }

	long startTimeMS = System.currentTimeMillis();

    while ((System.currentTimeMillis() - startTimeMS) <= pTimeOut) {

		int numBytesAvailable = comPort.bytesAvailable​();

		if (numBytesAvailable == -1){ return(PORT_CLOSED); }

		if (numBytesAvailable >= pNumBytesToWaitFor){
			pNumBytesAvailable.i = numBytesAvailable;
			return(SUCCESS);
		}
	}

	return(TIMEOUT);

}// end of RS232Link::waitForData
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::readStringResponse
//
/**
 * Reads a character response from the remote device. Characters will be read until pMaxNumBytes
 * have been read, LINE_TERMINATOR is received to indicate the end of the response, or timeout
 * occurs. The response will be returned as a String. If timeout before any characters received, the
 * String will have length of 0. If timeout before LINE_TERMINATOR is received, the last character
 * of the String will not be LINE_TERMINATOR.
 *
 * timeout in milliseconds ~= READ_LOOP_TIMEOUT * READ_SLEEP_MS
 *
 * NOTE: this function is not normally used when data packets are used to communicate. It might
 * be used to receive an initial greeting, but that could also be done using packets.
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @return				the string read - terminated if terminator received before timeout or
 *						pMaxNumBytes characters read; empty string if timeout occurs before any
 *						characters received; null if an error occurs
 *
 */

public String readStringResponse(int pMaxNumBytes)
{

	if(comPort == null){ return(""); }

	int timeoutLoopCounter = 0;
	boolean done = false;

	inString.setLength(0);

	while (!done && timeoutLoopCounter++ < READ_LOOP_TIMEOUT){

		while(!done && comPort.bytesAvailable() != 0){

			comPort.readBytes(inByte, 1);

			inString.append((char)inByte[0]);

			timeoutLoopCounter = 0;

			if(inString.length() == pMaxNumBytes){ done = true; break; }

			if(inByte[0] == LINE_TERMINATOR){ done = true; }
		}

		if(!done) { threadSleep(READ_SLEEP_MS); }

	}

	return(inString.toString());

}// end of RS232Link::readStringResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::clearReceiveBuffer
//
/**
 * Clears the receive buffer by reading all bytes available. The function loops until the
 * number of available bytes is 0. Looping is done to help ensure that any bytes that might arrive
 * during the clearing are also cleared on the next pass. Also, the bytesAvailable function of some
 * libraries is an estimate, so looping until zero helps make sure the buffer is fully cleared.
 *
 * @return number of bytes cleared, -1 on error
 *
 */

@Override
public int clearReceiveBuffer()
{

	int numBytesAvailable;
	int numBytesCleared = 0;

	while((numBytesAvailable = comPort.bytesAvailable()) != 0){

		numBytesCleared += numBytesAvailable;

		byte[] inBytes = new byte[numBytesAvailable];

		comPort.readBytes(inBytes, numBytesAvailable);

	}

	return(numBytesCleared);

}// end of RS232Link::clearReceiveBuffer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::readBytesResponse
//
/**
 * Reads a byte block response from the remote device. Characters will be read until pMaxNumBytes
 * have been read or timeout occurs.
 *
 * The response will be returned as a byte array. If timeout before any characters received, the
 * byte array will be empty.
 *
 * The number of bytes read will be stored in class member variable numBytesRead.
 *
 * timeout in milliseconds ~= READ_LOOP_TIMEOUT * READ_SLEEP_MS
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @return				byte array containing bytes read from stream; may be empty
 *						the number of bytes read is stored in class member variable numBytesRead
 *
 */

@Override
public byte[] readBytesResponse(int pMaxNumBytes)
{

	if(comPort == null){ return(null); }

	int timeoutLoopCounter = 0;
	boolean done = false;
	byte[] inBytes = new byte[pMaxNumBytes];

	int i = 0;

	while (!done && timeoutLoopCounter++ < READ_LOOP_TIMEOUT){

		while(!done && comPort.bytesAvailable() != 0){

			comPort.readBytes(inByte, 1);

			inBytes[i++] = inByte[0];

			timeoutLoopCounter = 0;

			if(i == pMaxNumBytes){ done = true; break; }

		}

		if(!done) { threadSleep(READ_SLEEP_MS); }

	}

	numBytesRead = i;

	return(inBytes);

}// end of RS232Link::readBytesResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::readBytesResponse
//
/**
 * Reads a byte block response from the remote device. Characters will be read until pMaxNumBytes
 * have been read or pTimeOut milliseconds have passed.
 *
 * The response will be returned in pByteArray. If timeout before any characters received, the
 * byte array data will be invalid.
 *
 * The number of bytes read will be stored in pNumBytesRead.
 *
 * @param pMaxNumBytes	the maximum number of bytes to be read
 * @param pByteArray	a reference to an array for storing the bytes read
 * @param pNumBytesRead	returns the number of bytes available or zero on time out or error
 *
 * @return				success/error code: SUCCESS, PORT_CLOSED, PORT_ERROR, TIMEOUT
 *
 */

@Override
public int readBytesResponse(long pMaxNumBytes, byte[] pByteArray, IntParameter pNumBytesRead)
{

	if(comPort == null){ return(PORT_CLOSED); }

	pNumBytesRead.i = 0;

	int lNumBytesRead = comPort.readBytes​(pByteArray, pMaxNumBytes);

	if(lNumBytesRead == -1){ return(PORT_ERROR); }

	pNumBytesRead.i = lNumBytesRead;

	return(SUCCESS);

}// end of RS232Link::readBytesResponse
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::convertBytesToCommaDelimitedDecimalString
//
// Returns the bytes in pBuffer as a comma-delimited string of decimal numbers.
//

public String convertBytesToCommaDelimitedDecimalString(byte[] pBuffer)
{

	inString.setLength(0);

	for(byte b : pBuffer){
		inString.append(b);
		inString.append(',');
	}

	return(inString.toString());

}// end of RS232Link::convertBytesToCommaDelimitedDecimalString
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::getListOfSerialComPorts
//
// Retrieves from the operating system a list of the available serial com ports.
//
// Returns an array of SerialPort objects.
//

public SerialPort[] getListOfSerialComPorts()
{

	return(SerialPort.getCommPorts());

}// end of RS232Link::getListOfSerialComPorts
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::getListOfSerialComPortNames
//
// Retrieves from the operating system a list of names of the available serial com ports. These
// will be in the form of "COM1", "COM25", etc.
//
// Returns a ListArray<String> object.
//

public ArrayList<String> getListOfSerialComPortNames()
{

	SerialPort[] serialPorts = getListOfSerialComPorts();

	ArrayList<String> names = new ArrayList<>(serialPorts.length);

	for(SerialPort port : serialPorts){ names.add(port.getSystemPortName()); }

	return(names);

}// end of RS232Link::getListOfSerialComPortNames
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::getIndexOfSerialPortSpecifiedByName
//
// Finds the index of the serial port with a name matching pName
//
// Returns the index of the port.
// If no match is found, returns -1.
//

protected int getIndexOfSerialPortSpecifiedByName(String pName)
{

	ArrayList<String> ports =  getListOfSerialComPortNames();

	if(ports.isEmpty()){
		tsLog.appendLine("no serial port found!");
		return(-1);
	}

	if(!ports.contains(pName)){
		tsLog.appendLine(pName + " serial port not found!");
		return(-1);
	}

	return(ports.indexOf(pName));

}// end of RS232Link::getIndexOfSerialPortSpecifiedByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::sendInitCommands
//

public void sendInitCommands(){



}// end of RS232Link::sendInitCommands
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232LinkStub::threadSleep
//
// Calls the Thread.sleep function. Placed in a function to avoid the
// "Thread.sleep called in a loop" warning -- yeah, it's cheezy.
//
// Returns true if InterruptedException caught...this usually means some other thread wants to
// shut down this thread.
//

public boolean threadSleep(int pSleepTime)
{

    try {Thread.sleep(pSleepTime);} catch (InterruptedException e) { return(true); }

	return(false);

}//end of RS232LinkStub::threadSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// RS232Link::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of RS232Link::logSevere
//-----------------------------------------------------------------------------

	@Override
	public void open() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public void close() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public byte readByte() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public byte[] readBytes(int pNumBytes) {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

	@Override
	public byte peek() {
		throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
	}

}//end of class RS232Link
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
