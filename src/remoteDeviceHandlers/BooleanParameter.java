/******************************************************************************
* Title: BooleanParameter.java
* Author: Mike Schoonover
* Date: 11/16/21
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

//-----------------------------------------------------------------------------
// Class BooleanParameter
//
/**
 * This class encapsulates an int primitive. It is useful for passing as a parameter so that a
 * value can be returned by a method. Unlike the Integer class, this class is mutable.
 *
 * @author Mike Schoonover
 *
 */

public class BooleanParameter{

	public boolean b;

//-----------------------------------------------------------------------------
// BooleanParameter::BooleanParameter (constructor)
//
/**
 * Constructor which sets the int primitive to zero.
 *
 */

public BooleanParameter()
{

	b = false;

}//end of BooleanParameter::BooleanParameter (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BooleanParameter::BooleanParameter (constructor)
//
/**
 * Constructor which accepts an initial value.
 *
 * @param pValue	the initial value for the boolean primitive
 *
 */

public BooleanParameter(boolean pValue)
{

	b = pValue;

}//end of BooleanParameter::BooleanParameter (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BooleanParameter::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

public void init()
{


}// end of BooleanParameter::init
//-----------------------------------------------------------------------------

}//end of class BooleanParameter
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
