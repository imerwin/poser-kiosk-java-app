/******************************************************************************
* Title: PoseRecognizerHandler.java
* Author: Mike Schoonover
* Date: 07/12/21
*
* Purpose:
*
* This class handles interfacing with the Pose Recognizer server.
*
* This class requires EthernetLink and related objects which it uses to communicate with the device.
*
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

import controller.EventHandler;
import controller.EventInfo;
import java.net.InetAddress;
import java.nio.charset.Charset;
import java.util.ArrayList;
import controller.LoggerBasic;
import controller.ProcessHandler;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.geom.Point2D;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import controller.PlotInterface;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class Gesture
//
/**
 * This class handles information about a gesture, such as an identifying number and a text name.
 *
 *
 */

class Gesture{

	public int id = Integer.MAX_VALUE;
	public String name = "";

}//end of class Gesture
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class PoseRecognizerHandler
//
/**
 *
 * This class handles interfacing with the Pose Recognizer server.
 *
 * The class requires EthernetLink and related objects which it uses to communicate with the
 * Wheel Controller.
 *
 */

public class PoseRecognizerHandler extends Object implements ProcessHandler{

	String processName;

	EventHandler eventHandler;

	protected final DecimalFormat decimalFormat1Dec = new DecimalFormat("#.0");

	int gestureState;

	long taskEndTimeMS;

	long handWaveEndTimeMS;

	String gestureUser = "";

	String speakPhrase = "";

	String lastPersonLoggedIn;

	int helloPhraseSeriesTracker;

	public static final int BACKGROUND_MODE = 0;
	public static final int ACTIVE = 1;
	public static final int WAITING_ON_42 = 2;
	public static final int WAITING_ON_ID = 3;

	ArrayList<String> displayMsgs;

	@Override
	public String getProcessName(){ return(processName); }

	int remoteDeviceIdentifier;

    String simulationDataSourceFilePath = "";

    LoggerBasic tsLog;

	PacketTool packetTool;

	EthernetLink ethernetLink;

	InetAddress ipAddr;
	String ipAddrS;
	int port;

	PlotInterface graphicsPanel;

	int graphicsPanelWidth;
	int graphicsPanelHeight;

	double oakDataScale;

	boolean isWavingHand;

	final Charset UTF8_CHARSET = Charset.forName("UTF-8");

	boolean waitingForACKPkt;

	BufferFilter<Integer> leftGestureBuffer;
	BufferFilter<Integer> rightGestureBuffer;

	int AVATAR_X = 950;
	int AVATAR_Y = 600;
	double AVATAR_SCALE = 1.0;
	int AVATAR_HEAD_DIAMETER = 500;
	int avatarX;

	@Override
	public void resetProcess(boolean pStopNVLCPlayer){};

	@Override
	public void stopProcess(){};

	public void setWaitingForACKPkt(boolean pState) { waitingForACKPkt = pState; }

	public boolean getWaitingForACKPkt() { return(waitingForACKPkt); }

	public static final String OBJECT_NAME = "PoseRecognizerHandler";

	public static final int OAK_DATA_MAX_X = 1152;
	public static final int OAK_DATA_MAX_Y = 648;

	public static final int LEFT = 0;
	public static final int RIGHT = 1;

	public static final double LANDMARK_CIRCLE_RADIUS = 5.0;
	public static final double LANDMARK_CIRCLE_RADIUS_HALF = LANDMARK_CIRCLE_RADIUS / 2;

	public static final double PALM_BASE_LANDMARK_CIRCLE_RADIUS = 10.0;
	public static final double PALM_BASE_LANDMARK_CIRCLE_RADIUS_HALF =
															PALM_BASE_LANDMARK_CIRCLE_RADIUS / 2;


	public static final int NUM_DATA_BYTES_FOR_HAND_IN_PACKET = 104;

	public static final int GESTURE_ZERO = 0;
	public static final int GESTURE_ONE = 1;
	public static final int GESTURE_TWO = 2;
	public static final int GESTURE_THREE = 3;
	public static final int GESTURE_FOUR = 4;
	public static final int GESTURE_FIVE = 5;

	public static final int GESTURE_HORNS = 50;
	public static final int GESTURE_LITTLE = 51;
	public static final int GESTURE_THUMB_UP = 52;
	public static final int GESTURE_THUMB_SIDE = 53;

	public static final int GESTURE_42_WAIT_TIME = 10000;
	public static final int GESTURE_ID_WAIT_TIME = 10000;
	public static final int HAND_WAVE_TIME = 3000;

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::PoseRecognizerHandler (constructor)
//

public PoseRecognizerHandler(EventHandler pEventHandler, LoggerBasic pThreadSafeLogger)
{

	eventHandler = pEventHandler;

	leftGestureBuffer = new BufferFilter<>(6);
	leftGestureBuffer.init(Integer.MAX_VALUE);

	rightGestureBuffer = new BufferFilter<>(6);
	rightGestureBuffer.init(Integer.MAX_VALUE);

	displayMsgs = new ArrayList<>(5);

	tsLog = pThreadSafeLogger;

	gestureState = BACKGROUND_MODE;

	lastPersonLoggedIn = "";

	helloPhraseSeriesTracker = 0;

	isWavingHand = false;

	avatarX = AVATAR_X;

}//end of PoseRecognizerHandler::PoseRecognizerHandler (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::init
//
/**
 * Initializes the object.Must be called immediately after instantiation.
 *
 * @param pRemoteDeviceIdentifier the identifier on the network of the remote device
 * @param pPacketTool			used to create outgoing and parse incoming data packets.
 * @param pEthernetLink			used to communicate with the remote device
 *
 */

public void init(int pRemoteDeviceIdentifier, PacketTool pPacketTool, EthernetLink pEthernetLink)
{

	setupHands();

	processName = "Pose Recognizer Server Handler";

	remoteDeviceIdentifier = pRemoteDeviceIdentifier;

	packetTool = pPacketTool; ethernetLink = pEthernetLink;

	setWaitingForACKPkt(false);

}// end of PoseRecognizerHandler::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::setGraphicsPanel
//
/**
 * Sets the reference to a PlotInterface object for drawing graphics. If no such panel is available
 * or current panel becomes unavailable pGraphicsPanel should be set null by client code.
 *
 * @param pGraphicsPanel	a reference to a PlotInterface on which graphics can be drawn, should
 *							be null if no panel is currently available or if the current panel
 *							becomes unavailable
 *
 */

public void setGraphicsPanel(PlotInterface pGraphicsPanel)
{

	graphicsPanel = pGraphicsPanel;

	if(graphicsPanel == null){ return; }

	Rectangle bounds = graphicsPanel.getBounds();

	graphicsPanelWidth = bounds.width;
	graphicsPanelHeight = bounds.height;

	double xRatio = (double)OAK_DATA_MAX_X / (double)graphicsPanelWidth;
	double yRatio = (double)OAK_DATA_MAX_Y / (double)graphicsPanelHeight;

	oakDataScale = Math.max(xRatio, yRatio);

}//end of PoseRecognizerHandler::setGraphicsPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::setupBodyLines
//
/**
 *
 * Creates and initializes the list body lines.
 *
 */

void setupHands()
{

}// end of PoseRecognizerHandler::setupHands
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::connect
//
/**
 * Establishes a connection with the remote device.
 *
 * @param pIPAddr	InetAddress of remote
 * @param pPort		port to be connected on remote
 * @return			1 on success, -1 on failure to connect
 *
 */

public int connect(InetAddress pIPAddr, int pPort)
{

	ipAddr = pIPAddr; ipAddrS = ipAddr.toString(); port = pPort;

	tsLog.appendLine("");
	tsLog.appendLine("connecting to Pose Detector at " + ipAddrS + ":" + pPort);

	int status = ethernetLink.connectToRemote(ipAddr, port);

	if(status == -1) { return(status); }

	packetTool.setStreams(ethernetLink.getInputStream(), ethernetLink.getOutputStream());

	packetTool.sendBytes(remoteDeviceIdentifier, PacketTypeEnum.GET_DEVICE_INFO, (byte)0x00);

	return(status);

}//end of PoseRecognizerHandler::connect
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::disconnect
//
/**
 * Closes the connection with the remote device.
 *
 * @return	true if port successfully closed, false if not
 *
 */

public boolean disconnect()
{

	return(ethernetLink.closeConnectionToRemote());

}//end of PoseRecognizerHandler::disconnect
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PoseRecognizerHandler::process
//
/**
 * Handles communications and actions regarding the remote device. This function should be called
 * often to allow continuous processing.
 *
 * @return	true if process is to be kept active, false if process is to be halted
 *
 */

@Override
public boolean process()
{

	handleCommunications();

	decoratePoserDisplay();

	return(true);

}//end of PoseRecognizerHandler::process
//--------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::decoratePoserDisplay
//
/**
 *
 * Draws various sprites on the Poser Display panel.
 *
 */

void decoratePoserDisplay()
{

	graphicsPanel.clearPanel(Color.BLACK);

	drawRightCircleMenu();

	drawPoseAvatar();

	graphicsPanel.applyBufferedImage();

}// end of PoseRecognizerHandler::decoratePoserDisplay
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::drawPoseAvatar
//
/**
 *
 * Draws an example Pose Avatar stick figure.
 *
 */

void drawPoseAvatar()
{

	if(graphicsPanel == null){ return; }

	boolean fillCircle = false;
	float lineWidth = 5;

	graphicsPanel.setColor(Color.BLUE);
	graphicsPanel.drawEllipseDouble(
			avatarX, AVATAR_Y, AVATAR_HEAD_DIAMETER, AVATAR_HEAD_DIAMETER, fillCircle, lineWidth);

	avatarX--;

}// end of PoseRecognizerHandler::drawPoseAvatar
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::decoratePoserDisplay
//
/**
 *
 * Draws the right side circle menu.
 *
 */

void drawRightCircleMenu()
{

	int RIGHT_MENU_X = 1900;
	int RIGHT_MENU_Y = 100;
	int RIGHT_MENU_VERTICAL_SPACING = 350;

	int MENU_CIRCLE_DIAMETER = 200;

	boolean fillCircle = true;
	float lineWidth = 5;

	int menuCircleY;

	menuCircleY = RIGHT_MENU_Y;
	graphicsPanel.setColor(Color.RED);
	graphicsPanel.drawEllipseDouble(RIGHT_MENU_X, menuCircleY,
							MENU_CIRCLE_DIAMETER, MENU_CIRCLE_DIAMETER, fillCircle, lineWidth);

	menuCircleY += RIGHT_MENU_VERTICAL_SPACING;
	graphicsPanel.setColor(Color.BLUE);
	graphicsPanel.drawEllipseDouble(RIGHT_MENU_X, menuCircleY,
							MENU_CIRCLE_DIAMETER, MENU_CIRCLE_DIAMETER, fillCircle, lineWidth);

	menuCircleY += RIGHT_MENU_VERTICAL_SPACING;
	graphicsPanel.setColor(Color.GREEN);
	graphicsPanel.drawEllipseDouble(RIGHT_MENU_X, menuCircleY,
							MENU_CIRCLE_DIAMETER, MENU_CIRCLE_DIAMETER, fillCircle, lineWidth);

	menuCircleY += RIGHT_MENU_VERTICAL_SPACING;
	graphicsPanel.setColor(Color.CYAN);
	graphicsPanel.drawEllipseDouble(RIGHT_MENU_X, menuCircleY,
							MENU_CIRCLE_DIAMETER, MENU_CIRCLE_DIAMETER, fillCircle, lineWidth);

	menuCircleY += RIGHT_MENU_VERTICAL_SPACING;
	graphicsPanel.setColor(Color.YELLOW);
	graphicsPanel.drawEllipseDouble(RIGHT_MENU_X, menuCircleY,
							MENU_CIRCLE_DIAMETER, MENU_CIRCLE_DIAMETER, fillCircle, lineWidth);

	menuCircleY += RIGHT_MENU_VERTICAL_SPACING;
	graphicsPanel.setColor(Color.MAGENTA);
	graphicsPanel.drawEllipseDouble(RIGHT_MENU_X, menuCircleY,
							MENU_CIRCLE_DIAMETER, MENU_CIRCLE_DIAMETER, fillCircle, lineWidth);


}// end of PoseRecognizerHandler::drawRightCircleMenu
//-----------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PoseRecognizerHandler::handleCommunications
//
/**
 * Handles a packet
 *
 */

void handleCommunications()
{

	boolean packetReady = packetTool.checkForPacketReady();

	if(packetReady){ handlePacket(); }

}//end of PoseRecognizerHandler::handleCommunications
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PoseRecognizerHandler::handlePacket
//
/**
 * Handles a packet received from the remote device.
 *
 */

void handlePacket()
{

	PacketTypeEnum pktType = packetTool.getPktType();

	switch (pktType) {

		case ACK_PKT : setWaitingForACKPkt(false); return;

		case LOG_MESSAGE : logMessageFromPacket(); return;

		case HAND_GESTURE_DATA :
			handleHandGestureDataPkt();
			handleDrawing();
			if(graphicsPanel != null){ graphicsPanel.applyBufferedImage(); }
			return;

		default:

			return;
		}

}//end of PoseRecognizerHandler::handlePacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PoseRecognizerHandler::drawDisplayMessages
//
/**
 * Draws all messages in the displayMsgs list on the graphics panel. If graphicsPanel is null or
 * the list is empty, returns immediately.
 *
 */

void drawDisplayMessages()
{

	if(graphicsPanel == null ) { return; }

	if(displayMsgs.isEmpty()){ return; }

	double x = 20, y = 20;

	graphicsPanel.setColorRGBAlpha(0, 255, 0, 255);
	graphicsPanel.setColorRGBAlpha(0, 0, 255, 255);	//debug mks


	for(String msg: displayMsgs){
		graphicsPanel.drawText(msg, x, y);
		y += 20;
	}

}//end of PoseRecognizerHandler::drawDisplayMessages
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PoseRecognizerHandler::logMessageFromPacket
//
/**
 * Logs a message received in a packet from a remote device. The message C-string is
 * extracted from PacketTool.pktDataBuffer and converted to a String. The C-string should have
 * a null terminator in the packet which is ignored when converting to a string.
 *
 */

public void logMessageFromPacket()
{

	int numPktDataBytes = packetTool.getNumPktDataBytes();

	byte[] buf = packetTool.getPktDataBuffer();

	String message = new String(buf, 0, numPktDataBytes-1);

	tsLog.appendLine(message);

}//end of PoseRecognizerHandler::logMessageFromPacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PoseRecognizerHandler::handleDrawing
//
/**
 * Handles all drawing functions.
 *
 */

public void handleDrawing()
{

	if (graphicsPanel == null){ return; }

	graphicsPanel.clearPanel(Color.BLACK);

	drawDisplayMessages();


}//end of PoseRecognizerHandler::handleDrawing
//--------------------------------------------------------------------------------------------------



//--------------------------------------------------------------------------------------------------
// PoseRecognizerHandler::handleHandGestureDataPkt
//
/**
 * Extracts data from packet for hand gesture data and processes it.
 *
 * The packet data contains various x/y locations of the key points of the palm and digits as well
 * as information about whether each digit is extended or retracted.
 *
 * If there are not enough bytes as expected for at least one hand, the function returns without
 * extracting any bytes.
 *
 * If there are not enough bytes as expected for a second hand, the function returns without
 * extracting any bytes more than already extracted for the first hand.
 *
 * Leaving the bytes in the buffer will force a resync on the next packet, but this is probably
 * the preferred behavior. Attempting to remove bytes due to an unexpected occurrence is very
 * likely to wipe out a following packet as the number of bytes reported in the errant header is
 * not trustworthy.
 *
 *
 *  The data block in the packet is a series of signed short ints (16 bits) as follows:
 *
 *	byte	name			purpose
 *
 *	(first hand starts at byte 0)
 *
 *	  0:1    valid data      0 if data is invalid, 1 if data is valid
 *	  2:3    hand width      0 if data invalid, width of the bounding square around hand if valid
 *
 *	  4:5    thumb state     unknown/retracted/extended pointing angle state of the thumb
 *	  6:7    index state     unknown/retracted/extended pointing angle state of the index finger
 *	  8:9    middle state    unknown/retracted/extended pointing angle state of the middle finger
 *	 10:11   ring state      unknown/retracted/extended pointing angle state of the ring finger
 *	 12:13   little state    unknown/retracted/extended pointing angle state of the little finger
 *	 14:15   which hand      0 for left hand, 1 for right; only works with palms facing camera
 *
 *	 16:17   x label anchor  x coord anchor point useful for positioning labels drawn around hand
 *	 18:19   y label anchor  y coord anchor point useful for positioning labels drawn around hand
 *
 *	 20:21   x of palm base  x coordinate of the base of the palm near to the wrist
 *	 22:23   y of palm base  y coordinate of the base of the palm near to the wrist
 *
 *	 24:25   x0 of thumb     x coordinate of point 0 of the thumb (base)
 *	 26:27   y0 of thumb     y coordinate of point 0 of the thumb (base)
 *	 28:29   x1 of thumb     x coordinate of point 1 of the thumb
 *	 30:31   y1 of thumb     y coordinate of point 1 of the thumb
 *	 32:33   x2 of thumb     x coordinate of point 2 of the thumb
 *	 34:35   y2 of thumb     y coordinate of point 2 of the thumb
 *	 36:37   x3 of thumb     x coordinate of point 3 of the thumb (tip)
 *	 38:39   y3 of thumb     y coordinate of point 3 of the thumb (tip)
 *
 *	 40:41   x0 of index     x coordinate of point 0 of the index (base)
 *	 42:43   y0 of index     y coordinate of point 0 of the index (base)
 *	 44:45   x1 of index     x coordinate of point 1 of the index
 *	 46:47   y1 of index     y coordinate of point 1 of the index
 *	 48:49   x2 of index     x coordinate of point 2 of the index
 *	 50:51   y2 of index     y coordinate of point 2 of the index
 *	 52:53   x3 of index     x coordinate of point 3 of the index (tip)
 *	 54:55   y3 of index     y coordinate of point 3 of the index (tip)
 *
 *	 56:57   x0 of middle    x coordinate of point 0 of the middle (base)
 *	 58:59   y0 of middle    y coordinate of point 0 of the middle (base)
 *	 60:61   x1 of middle    x coordinate of point 1 of the middle
 *	 62:63   y1 of middle    y coordinate of point 1 of the middle
 *	 64:65   x2 of middle    x coordinate of point 2 of the middle
 *	 66:67   y2 of middle    y coordinate of point 2 of the middle
 *	 68:69   x3 of middle    x coordinate of point 3 of the middle (tip)
 *	 70:71   y3 of middle    y coordinate of point 3 of the middle (tip)
 *
 * 	 72:73   x0 of ring      x coordinate of point 0 of the ring (base)
 * 	 74:75   y0 of ring      y coordinate of point 0 of the ring (base)
 *	 76:77   x1 of ring      x coordinate of point 1 of the ring
 *	 78:79   y1 of ring      y coordinate of point 1 of the ring
 *	 80:81   x2 of ring      x coordinate of point 2 of the ring
 *	 82:83   y2 of ring      y coordinate of point 2 of the ring
 *	 84:85   x3 of ring      x coordinate of point 3 of the ring (tip)
 *	 86:87   y3 of ring      y coordinate of point 3 of the ring (tip)
 *
 *	 88:89   x0 of little    x coordinate of point 0 of the little (base)
 *	 90:91   y0 of little    y coordinate of point 0 of the little (base)
 *	 92:93   x1 of little    x coordinate of point 1 of the little
 *	 94:95   y1 of little    y coordinate of point 1 of the little
 *	 96:97   x2 of little    x coordinate of point 2 of the little
 *	 98:99   y2 of little    y coordinate of point 2 of the little
 *	100:101  x3 of little    x coordinate of point 3 of the little (tip)
 *	102:103  y3 of little    y coordinate of point 3 of the little (tip)
 *
 *	(next hand starts at byte 104)
 *
 *	104:105  valid data      0 if data is invalid, 1 if data is valid
 *	106:107  hand width      0 if data invalid, width of the bounding square around hand if valid
 *	...
 *	...                 { duplicate of first hand...refer to above }
 *	...
 *
 *
 */

public void handleHandGestureDataPkt()
{

//	hand1.setDataValid(Hand.DATA_INVALID);


	int numPktDataBytes = packetTool.getNumPktDataBytes();

	int i = 0;

	if(numPktDataBytes >= NUM_DATA_BYTES_FOR_HAND_IN_PACKET) {
//		i = extractAllDataForAHandFromPacket(i, hand1);
	}

	if ((numPktDataBytes - i) >= NUM_DATA_BYTES_FOR_HAND_IN_PACKET){
//		extractAllDataForAHandFromPacket(i, hand2);
	}

//	hands.clear();

}//end of PoseRecognizerHandler::handleHandGestureDataPkt
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PoseRecognizerHandler::extractAllDataForAHandFromPacket
//
/**
 * Parses all data for Hand pHand from a packet. This includes data such as digit states
 * (extended/retracted), landmarks for the hand and its digits, data valid flags, etc. The first
 * byte to be read from the packet is indexed by pStartIndex.
 *
 * The new index is returned which will point to the position in the buffer after the extracted
 * data.
 *
 * @param pStartIndex		index of the first byte of the first integer
 * @param pHand				the Hand for which the data is to be parsed
 *
 * @return					the updated index pointing to the next position in the buffer
 *
 */

public int extractAllDataForAHandFromPacket(int pStartIndex /*, Hand pHand*/)
{

	int i = pStartIndex;
	double x, y;


	// parse and store the data valid flag
//	pHand.setDataValid(packetTool.parseShortIntegerFromPacket(i)); i += 2;

	// parse and store the width of the bounding square containing the hand
//	pHand.setHandBoundingBoxWidth(packetTool.parseShortIntegerFromPacket(i)); i += 2;

	// parse states of all digits (extended/retracted)

//	pHand.setDigitState(Hand.THUMB_DIGIT, packetTool.parseShortIntegerFromPacket(i)); i += 2;
//	pHand.setDigitState(Hand.INDEX_DIGIT, packetTool.parseShortIntegerFromPacket(i)); i += 2;
//	pHand.setDigitState(Hand.MIDDLE_DIGIT, packetTool.parseShortIntegerFromPacket(i)); i += 2;
//	pHand.setDigitState(Hand.RING_DIGIT, packetTool.parseShortIntegerFromPacket(i)); i += 2;
//	pHand.setDigitState(Hand.LITTLE_DIGIT, packetTool.parseShortIntegerFromPacket(i)); i += 2;

	int whichHand = packetTool.parseShortIntegerFromPacket(i); i += 2;

	// parse and store the label anchor point
	x = packetTool.parseShortIntegerFromPacket(i); i += 2;
	y = packetTool.parseShortIntegerFromPacket(i); i += 2;
//	pHand.setLabelAnchorPoint(x,y);

	// parse and store palm base point landmark (near the wrist)
	x = packetTool.parseShortIntegerFromPacket(i); i += 2;
	y = packetTool.parseShortIntegerFromPacket(i); i += 2;
//	pHand.setPalmBasePoint(x,y);

	// parse landmarks for all digits
//	i = parseAHandsDigitsLandmarkXYIntsFromPacketAndAddToHandLandmarks(i, pHand);

	return(i);

}//end of PoseRecognizerHandler::extractAllDataForAHandFromPacket
//--------------------------------------------------------------------------------------------------

//--------------------------------------------------------------------------------------------------
// PoseRecognizerHandler::parseXYIntsFromPacketAndAddToList
//
/**
 * Parses x and y coordinates from a packet of hand gesture data and stores them as a
 * Point2D.Double in pList. The first byte to be read from the packet is indexed by pStartIndex.
 *
 * Each coordinate is a two byte unsigned int, so four bytes are read from the buffer.
 *
 * The new index is returned which will point to the position in the buffer after the extracted
 * data.
 *
 * @param pStartIndex		index of the first byte of the first integer
 * @param pLandMarkPoints	the list to which the points are to be added
 *
 * @return					the updated index pointing to the next position in the buffer
 *
 */

int parseXYIntsFromPacketAndAddToList(int pStartIndex, ArrayList<Point2D.Double> pLandmarkPoints)
{

	int i = pStartIndex;

	int x = packetTool.parseShortIntegerFromPacket(i); i += 2;
	int y = packetTool.parseShortIntegerFromPacket(i); i += 2;

	pLandmarkPoints.add(new Point2D.Double(x,y));

	return(i);

}//end of PoseRecognizerHandler::parseXYIntsFromPacketAndAddToList
//--------------------------------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::waitSleep
//
// Sleeps for pTime milliseconds.
//

public void waitSleep(int pTime)
{

    try {Thread.sleep(pTime);} catch (InterruptedException e) { }

}//end of PoseRecognizerHandler::waitSleep
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::logStatus
//
// Writes various status and error messages to the log window.
//

public void logStatus(LoggerBasic pLogger)
{

}//end of PoseRecognizerHandler::logStatus
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::logSevere
//
// Logs pMessage with level SEVERE using the Java logger.
//

void logSevere(String pMessage)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage);

}//end of PoseRecognizerHandler::logSevere
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoseRecognizerHandler::logStackTrace
//
// Logs stack trace info for exception pE with pMessage at level SEVERE using
// the Java logger.
//

void logStackTrace(String pMessage, Exception pE)
{

    Logger.getLogger(getClass().getName()).log(Level.SEVERE, pMessage, pE);

}//end of PoseRecognizerHandler::logStackTrace
//-----------------------------------------------------------------------------

}//end of class PoseRecognizerHandler
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
