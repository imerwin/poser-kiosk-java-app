/******************************************************************************
* Title: Communications.java
* Author: Mike Schoonover
* Date: 11/06/20
*
* Purpose:
*
* This file contains the interface definition for Communications.  This interface provides basic
* read/write/query functions applicable to a communications port such as a Stream, serial port,
* etc.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package remoteDeviceHandlers;

//-----------------------------------------------------------------------------

import controller.LoggerBasic;
import java.net.InetAddress;

//-----------------------------------------------------------------------------
// interface Communications
//
// Defines functions to allow different objects to call functions in other
// objects.
//

public interface Communications {

	public void init(LoggerBasic pThreadSafeLogger);

	public int connectToRemote(String pSelectedCOMPort, int pBaudRate);

	public int connectToRemote(InetAddress pIPAddr, int pPort);

	public boolean closeConnectionToRemote();

	public void open();

	public void close();

	public boolean getIsConnected();

	public int clearReceiveBuffer();

	public boolean setDTR();

	public boolean clearDTR();

	public int waitForData(long pNumBytesToWaitFor, long pTimeOut, IntParameter pNumBytesAvailable);

	public int sendBytes(byte[] pBytes, int pNumBytes);

	public byte readByte();

	public byte[] readBytesResponse(int pMaxNumBytes);

	public int readBytesResponse(long pMaxNumBytes, byte[] pByteArray, IntParameter pNumBytesRead);

	public byte peek();

	public byte[] readBytes(int pNumBytes);

	public void disposeOfAllResources();

}//end of interface Communications
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
