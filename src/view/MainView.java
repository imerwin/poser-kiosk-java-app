/******************************************************************************
* Title: MainView.java
* Author: Mike Schoonover
* Date: 3/12/13
*
* Purpose:
*
* This class is the Main View in a Model-View-Controller architecture.
* It creates and handles all GUI components.
* It knows about the Controller.
*
* There may be many classes in the view package which handle different aspects
* of the GUI.
*
* All GUI control events, including Timer events are caught by this object
* and passed on to the "Controller" object pointed by the class member
* "eventHandler" for final handling.
*
*/

//-----------------------------------------------------------------------------

package view;

//-----------------------------------------------------------------------------

import controller.EventHandler;
import controller.SwissArmyKnife;
import controller.Xfer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.awt.font.TextAttribute;
import java.awt.geom.Rectangle2D;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;
import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import mksystems.mswing.ComboBoxPanel;
import mksystems.mswing.SpinnerPanel;
import mksystems.mswing.TextFieldPanel;
import model.PersistentDataElement;
import model.PersistentDataSet;
import controller.EventInfo;
import controller.PlotInterface;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.Window;
import java.io.File;
import javax.swing.JComboBox;
import mksystems.mswing.MFloatSpinner;
import main.Main;
import settings.Settings;
import toolkit.Tools;

//-----------------------------------------------------------------------------
// class MainView
//

public class MainView implements
        ActionListener, WindowListener, ChangeListener, MouseWheelListener, MouseListener,
		MenuListener
{


    protected JFrame mainFrame;
    protected JPanel mainPanel;

	protected JPanel settingsPanel,	settingsColumn1, settingsColumn2, settingsColumn3;
	protected JPanel resultsPanel,	resultsColumn1, resultsColumn2, resultsColumn3;
	protected JPanel controlsPanel;

	JLabel dataNotSavedLabel;

	DecimalFormat decimalFormat3Dec;

	Settings settings;

	protected MainTitleBarMenu mainMenu;

	ComboBoxPanel comPortSelector;

//    protected Status statusWindow;

//    protected GuiUpdater guiUpdater;
    protected Log log;
    protected ThreadSafeLogger tsLog;
    protected Help help;

	protected javax.swing.Timer mainTimer;
	protected javax.swing.Timer animationTimer;
	protected javax.swing.Timer blinkTimer;
	protected javax.swing.Timer announcementTimer;

    protected final EventHandler eventHandler;

    protected Font blackSmallFont, redSmallFont;
    protected Font redLargeFont, greenLargeFont, yellowLargeFont, blackLargeFont;

    Dimension totalScreenSize, usableScreenSize;

	static final String HELP_FILENAME = "Help Files" + File.separator + "Spud Help.html";

	private PoserDisplay poserDisplay;
	public PoserDisplay getPoserDisplay(){ return(poserDisplay); }

	private PlotInterface poserCanvas;

	// NOTE:
	//
	// Raspberry Pi GUI forces Y position to 20 if it is set to 0. This forces window below the
	// taskbar. So it is explicitly set to 20 here for clarity. On Windows, this simply sets the
	// window 20 pixels down from the top of the display.

	final int FRAME_X_POS = 0;
	final int FRAME_Y_POS = 20;

	static final int EYE_WHITE_LEFT_X_POSITION = 0;
	static final int EYE_WHITE_LEFT_Y_POSITION = 50;
	static final int EYE_WHITE_LEFT_WIDTH = 240;
	static final int EYE_WHITE_LEFT_HEIGHT = 410;

	static final int EYE_WHITE_RIGHT_X_POSITION = 240;
	static final int EYE_WHITE_RIGHT_Y_POSITION = 50;
	static final int EYE_WHITE_RIGHT_WIDTH = 240;
	static final int EYE_WHITE_RIGHT_HEIGHT = 410;

	static final int AVERAGE_BLINK_PERIOD = 3000;

/* Example of using an Action

class F1ScopeFreezeAction extends AbstractAction {

	@SuppressWarnings("OverridableMethodCallInConstructor")
    public F1ScopeFreezeAction(String pName, Integer pMnemonic) {
        super(pName);
        putValue(SHORT_DESCRIPTION, "Toggle Scope Display Frozen State");
        putValue(MNEMONIC_KEY, pMnemonic);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        eventHandler.handleEvent(new EventInfo("Toggle Scope Display Frozen State", ""));
    }

}//end of class F1ScopeFreezeAction

*/

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::MainView (constructor)
//

public MainView(EventHandler pEventHandler, Settings pSettings)
{

    eventHandler = pEventHandler;
    settings = pSettings;

	poserDisplay = null;

}//end of MainView::MainView (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

public void init()
{

    setupMainFrame();

    setupHotKeys();

    decimalFormat3Dec = new DecimalFormat("0.000");

    //create an object to handle thread safe updates of GUI components
//    guiUpdater = new GuiUpdater(mainFrame);
//    guiUpdater.init();

    //add a menu to the main form, passing this as the action listener
    mainMenu = new MainTitleBarMenu(this, this); mainMenu.init();
    mainFrame.setJMenuBar(mainMenu);

    //create various fonts for use by the program
    createFonts();

    //create user interface: buttons, displays, etc.
    setupGui();

    //arrange all the GUI items
    mainFrame.pack();

    //display the main frame
    mainFrame.setVisible(true);

    int xPos = mainFrame.getWidth() + (int)mainFrame.getLocation().getX();
    int yPos = (int)mainFrame.getLocation().getY() + 315;

    // statusWindow = new Status(mainFrame, xPos, yPos); statusWindow.init();

}// end of MainView::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainController::disposeOfAllResources
//
/**
 * Releases all objects created which may have back references. This is
 * necessary because some of those objects may hold pointers back to a parent
 * object (such as Java Listeners and such). These back references will prevent
 * the parent object from being released and a memory leak will occur.
 *
 * Not all objects must be explicitly released here as most will be
 * automatically freed when this object is released.
 *
 */

public void disposeOfAllResources()
{

	if (poserDisplay != null) { poserDisplay.disposeOfAllResources(); }

}//end of MainController::disposeOfAllResources
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::closeMainWindow
//
// Closes the main window which will trigger the shut down process and exit the program.
//
// The main window is closed by dispatching a WINDOW_CLOSING event so that it functions the same
// as if the user clicked the "x" to close the window, thus invoking the same shutdown processes.
//
// Finds the active window before creating and dispatching a WINDOW_CLOSING event.

public void closeMainWindow()
{

	WindowEvent windowClosing = new WindowEvent(mainFrame, WindowEvent.WINDOW_CLOSING);
	mainFrame.dispatchEvent(windowClosing);

}// end of MainView::closeMainWindow
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::displayDialogWindow
//
// Displays a dialog window with text pMessage and title pTitle.
//
// The type of message is passed via pMessageType.
//

public void displayDialogWindow(String pMessage, String pTitle,
														Tools.DIALOG_MESSAGE_TYPES pMessageType)
{

	int messageType;

	switch(pMessageType){

		case INFORMATION : messageType = JOptionPane.INFORMATION_MESSAGE; break;

		case WARNING : messageType = JOptionPane.WARNING_MESSAGE; break;

		case ERROR : messageType = JOptionPane.ERROR_MESSAGE; break;

		default : messageType = JOptionPane.INFORMATION_MESSAGE; break;
	}

	JOptionPane.showMessageDialog(poserDisplay, pMessage, pTitle, messageType);

}// end of MainView::displayDialogWindow
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::displayYesNoPrompt
//
// Displays a modal dialog window showing pMessage and YES, NO, CANCEL options.
//
// Returns JOption

public int displayYesNoPrompt(String pMessage)
{

    int ync = JOptionPane.showConfirmDialog(
        mainFrame,
		pMessage,
        "Confirm",
        JOptionPane.YES_NO_CANCEL_OPTION);

	switch(ync){

		case JOptionPane.CANCEL_OPTION: return( Tools.CANCEL_OPTION);

		case JOptionPane.NO_OPTION: return( Tools.NO_OPTION);

		case JOptionPane.YES_OPTION: return( Tools.YES_OPTION);

		default: return( Tools.CANCEL_OPTION);

	}

}//end of MainView::displayYesNoPrompt
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createPoserDisplay
//
// Creates the window for displaying the pose avatar.
//
// Returns the PoserDisplay reference.
//

public PoserDisplay createPoserDisplay()
{

	poserDisplay = new PoserDisplay(mainFrame, this);

    poserDisplay.init();

	return(poserDisplay);

}// end of MainView::createPoserDisplay
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createLogger
//
// Creates the log window and a theadsafe logger object.
//
// Returns the threadsafe logger.
//

public ThreadSafeLogger createLogger()
{

	int xPos = 0; int yPos = 465;

    log = new Log(mainFrame, this, 440, 330); log.setLocation(xPos, yPos); log.init();

    log.setVisible(true);

    tsLog = new ThreadSafeLogger(log.textArea);

	poserDisplay.setTsLog(tsLog);

    return(tsLog);

}// end of MainView::createLogger
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::getScreenSize
//
// Retrieves the current screen size along with the actual usable vertical
// size after subtracting the size of the taskbar.
//
// Returns the usable size as a Dimension.
//

protected Dimension getScreenSize()
{

    totalScreenSize = Toolkit.getDefaultToolkit().getScreenSize();

    //height of the task bar
    Insets scnMax = Toolkit.getDefaultToolkit().getScreenInsets(
                                        mainFrame.getGraphicsConfiguration());
    int taskBarHeight = scnMax.bottom;

    usableScreenSize = new Dimension(
                  totalScreenSize.width, totalScreenSize.height-taskBarHeight);

    return(usableScreenSize);

}// end of MainView::getScreenSize
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setupMainFrame
//
// Sets various options and styles for the main frame.
//
// The default close operation is set to DO_NOTHING_ON_CLOSE so that the window closing event
// can decide whether or not to close the Main Window. The user may cancel the operation via the
// "Do you want to save changes?" prompt when modified data needs to be saved.
//

public void setupMainFrame()
{

    mainFrame = new JFrame("Spud Main Pi 4");

	mainFrame.setLocation(FRAME_X_POS, FRAME_Y_POS);

	mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);

    SwissArmyKnife.setIconImages(mainFrame, Main.class, "Spud Icon");

    getScreenSize();

    //add a JPanel to the frame to provide a familiar container
    mainPanel = new JPanel();
    mainFrame.setContentPane(mainPanel);

    mainFrame.addWindowListener(this);

    //turn off default bold for Metal look and feel
    UIManager.put("swing.boldMetal", Boolean.FALSE);

    //force "look and feel" to Java style
    try {
        UIManager.setLookAndFeel(
            UIManager.getCrossPlatformLookAndFeelClassName());
        }
    catch (ClassNotFoundException | InstantiationException |
            IllegalAccessException | UnsupportedLookAndFeelException e) {
        System.out.println("Could not set Look and Feel");
        }

}// end of MainView::setupMainFrame
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setupHotKeys
//
// Sets up hot keys to control various functions.
//

protected void setupHotKeys()
{

/*
    JRootPane rootPane = mainFrame.getRootPane();

    F1ScopeFreezeAction f1Action =
					new F1ScopeFreezeAction("Toggle Scope Display Frozen State", KeyEvent.VK_F1);

    rootPane.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT).
				put(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0 ), // 0->InputEvent.SHIFT_MASK
															"Toggle Scope Display Frozen State");
    rootPane.getActionMap().put("Toggle Scope Display Frozen State", f1Action);

*/

}// end of MainView::setupHotKeys
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setupGUI
//
// Sets up the user interface on the mainPanel: buttons, displays, etc.
//

protected void setupGui()
{

    mainPanel.setLayout(new BoxLayout(mainPanel, BoxLayout.Y_AXIS));

	JPanel subPanel;
	mainPanel.add(subPanel = new JPanel());
	subPanel.setLayout(new BoxLayout(subPanel, BoxLayout.X_AXIS));
	subPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

	subPanel.add(settingsPanel = createSettingsPanel());

	mainPanel.add(controlsPanel = createControlsPanel());

	createPoserDisplay();

}// end of MainView::setupGui
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createSettingsPanel
//
// Returns a JPanel containing the settings GUI components.
//
// Adds the panel and its subpanels to the ArrayList containersWithPersistentValueControls so that
// they will be scanned when updating persistent data variables.
//

protected JPanel createSettingsPanel()
{

    JPanel panel = new JPanel();
    panel.setBorder(BorderFactory.createTitledBorder("Settings"));
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    panel.setAlignmentX(Component.LEFT_ALIGNMENT);

	settingsColumn1 = new JPanel();
	settingsColumn1.setLayout(new BoxLayout(settingsColumn1, BoxLayout.Y_AXIS));
	panel.add(settingsColumn1);

	// -- Settings Column 1 --

	createBasicTextField(settingsColumn1, "Robot Name", "General | Robot Name", "Robot Name",
																					 125, true, 3);

	createBasicTextField(settingsColumn1, "Neck Motor Current Level",
	"Neck Motor | Current Level", "Neck Motor Current Level", 125, true, 2);

	settingsColumn1.add(Box.createVerticalGlue());

    return(panel);

}// end of MainView::createSettingsPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createBasicTextField
//
// Creates a TextFieldPanel using appropriate settings for display along with the supplied values:
//
//	pDescriptionLabelText, pComponentName, pActionCommand
//
// The text field will have width pWidth.
//
// If pEditable is true, the component will be editable by the user.
//
// A vertical spacer will be added above the panel of size pVerticalSpacerAboveSize.
//
// The new component is added to pPanel.
//
// Returns the newly created component as a TextFieldPanel.
//

protected TextFieldPanel createBasicTextField(JPanel pPanel, String pDescriptionLabelText,
					String pComponentName, String pActionCommand, int pWidth, boolean pEditable,
																	int pVerticalSpacerAboveSize)
{

	addVerticalSpacer(pPanel, pVerticalSpacerAboveSize);

	TextFieldPanel textFieldPanel;

	pPanel.add(textFieldPanel = new TextFieldPanel(pDescriptionLabelText, "",
								pComponentName, pEditable, pWidth, 20, pActionCommand, this, this));

	textFieldPanel.init();
	textFieldPanel.setAlignmentX(Component.LEFT_ALIGNMENT);

	return(textFieldPanel);

}// end of MainView::createBasicTextField
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createControlsPanel
//
// Returns a JPanel containing the control GUI items.
//
// Adds the panel to the ArrayList containersWithPersistentValueControls so that it will be
// scanned when updating persistent data variables.
//

protected JPanel createControlsPanel()
{

    JPanel panel = new JPanel();
    panel.setBorder(BorderFactory.createTitledBorder("Controls"));
    panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
    panel.setAlignmentX(Component.LEFT_ALIGNMENT);

    addHorizontalSpacer(panel, 2);

	ArrayList<String> comPortList = new ArrayList<>(1);
	comPortList.add("select COM port");

	panel.add(comPortSelector = new ComboBoxPanel("COM Port", 5, "", comPortList,
								"COM Port JComboBox", false, 125, 20, "set COM Port", this, this));
	comPortSelector.init();

    addHorizontalSpacer(panel, 10);

	panel.add(dataNotSavedLabel = new JLabel(""));

	addHorizontalSpacer(panel, 10);

	panel.add(Box.createHorizontalGlue());

    JButton refreshBtn = new JButton("Refresh");
    refreshBtn.setAlignmentX(Component.LEFT_ALIGNMENT);
    refreshBtn.setActionCommand("Refresh Settings and Displays");
    refreshBtn.addActionListener(this);
    refreshBtn.setToolTipText("Click this button after making data changes.");
    panel.add(refreshBtn);

    addHorizontalSpacer(panel, 5);

    JButton connectBtn = new JButton("Connect");
    connectBtn.setAlignmentX(Component.LEFT_ALIGNMENT);
    connectBtn.setActionCommand("Connect to Backpack");
    connectBtn.addActionListener(this);
    connectBtn.setToolTipText("Connect to the Main Pi Backpack processor.");
    panel.add(connectBtn);

    addHorizontalSpacer(panel, 5);

/*
    freezeBtn = new JToggleButton("Freeze");
    freezeBtn.setAlignmentX(Component.LEFT_ALIGNMENT);
    freezeBtn.setActionCommand("Toggle Scope Display Frozen State");
    freezeBtn.addActionListener(this);
    freezeBtn.setToolTipText("Freeze/Unfreeze the scope displays and enable scope panel controls.");
    panel.add(freezeBtn);
*/

    addHorizontalSpacer(panel, 5);

    return(panel);

}// end of MainView::createControlsPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setEnableForAllGUIComponents
//
/**
 * Sets the enabled flag for all Components of all Frames, Dialogs, Windows, etc. and all their
 * child descendants recursively.
 *
 * @param pEnabled		true to enable all Components, false to disable
 *
 */

public void setEnableForAllGUIComponents(boolean pEnabled)
{

	setEnableForAllChildGUIComponents(mainFrame, pEnabled);

	Window[] allWindows = mainFrame.getOwnedWindows();

	for(Window window : allWindows){
		setEnableForAllChildGUIComponents(window, pEnabled);
	}

}// end of MainView::setEnableForAllGUIComponents
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setEnableForAllChildGUIComponents
//
/**
 * Sets the enabled flag for all Components of pContainer and all their child descendants
 * recursively.
 *
 * @param pContainer	top level Container to be updated along with all its child descendants
 *
 * @param pEnabled		true to enable all Components, false to disable
 *
 */

public void setEnableForAllChildGUIComponents(Container pContainer, boolean pEnabled)
{

	Component[] components = pContainer.getComponents();

	for(Component component : components){

		if(Container.class.isInstance(component)){
			if (((Container)component).getComponents().length > 0){
				setEnableForAllChildGUIComponents((Container)component, pEnabled);
			}
		}

		String componentName = component.getName();

		if(!pEnabled && componentName != null
									&& componentName.equals("Some Name")){ continue; }

			component.setEnabled(pEnabled);

	}

}// end of MainView::setEnableForAllChildGUIComponents
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::getGUIComponentByName
//
/**
 * Searches all Frames, Dialogs, Windows, etc. and all their child descendants recursively for the
 * Component with name of pName and returns a reference to the Component.
 *
 * @param pName		the name of the Component to search for
 *
 * @return	a reference to the Component or null if no Component found with the specified name
 *
 */

public Component getGUIComponentByName(String pName)
{

	Component component = searchAllChildGUIComponentsByName(mainFrame, pName);

	if (component != null){ return(component); }

	Window[] allWindows = mainFrame.getOwnedWindows();

	for(Window window : allWindows){
		component = searchAllChildGUIComponentsByName(window, pName);
		if(component != null){ return(component); }
	}

	return(component);

}// end of MainView::getGUIComponentByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::searchAllChildGUIComponentsByName
//
/**
 * Searches all of pContainer's child descendants recursively for the Component with name of pName
 * and returns a reference to the Component. Returns null if no Component found which has a
 * matching name.
 *
 * @param pContainer	top level Container to be searched along with all its child descendants
 *
 * @param pName		the name of the Component to search for
 *
 * @return	a reference to the Component or null if no Component found with the specified name
 *
 */

public Component searchAllChildGUIComponentsByName(Container pContainer,String pName)
{

	Component[] components = pContainer.getComponents();

	for(Component component : components){

		if(Container.class.isInstance(component)){
			if (((Container)component).getComponents().length > 0){
				Component subComponent =
									searchAllChildGUIComponentsByName((Container)component, pName);
				if(subComponent != null){ return(subComponent); }
			}
		}

		String componentName = component.getName();

		if((componentName != null) && componentName.equals(pName)){ return(component); }

	}

	return(null);

}// end of MainView::searchAllChildGUIComponentsByName
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::copyPersistentDataElementValuesToGUIComponents
//
/**
 * Copies the values from the data elements in pElements to their corresponding GUI components.
 * The name of each element is used to match it with the GUI control with the same name.
 *
 * Only the first Component found with a matching name will be updated.
 *
 * This includes all JFrames, JDialogs, and such owned by mainFrame and mainFrame itself.
 *
 * @param pElements	PersistentDataSet containing the data to be transferred to GUI components
 *
 */

public void copyPersistentDataElementValuesToGUIComponents(PersistentDataSet pElements)
{

	for(HashMap.Entry entry : pElements.getElements().entrySet()){

		String componentName = (String)entry.getKey();

		PersistentDataElement dataElement = ((PersistentDataElement) (entry.getValue()));

		Component component = getGUIComponentByName(componentName);

		if(component != null){

			if(component.getClass().getName().equals("mksystems.mswing.SpinnerPanel")){
				copyPersistentDataElementValueToMFloatSpinner(component, dataElement);
			}

			if(component.getClass().getName().equals("mksystems.mswing.ComboBoxPanel")){
				String v = (String)dataElement.getValue(true);
				((ComboBoxPanel)component).comboBox.setSelectedItem(v);
			}

			if(component.getClass().getName().equals("mksystems.mswing.TextFieldPanel")){
				copyPersistentDataElementValueToTextFieldPanel(component, dataElement);
			}

		}

	}

}// end of MainView::copyPersistentDataElementValuesToGUIComponents
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::copyGUIComponentsToPersistentDataElementValues
//
/**
 * Copies the values from the GUI components to their corresponding data elements in pElements.
 * The name of each element is used to match it with the GUI control with the same name.
 *
 * This includes all JFrames, JDialogs, and such owned by mainFrame and mainFrame itself.
 *
 * @param pElements	PersistentDataSet containing the data set to be transferred from GUI components
 *
 */

public void copyGUIComponentsToPersistentDataElementValues(PersistentDataSet pElements)
{

	for(HashMap.Entry entry : pElements.getElements().entrySet()){

		String componentName = (String)entry.getKey();

		PersistentDataElement dataElement = ((PersistentDataElement) (entry.getValue()));

		Component component = getGUIComponentByName(componentName);

		if(component != null){

			if(component.getClass().getName().equals("mksystems.mswing.SpinnerPanel")){
				copyMFloatSpinnerValueToPersistentDataElement(component, dataElement);
			}

			if(component.getClass().getName().equals("mksystems.mswing.ComboBoxPanel")){
				copyJComboBoxValueToPersistentDataElement(component, dataElement);
			}

			if(component.getClass().getName().equals("mksystems.mswing.TextFieldPanel")){
				copyJTextFieldValueToPersistentDataElement(component, dataElement);
			}

		}

	}

}// end of MainView::copyGUIComponentsToPersistentDataElementValues
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::copyPersistentDataElementValueToMFloatSpinner
//
// Copies the value from a PersistentDataElement to an MFloatSpinner.
//
// The Element could either be an Integer or a Double. The MFloatSpinner actually only handles
// doubles, so the transfer is handled differently depending on the type of Element.
//

protected void copyPersistentDataElementValueToMFloatSpinner(Component pComponent,
															PersistentDataElement pElement)
{

	String className = pElement.dummyToAllowTypeDetection.getClass().getName();

	//the spinner is always a Double when used in a SpinnerPanel, but the data element might be
	//either an Integer or a Double, so handle two different ways

	Double v;

	if(className.equals("java.lang.Integer")){

		v = ((Integer)pElement.getValue(true)).doubleValue();

		((SpinnerPanel)pComponent).spinner.setValue(v);

	} else if(className.equals("java.lang.Double")){

		v = ((Double)pElement.getValue(true));

		((SpinnerPanel)pComponent).spinner.setValue(v);

	}

}// end of MainView::copyPersistentDataElementValueToMFloatSpinner
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::copyPersistentDataElementValueToTextFieldPanel
//
// Copies the value from a PersistentDataElement to a JTextField in a TextFieldPanel
//

protected void copyPersistentDataElementValueToTextFieldPanel(Component pComponent,
															PersistentDataElement pElement)
{

	String v = (String)pElement.getValue(true);

	((TextFieldPanel)pComponent).getTextField().setText(v);

}// end of MainView::copyPersistentDataElementValueToTextFieldPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::copyMFloatSpinnerValueToPersistentDataElement
//
// Copies the value from an MFloatSpinner to a PersistentDataElement.
//
// The Element could either be an Integer or a Double. The MFloatSpinner actually only handles
// doubles, so the transfer is handled differently depending on the type of Element.
//

protected void copyMFloatSpinnerValueToPersistentDataElement(Component pComponent,
															PersistentDataElement pElement)
{

	String className = pElement.dummyToAllowTypeDetection.getClass().getName();

	//the spinner is always a Double when used in a SpinnerPanel, but the data element might be
	//either an Integer or a Double, so handle two different ways

	Double v = (Double)((SpinnerPanel)pComponent).spinner.getValue();

	if(className.equals("java.lang.Integer")){

		pElement.setIntegerValue(v.intValue(), false);

	} else if(className.equals("java.lang.Double")){

		pElement.setDoubleValue(v, false);

	}

}// end of MainView::copyMFloatSpinnerValueToPersistentDataElement
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::copyJComboBoxValueToPersistentDataElement
//
// Copies the value from a JComboBox to a PersistentDataElement.
//

protected void copyJComboBoxValueToPersistentDataElement(Component pComponent,
															PersistentDataElement pElement)
{

	String v = (String)((ComboBoxPanel)pComponent).comboBox.getSelectedItem();
	pElement.setStringValue(v, false);

}// end of MainView::copyJComboBoxValueToPersistentDataElement
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::copyJTextFieldValueToPersistentDataElement
//
// Copies the value from a JTextfield to a PersistentDataElement.
//

protected void copyJTextFieldValueToPersistentDataElement(Component pComponent,
															PersistentDataElement pElement)
{

	String v = (String)((TextFieldPanel)pComponent).getTextField().getText();
	pElement.setStringValue(v, false);

}// end of MainView::copyJTextFieldValueToPersistentDataElement
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setToolTipsForGUIComponents
//
/**
 * Sets the mouse-over tooltips for GUI components from the HashMap pToolTips where:
 *		key = name of component AND value = tooltip
 *
 * The key of each entry is used to match it with the GUI control with the same name.
 *
 * @param pToolTips the HashMap containing the Component Name / ToolTip pairs
 *
 */

public void setToolTipsForGUIComponents(HashMap<String, String> pToolTips)
{


	for(HashMap.Entry entry : pToolTips.entrySet()){

		String componentName = (String)entry.getKey();

		String toolTip = (String)entry.getValue();

		Component component = getGUIComponentByName(componentName);

		if(component != null){

			setToolTipForComponent(component, toolTip);

		}

	}

}// end of MainView::setToolTipsForGUIComponents
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setToolTipForComponent
//
// Sets the tooltip to pToolTip for various types of components specified by pComponent.
//
// The Component class does not have setToolTipText as a method, so the class must be cast to
// the appropriate subclass.
//

protected void setToolTipForComponent(Component pComponent, String pToolTip)
{


	if(pComponent instanceof JComponent){
		((JComponent)pComponent).setToolTipText(pToolTip);
	}

}// end of MainView::setToolTipForComponent
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setComboBoxSelectedItem
//
/**
 * Sets the selected item to pSlected for the JComboBox with name of pName if pSelected exists in
 * the box's list of items.
 *
 * If pSelected is in the list and the box is editable, selected will be set to pSelected.
 *
 * If pSelected is not in the list or the combo box is not editable, the current selection will not
 * be changed.
 *
 * @param pName			the name of the JComboBox to be modified
 * @param pSelected		the item to be selected if it exists in the list of items
 *
 */

public void setComboBoxSelectedItem(String pName, String pSelected)
{

	Component component = getGUIComponentByName(pName);

	if(component == null){ return; }

	JComboBox<String> comboBox = ((ComboBoxPanel)component).comboBox;

	comboBox.setSelectedItem(pSelected);

}// end of MainView::setComboBoxSelectedItem
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setComboBoxList
//
/**
 * Sets the list of items for the JComboBox with name of pName to the items in pList.
 *
 * The container holding the target JComboBox must be in ArrayList:
 *		containersWithPersistentValueControls
 *
 * Inserts pHeaderItem at the top of the list which is often used for such phrases as:
 *		"select option", "select COM port", etc.
 *
 * NOTE: modifying the ArrayList passed into the ComboBox at creation doesn't seem to have any
 * effect on the ComboBox's list of items. Seems like removeAllItems and addItem are the only
 * solutions.
 *
 * @param pName			the name of the JComboBox to be modified
 * @param pList			the list of items to be used as the JComboBox's drop down list
 * @param pHeaderItem	the String to add as the first item in the list
 *
 */

public void setComboBoxList(String pName, ArrayList<String> pList, String pHeaderItem)
{

	Component component = getGUIComponentByName(pName);

	if(component == null){ return; }

	JComboBox<String> comboBox = ((ComboBoxPanel)component).comboBox;

	comboBox.removeAllItems();

	comboBox.addItem(pHeaderItem);

	for(String name : pList){ comboBox.addItem(name); }

}// end of MainView::setComboBoxList
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::addHorizontalSpacer
//
// Adds a horizontal spacer of pNumPixels width to JPanel pTarget.
//

protected void addHorizontalSpacer(JPanel pTarget, int pNumPixels)
{

    pTarget.add(Box.createRigidArea(new Dimension(pNumPixels,0)));

}// end of MainView::addHorizontalSpacer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::addVerticalSpacer
//
// Adds a vertical spacer of pNumPixels height to JPanel pTarget.
//

protected void addVerticalSpacer(JPanel pTarget, int pNumPixels)
{

    pTarget.add(Box.createRigidArea(new Dimension(0,pNumPixels)));

}// end of MainView::addVerticalSpacer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::draw
//
// Draws the universe.
//

public void draw(Graphics2D pG2)
{

}//end of MainView::draw
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::createFonts
//
// Creates fonts for use by the program.
//

public void createFonts()
{

    //create small and large red and green fonts for use with display objects
    HashMap<TextAttribute, Object> map = new HashMap<>();

    blackSmallFont = new Font("Dialog", Font.PLAIN, 12);

    map.put(TextAttribute.FOREGROUND, Color.RED);
    redSmallFont = blackSmallFont.deriveFont(map);

    //empty the map to use for creating the large fonts
    map.clear();

    blackLargeFont = new Font("Dialog", Font.PLAIN, 20);

    map.put(TextAttribute.FOREGROUND, Color.GREEN);
    greenLargeFont = blackLargeFont.deriveFont(map);

    map.put(TextAttribute.FOREGROUND, Color.RED);
    redLargeFont = blackLargeFont.deriveFont(map);

    map.put(TextAttribute.FOREGROUND, Color.YELLOW);
    yellowLargeFont = blackLargeFont.deriveFont(map);

}// end of MainView::createFonts
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setLogWindowVisibility
//
/**
 * Sets the visibility of the Log Window.
 *
 * The window is never released as the information is retained so it can be viewed the next time
 * it is made visible.
 *
 * @param pVisible	true if the window is to be visible, false for invisible
 *
 */


public void setLogWindowVisibility(boolean pVisible)
{

    log.setVisible(pVisible);

}//end of MainView::setLogWindowVisibility
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::displayHelp
//
// Displays help information.
//

public void displayHelp()
{

    help = new Help(mainFrame, HELP_FILENAME); help.init();
    help = null;  //window will be released on close, so point should be null

}//end of MainView::displayHelp
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::displayAboutWindow
//
/**
 * Displays the "About" window.
 *
 * @param pJavaJVMVersion	the version of the Java JVM on which the program is running.
 */

public void displayAbout(String pJavaJVMVersion)
{

    new About(mainFrame).init(pJavaJVMVersion);

}//end of MainView::displayAbout
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::displayErrorMessage
//
// Displays an error dialog with message pMessage.
//

public void displayErrorMessage(String pMessage)
{

    Tools.displayErrorMessage(pMessage, mainFrame);

}//end of MainView::displayErrorMessage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::updateGUIDataSet1
//
// Updates some of the GUI with data.
//

public void updateGUIDataSet1()
{


}//end of MainView::updateGUIDataSet1
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::drawRectangle
//
// Draws a rectangle on mainPanel
//

public void drawRectangle()
{

    Graphics2D g2 = (Graphics2D)mainPanel.getGraphics();

     // draw Rectangle2D.Double
    g2.draw(new Rectangle2D.Double(20, 10,10, 10));

}//end of MainView::drawRectangle
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::updateModelDataSet1
//
// Updates some of the model data with values in the GUI.
//

public void updateModelDataSet1()
{

}//end of MainView::updateModelDataSet1
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setupAndStartSwingTimers
//
// Prepares and starts various Java Swing timer.
//

public void setupAndStartSwingTimers()
{

    mainTimer = new javax.swing.Timer (2500, this); //debug mks -- was 10, 2500 is pretty slow for this purpose?
    mainTimer.setActionCommand("Main Timer");
    mainTimer.start();

    animationTimer = new javax.swing.Timer(100, this);
    animationTimer.setActionCommand ("Animation Timer");
    animationTimer.start();

    blinkTimer = new javax.swing.Timer(AVERAGE_BLINK_PERIOD, this);
    blinkTimer.setActionCommand ("Blink Timer");
    blinkTimer.start();

    announcementTimer = new javax.swing.Timer(300000, this);
    announcementTimer.setActionCommand ("Announcement Timer");
    announcementTimer.start();

}// end of MainView::setupAndStartSwingTimers
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setBlinkTimerDelayAndRestart
//
/**
 * Sets the delay for blinkTimer and restarts it so it will use the new value and void any time
 * already expired since the last event.
 *
 * @param pDelay	the new delay value for the timer.
 *
 */

public void setBlinkTimerDelayAndRestart(int pDelay)
{

	blinkTimer.setInitialDelay(pDelay);
    blinkTimer.setDelay(pDelay);

	blinkTimer.restart();

}// end of MainView::setBlinkTimerDelayAndRestart
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setAnimationTimerDelayAndRestart
//
/**
 * Sets the delay for animationTimer and restarts it so it will use the new value and void any time
 * already expired since the last event.
 *
 * @param pDelay	the new delay value for the timer.
 *
 */

public void setAnimationTimerDelayAndRestart(int pDelay)
{

	animationTimer.setInitialDelay(pDelay);
    animationTimer.setDelay(pDelay);

	animationTimer.restart();

}// end of MainView::setAnimationTimerDelayAndRestart
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::stopAllTimers
//
// Stops all Java Swing timers.
//

public void stopAllTimers()
{

//    mainTimer.stop();

}// end of MainView::stopAllTimers
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::actionPerformed
//
// Responds to events and passes them on to the "Controller" (MVC Concept)
// objects.
//

@Override
public void actionPerformed(ActionEvent e)
{

    eventHandler.handleEvent(new EventInfo(e.getActionCommand(), ""));

}//end of MainView::actionPerformed
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::mouseWheelMoved
//
// Handles movement of the mouse wheel.
//

@Override
public void mouseWheelMoved(MouseWheelEvent evt){

    if (evt.getWheelRotation() > 0 ){

    }
    else{

    }

}// end of MainView::mouseWheelMoved
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::updateGUI
//
// Updates various GUI items such as status messages and labels.
//

public void updateGUI()
{

}//end of MainView::updateGUI
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setTextOfDataNotSavedLabel
//
// Sets the text of a label specifically reserved to display the "unsaved changes" state of the data
// set. This label should always be separate from other message labels as it needs to show the
// saved/unsaved state at all times.
//

public void setTextOfDataNotSavedLabel(String pText) {


	dataNotSavedLabel.setText(pText);

}//end of MainView::setTextOfDataNotSavedLabel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::stateChanged
//
// This is an example of handling of stateChanged responses.
//
// Some components such as Spinners fire stateChanged calls instead of actionPerformed. Below
// is an example of catching a component with a particular name, extracting its value, and
// sending it to the EventHandler via an EventInfo object.
//

@Override
public void stateChanged(ChangeEvent ce) {

    //if for some reason the object which changed state is not a subclass of
    //of Component, do nothing as this code only handles Components

    if (!(ce.getSource() instanceof Component)) { return; }

    //cast the object to a Component so it's methods can be accessed
    Component c = (Component)ce.getSource();

    String name = c.getName();

    if (name.startsWith("Integer Spinner 1")){

        //Since we know that the Component with the name starting with
        //"Integer Spinner 1" is an MFloatSpinner (because we created it and
        // used that name for it), it can safely be cast to an MFloatSpinner.
        //Since the values in that spinner are meant to be integers, the
        //getIntValue method is used to retrieve the value.

        int value = ((MFloatSpinner)c).getIntValue();

		eventHandler.handleEvent(new EventInfo(name, "" + value));

    }

}//end of MainView::stateChanged
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::menuSelected
//

@Override
public void menuSelected(MenuEvent e)
{

	eventHandler.handleEvent(new EventInfo("Top Level Menu Selected", ""));

}//end of MainView::menuSelected
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::menuDeselected
//

@Override
public void menuDeselected(MenuEvent e)
{

	eventHandler.handleEvent(new EventInfo("Top Level Menu Deselected", ""));

}//end of MainView::menuDeselected
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::(various MenuListener functions)
//
// These functions are implemented per requirements of interface MenuListener
// but do nothing at the present time.  As code is added to each function, it
// should be moved from this section and formatted properly.
//

@Override
public void menuCanceled(MenuEvent e){}

//end of MainView::(various MenuListener functions)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::mouseClicked
//
// Responds when the mouse button is released while over a component which is
// listening to the mouse.
//

@Override
public void mouseClicked(MouseEvent e)
{

    int button = e.getButton();

    //catch left clicks
    if (button == MouseEvent.BUTTON1){

        return;

    }

    //catch right clicks
    if (button == MouseEvent.BUTTON3){

		return;

    }

}//end of MainView::mouseClicked
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::setMouseCursorLocation
//
/**
 * Sets the location of the mouse cursor on the screen.
 *
 *
 *
 *
 * @param pX	new x location for the cursor
 * @param pY	new Y location for the cursor
 *
 */

public void setMouseCursorLocation(int pX, int pY){

	try {
		Robot robot = new Robot();
		robot.mouseMove(pX, pY);
	} catch (AWTException e) {
	}

}//end of MainView::setMouseCursorLocation
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::(various MouseListener functions)
//
// These functions are implemented per requirements of interface MouseListener
// but do nothing at the present time.  As code is added to each function, it
// should be moved from this section and formatted properly.
//

@Override
public void mousePressed(MouseEvent e){}
@Override
public void mouseReleased(MouseEvent e){}
@Override
public void mouseEntered(MouseEvent e){}
@Override
public void mouseExited(MouseEvent e){}

//end of MainView::(various MouseListener functions)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::windowClosing
//
// Handles actions necessary when the window is closing
//

@Override
public void windowClosing(WindowEvent e)
{

	String windowClosingStatus =
							eventHandler.handleEvent(new EventInfo("Main Window is Closing", ""));

	if(windowClosingStatus.equals("OK to Close Window")){
		stopAllTimers();
		mainFrame.dispose();
	}

}//end of Controller::windowClosing
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainView::(various WindowListener functions)
//
// These functions are implemented per requirements of interface WindowListener
// but do nothing at the present time.  As code is added to each function, it
// should be moved from this section and formatted properly.
//

@Override
public void windowActivated(WindowEvent e){}
@Override
public void windowDeactivated(WindowEvent e){}
@Override
public void windowOpened(WindowEvent e){}
//@Override
//public void windowClosing(WindowEvent e){}
@Override
public void windowClosed(WindowEvent e){}
@Override
public void windowIconified(WindowEvent e){}
@Override
public void windowDeiconified(WindowEvent e){}

//end of MainView::(various WindowListener functions)
//-----------------------------------------------------------------------------

}//end of class MainView
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
