/******************************************************************************
* Title: PoserDisplay.java
* Author: Mike Schoonover
* Date: 7/18/2020
*
* Purpose:
*
* This class handles a window for displaying content on the robot's mouth display.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package view;

import controller.LoggerBasic;
import controller.PlotInterface;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;
import model.Options;
import remoteDeviceHandlers.EthernetClient;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class PoserDisplay
//

public class PoserDisplay extends JDialog{

	JPanel mainPanel;

    public JTextArea textArea;

	LoggerBasic tsLog;
	public void setTsLog(LoggerBasic pThreadSafeLogger){ tsLog = pThreadSafeLogger; }

	ActionListener actionListener;

	EthernetClient mjpegStreamViewer;

	Thread mjpegViewerThread;

	final boolean HAS_REFRESH_BUTTON = true;
	final boolean NO_HAS_REFRESH_BUTTON = false;

	// NOTE:
	//
	// Raspberry Pi GUI forces Y position to 20 if it is set to 0. This forces window below the
	// taskbar. So it is explicitly set to 20 here for clarity. On Windows, this simply sets the
	// window 20 pixels down from the top of the display.

	// 4K resolution: 2,160 pixels tall and 3,840 pixels wide

	// wip mks -- FRAME_X_POS should be -10 (or similar) for Windows to actually set at left edge

	final int FRAME_X_POS = 0;
	final int FRAME_Y_POS = 0;
	final int FRAME_WIDTH = 2160;
	final int FRAME_HEIGHT = 3840;

	int SPACER_PANEL_HEIGHT = 20;

	public Point getTargetXY(){
		if(mjpegStreamViewer != null){
			return(mjpegStreamViewer.getTargetXY());
		}else{
			return(new Point(0,0));
		}
	}

//-----------------------------------------------------------------------------
// PoserDisplay::PoserDisplay (constructor)
/**
 * Constructor for a PoserDisplay JDialog window.
 *
 *
 * @param pFrame				the owner frame of this window
 * @param pActionListener		the action listener for components of this window
 *
 */

public PoserDisplay(JFrame pFrame, ActionListener pActionListener)
{

    super(pFrame, "Poser Display");

	actionListener = pActionListener;

	mjpegStreamViewer = null;

	mjpegViewerThread = null;

}//end of PoserDisplay::PoserDisplay (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::init
//
// Initializes the object.  Must be called immediately after instantiation.
//

public void init()
{

	setWindowLocationToSecondDisplayIfExists();

	JPanel outerPanel = new JPanel();

	outerPanel.setLayout(new BoxLayout(outerPanel, BoxLayout.PAGE_AXIS));

	setContentPane(outerPanel);

	// add a spacer panel so the frame's top border is well above the robot's mouth hole to prevent
	// user accidentally touching and moving the display when trying to click something near the
	// top of the window

	JPanel spacerPanel = new JPanel();

	spacerPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
	spacerPanel.setMinimumSize(new Dimension(FRAME_WIDTH, SPACER_PANEL_HEIGHT));
    spacerPanel.setPreferredSize(new Dimension(FRAME_WIDTH, SPACER_PANEL_HEIGHT));
    spacerPanel.setMaximumSize(new Dimension(FRAME_WIDTH, SPACER_PANEL_HEIGHT));

	getContentPane().add(spacerPanel);

    //add a JPanel to the frame to provide a familiar container
    mainPanel = new JPanel();

	mainPanel.setAlignmentX(Component.LEFT_ALIGNMENT);
	mainPanel.setMinimumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
    mainPanel.setPreferredSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));
    mainPanel.setMaximumSize(new Dimension(FRAME_WIDTH, FRAME_HEIGHT));

	getContentPane().add(mainPanel);

    setVisible(true);

	pack();

}// end of PoserDisplay::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::setWindowLocationToSecondDisplayIfExists
//
/**
 *
 * Sets the window location to FRAME_X_POS,FRAME_Y_POS of the second display if there is a second
 * display. If there is no second display, the window location is set to FRAME_X_POS,FRAME_Y_POS of
 * the default display.
 *
 */

public void setWindowLocationToSecondDisplayIfExists()
{

	GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

	GraphicsDevice[] gd = ge.getScreenDevices();

	if (gd.length > 1){
        setLocation(gd[1].getDefaultConfiguration().getBounds().x + FRAME_X_POS,
				gd[1].getDefaultConfiguration().getBounds().y + FRAME_Y_POS);
    } else if( gd.length > 0 ) {
        	setLocation(FRAME_X_POS, FRAME_Y_POS);
    } else {
        throw new RuntimeException( "No Screens Found" );
    }

}// end of PoserDisplay::setWindowLocationToSecondDisplayIfExists
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::disposeOfAllResources
//
/**
 * Releases all objects created which may have back references. This is
 * necessary because some of those objects may hold pointers back to a parent
 * object (such as Java Listeners and such). These back references will prevent
 * the parent object from being released and a memory leak will occur.
 *
 * Not all objects must be explicitly released here as most will be
 * automatically freed when this object is released.
 *
 */

public void disposeOfAllResources()
{

	if(mjpegStreamViewer != null) { mjpegStreamViewer.stopThread(); }

}//end of PoserDisplay::disposeOfAllResources
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::displayBasicGraphicsPanel
//
/**
 * Displays a panel for displaying basic custom graphics such as pixel drawings and
 * text. The panel will send back EventInfo requests for Painting to the controlling object and
 * for other events.
 *
 * The panel will use its title to create callback messages for such things as paint requests:
 *	  if title = 'Progress Graph', paint request message will be 'Paint Progress Graph'.
 *
 * The client can then watch for such messages and direct them to the appropriate object which
 * handles painting (or other events) for the panel.
 *
 *
 * @param pTitle				the title for the panel; will also be used to create callback
 *								events such as 'Paint Progress Graph'
 * @param pUseBufferedImage		forces all drawing to use a BufferedImage to reduce flickering
 * @param pEventCommandOnClose	the command string with which to call the ActionListener when
 *								the panel display is closed - this is usually used to select the
 *								panel which should be displayed such as a menu
 *
 * @return	a reference to the map panel as a PlotInterface.
 *
 */

public PlotInterface displayBasicGraphicsPanel(String pTitle, boolean pUseBufferedImage,
																		String pEventCommandOnClose)
{

	BasicGraphicsPanel panel = new BasicGraphicsPanel(pTitle, mainPanel, actionListener,
																			pEventCommandOnClose);
	panel.init();

	pack();

	repaint();

	panel.setUseBufferedImage(pUseBufferedImage);

	setFocusToMainPanelToEnableHotKeys();

	return(panel);

}// end of PoserDisplay::displayBasicGraphicsPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::addHorizontalSpacer
//
// Adds a horizontal spacer of pNumPixels width to JPanel pTarget.
//

protected void addHorizontalSpacer(JPanel pTarget, int pNumPixels)
{

    pTarget.add(Box.createRigidArea(new Dimension(pNumPixels,0)));

}// end of PoserDisplay::addHorizontalSpacer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::addVerticalSpacer
//
// Adds a vertical spacer of pNumPixels height to JPanel pTarget.
//

protected void addVerticalSpacer(JPanel pTarget, int pNumPixels)
{

    pTarget.add(Box.createRigidArea(new Dimension(0,pNumPixels)));

}// end of PoserDisplay::addVerticalSpacer
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::getImageIconFromJarFile
//
/**
 * Returns an ImageIcon created from an image retrieved from within the program's jar file. The
 * path in the jar file is specified by pPath.
 *
 * @pPath	the path to the image including the image name
 * @return	the ImageIcon created from the image or null on error loading the image
 *
 */

ImageIcon getImageIconFromJarFile(String pPath)
{

    java.net.URL imgURL;

    imgURL = PoserDisplay.class.getResource(pPath);

	if (imgURL != null) {
		return(new ImageIcon(imgURL));
	}else{
		return(null);
	}

}// end of PoserDisplay::getImageIconFromJarFile
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::createButton
//
/**
 * Creates a button with the specified values. Adds class member actionListener as the button's
 * actionListener. Button can either display text or an icon.
 *
 * @param pText				the text displayed on the button
 * @param pImageIcon		image to display for an Icon button; if non-null used instead of pText
 * @param pToolTipText		the text displayed as a Tool Tip (when cursor hovers over button)
 * @param pActionCommand	command fired when button is clicked
 * @return					a reference to the button
 *
 */

JButton createButton(String pText, ImageIcon pImageIcon, String pToolTipText, String pActionCommand)
{

	JButton button;

	if (pImageIcon == null){
		button = new JButton(pText);
	}else{
		button = new JButton(pImageIcon);
	}

	button.setToolTipText(pToolTipText);
	button.setActionCommand(pActionCommand);
	button.addActionListener(actionListener);

	return(button);

}// end of PoserDisplay::createButton
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::displayTextPanel
//
/**
 * Displays a text panel.
 *
 * Removes all previous children of the content pane. If removeAll is called on the PoserDisplay
 * object, it also removes the content pane which should not be removed. Calling add on PoserDisplay
 * adds children to the content pane.
 *
 */

void displayTextPanel()
{

	mainPanel.removeAll();

	textArea = new JTextArea();

    mainPanel.add(textArea);

    textArea.append("Spud Main Pi 4\n");

    textArea.append("\n");

    textArea.append("Software Version: " + Options.SOFTWARE_VERSION + "\n");

    textArea.append("\n");

	textArea.append("Author: Mike Schoonover" + "\n");

	pack();

	repaint();

	setFocusToMainPanelToEnableHotKeys();

}// end of PoserDisplay::displayTextPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::displayImage
//
/**
 * Loads an image from media and displays it. Removes all previous children of the content pane.
 *
 * If removeAll is called on the PoserDisplay object, it also removes the content pane which should
 * not be removed. Calling add on PoserDisplay adds children to the content pane.
 *
 * Note that MouseAdapter::mousePressed is used rather than mouseClicked {@literal (press &
 * release)} as some systems have more difficulty detecting the click than a simple press when
 * using a touch screen.
 *
 * @param pFilepath			the path and name of the image to display
 * @param pBackgroundColor	the background color for the image in format RRR,GGG,BBB
 * @param pScaleImageToFit	true if the image is to be scaled to fit, false otherwise
 *
 */

public void displayImage(String pFilepath, String pBackgroundColor, boolean pScaleImageToFit)
{

	mainPanel.removeAll();

	// force the imagePanel to fill the parent so the former's background color will fill

	mainPanel.setLayout(new GridLayout(0, 1));

	JPanel imagePanel = new JPanel();

	imagePanel.setOpaque(true);

	mainPanel.add(imagePanel);

	imagePanel.setLayout(new BoxLayout(imagePanel, BoxLayout.PAGE_AXIS));

	imagePanel.add(Box.createVerticalGlue());

	ImageIcon imageIcon;

	imageIcon = new ImageIcon(pFilepath);

	logMediaLoadResult(imageIcon);

	JLabel label;

	if(imageIcon.getImageLoadStatus() != MediaTracker.ERRORED) {
		if(pScaleImageToFit){
			Image image = imageIcon.getImage();
			Image newimg = image.getScaledInstance(
						mainPanel.getWidth(), mainPanel.getHeight(), java.awt.Image.SCALE_SMOOTH);
			imageIcon = new ImageIcon(newimg);
		}
		label = new JLabel(imageIcon);

		Color backgroundColor = colorFromString(pBackgroundColor, Color.WHITE);

		imagePanel.setBackground(backgroundColor);
		label.setBackground(backgroundColor);
		label.setOpaque(true);
	}else{
		String text, htmlText;
		text = "<center>Error loading image!<br>Click Here to Return to Menu</center>";
		htmlText = "<html>" + text + "</html>";
		label = new JLabel(htmlText);
		int width = 200, height = 300;
		label.setMinimumSize(new Dimension(width, height));
		label.setMaximumSize(new Dimension(width, height));
		label.setPreferredSize(new Dimension(width, height));
	}

	label.setAlignmentX(Component.CENTER_ALIGNMENT);

	label.addMouseListener(new MouseAdapter()
	{
		@Override
		public void mousePressed(MouseEvent e)  {
			actionListener.actionPerformed(new ActionEvent(this, 1, "Repeat Previous Command"));
		}
	});

	imagePanel.add(label);

	pack();

	repaint();

	setFocusToMainPanelToEnableHotKeys();

}// end of PoserDisplay::displayImage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::colorFromString (static)
//
/**
 * Creates a color to match that defined by pString.  If pString matches one
 * of the standard colors (BLACK, WHITE, RED, etc.) the color will be set
 * accordingly. If pString does not match a standard color, the color will be
 * set assuming the string is in the format rrr,ggg,bbb.  If a color value is
 * greater than 255 or less than 0, it will be forced to 255 or 0 respectively.
 *
 * If an error occurs parsing the string, the color will be set to pDefault.
 *
 * @param pString	the string containing the color values in format RRR,GGG,BBB
 * @param pDefault	the default color to be used if pString is invalid
 *
 * @return			the color specified by pString
 *
 */

static public Color colorFromString(String pString, Color pDefault)
{

    Color match = pDefault;
    boolean exit = false;

    pString = pString.toUpperCase();

    //if the color name matches a standard color, use that color
    switch (pString) {
        case "BLACK":
            match = Color.BLACK;
            exit = true;
            break;
        case "BLUE":
            match = Color.BLUE;
            exit = true;
            break;
        case "CYAN":
            match = Color.CYAN;
            exit = true;
            break;
        case "DARK_GRAY":
            match = Color.DARK_GRAY;
            exit = true;
            break;
        case "GRAY":
            match = Color.GRAY;
            exit = true;
            break;
        case "GREEN":
            match = Color.GREEN;
            exit = true;
            break;
        case "LIGHT GRAY":
            match = Color.LIGHT_GRAY;
            exit=true;
            break;
        case "MAGENTA":
            match = Color.MAGENTA;
            exit = true;
            break;
        case "ORANGE":
            match = Color.ORANGE;
            exit = true;
            break;
        case "PINK":
            match = Color.PINK;
            exit = true;
            break;
        case "RED":
            match = Color.RED;
            exit = true;
            break;
        case "WHITE":
            match = Color.WHITE;
            exit = true;
            break;
        case "YELLOW":
            match = Color.YELLOW;
            exit = true;
            break;
    }

    //if color found, exit with that color
    if (exit) {return(match);}

    //string does not name a standard color so assume it is rrr,ggg,bbb format
    //if a format error occurs, return the default color

    String rgb; int lRed; int lGreen; int lBlue;

    int comma, prevComma;

    try{
        //extract red value and convert to integer
        comma = pString.indexOf(',');
        if (comma == -1) {return(pDefault);}
        rgb = pString.substring(0, comma).trim();
        lRed = Integer.valueOf(rgb);

        //extract green value and convert to integer
        prevComma = comma; comma = pString.indexOf(',', prevComma+1);
        if (comma == -1) {return(pDefault);}
        rgb = pString.substring(prevComma+1, comma).trim();
        lGreen = Integer.valueOf(rgb);

        //extract blue value and convert to integer
        prevComma = comma;
        rgb = pString.substring(prevComma+1).trim();
        lBlue = Integer.valueOf(rgb);

    }
    catch(NumberFormatException e){
        //format error so return default color
        return(pDefault);
    }

    //correct illegal values
    if (lRed < 0) {lRed = 0;} if (lRed > 255) {lRed = 255;}
    if (lGreen < 0) {lGreen = 0;} if (lGreen > 255) {lGreen = 255;}
    if (lBlue < 0) {lBlue = 0;} if (lBlue > 255) {lBlue = 255;}

    //create a new MColor from the rgb values
    return(new Color(lRed, lGreen, lBlue));

}//end of PoserDisplay::colorFromString (static)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::logMediaLoadResult
//
/**
 * Logs the result code from attempting to load media for an ImageIcon in human readable form.
 *
 * Note that when loading GIF animations and possibly other large files, the status may be
 * "ABORTED" at the time this function is called, even though the file loads fine.
 *
 * @param pImageIcon	the ImageIcon for which the media load status is to be logged
 *
 */

void logMediaLoadResult(ImageIcon pImageIcon)
{

	String resultString;

	switch(pImageIcon.getImageLoadStatus()){

		case MediaTracker.ABORTED :
			resultString = "Media load ABORTED! (normal for animations)";
		break;

		case MediaTracker.ERRORED :
			resultString = "Media load ERRORED!";
		break;

		case MediaTracker.COMPLETE :
			resultString = "Media load COMPLETE!";
		break;

		default :
			resultString = "Media load result undefined!";
		break;

	}

	tsLog.appendLine(resultString);

}// end of PoserDisplay::logMediaLoadResult
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::displayVideoStream
//
/**
 * Connects to an MJPEG video stream at pStreamIPAddress and displays the video continuously,
 * updating at the specified frames-per-second rate.
 *
 * Removes all previous children of the content pane. If removeAll is called on the PoserDisplay
 * object, it also removes the content pane which should not be removed. Calling add on PoserDisplay
 * adds children to the content pane.
 *
 * @param pStreamIPAddress      the IP address of the stream source
 *								if stream is wrapped in an HTML page such as often provided by
 *								picamera, use format with http header, IP address, port number, and
 *								page combined: http://127.0.0.1:8000/stream.mjpg
 *
 *								libcamera provides a naked MJPEG stream and needs no prefix or
 *								suffix, simply: 127.0.0.1 with the port number supplied separately
 * @param pStreamPortNum		the port number of the stream source
 * @param pFramesPerSecond		the frame rate of the stream
 *
 */

public void displayVideoStream(String pStreamIPAddress, int pStreamPortNum, int pFramesPerSecond)
{

	mainPanel.removeAll();

	pack();

	mainPanel.setLayout(new OverlayLayout(mainPanel));

	if(pStreamIPAddress.startsWith("http")){
//		mjpegStreamViewer = new HTMLMJPEGStreamViewer();
	} else {
//		mjpegStreamViewer = new NakedMJPEGStreamViewer();
	}

	mjpegStreamViewer.init(mainPanel, pStreamIPAddress, pStreamPortNum, pFramesPerSecond);

	pack();

	mjpegViewerThread = new Thread(mjpegStreamViewer);
	mjpegViewerThread.setName("MJPEG Stream Viewer");

	mjpegViewerThread.start();

	mainPanel.addMouseListener(new MouseAdapter()
	{
		@Override
		public void mouseClicked(MouseEvent e)  {
			if (mjpegStreamViewer != null){
				mjpegStreamViewer.stopThread();
				while(mjpegViewerThread.isAlive()){}
				mjpegStreamViewer = null;
			}

		actionListener.actionPerformed(new ActionEvent(this, 1, "Display Camera Menu"));

		}
	});

	setFocusToMainPanelToEnableHotKeys();

}// end of PoserDisplay::displayVideoStream
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::setFocusToMainPanelToEnableHotKeys
//
/**
 * Sets the focus on the main panel of the mouth display so that any keyboard hot-keys that are
 * associated with that panel will be active.
 *
 */

void setFocusToMainPanelToEnableHotKeys()
{

	mainPanel.requestFocusInWindow();

}// end of PoserDisplay::setFocusToMainPanelToEnableHotKeys
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// PoserDisplay::createImageIconFromFile
//
/**
 * Returns an ImageIcon created from the file pFilepaht, or null if the path was invalid.
 *
 * Using getSystemResource has problems retrieving a file outside of the class path and may work
 * differently when running in the IDE vs standalone. Also it returns CRC errors on PNG files;
 * this might only be true for PNG files not included in the jar file.
 *
 ****************************************************************************
 * NOTE: You must use forward slashes in the path names for the resource
 * loader to find the image files in the JAR package.
 * ***************************************************************************
 *
 * @param	pFilepath	the path to the image file
 * @return	the imageIcon or null if the file could not be loaded
 *
 */

protected ImageIcon createImageIcon(String pFilepath)
{

	BufferedImage image;

	try {
		image = ImageIO.read(new File(pFilepath));
	} catch (IOException e) {
		return(null);
	}

	ImageIcon icon = new ImageIcon(image);

	return(icon);

}//end of PoserDisplay::createImageIcon
//-----------------------------------------------------------------------------

}//end of class PoserDisplay
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
