/******************************************************************************
* Title: BasicGraphicsPanel.java
* Author: Mike Schoonover
* Date: 1/09/22
*
* Purpose:
*
* This class displays a window for displaying basic graphics by any client.
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package view;

import controller.PlotInterface;
import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.Shape;
import java.awt.Transparency;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.geom.Ellipse2D;
import java.awt.geom.Line2D;
import java.awt.image.BufferedImage;
import javax.swing.*;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class BasicGraphicsPanel
//
/**
 *
 * This class displays a window for displaying basic graphics by any client.
 *
 */

public class BasicGraphicsPanel extends JPanel implements PlotInterface{

	String title;

	String paintEventCommand;

	Container mainContainer;

	ActionListener actionListener;

	String eventCommandOnExit = "";

	int panelWidth, panelHeight;

	Color currentColor;

	Graphics2D g2D;

	Graphics2D bufferedImageG2D;

	boolean useBufferedImage;
	public BufferedImage bufferedImage;

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::BasicGraphicsPanel (constructor)
//
/**
 * Displays a panel for displaying basic custom graphics such as pixel drawings and
 * text. The panel will send back EventInfo requests for Painting to the controlling object and
 * for other events.
 *
 * The panel will use its title to create callback messages for such things as paint requests:
 *	 if title = 'Progress Graph', paint request message will be 'Paint Progress Graph'.
 *
 * The client can then watch for such messages and direct them to the appropriate object which
 * handles painting (or other events) for the panel.
 *
 *
 * @param pTitle				the title for the panel; will also be used to create callback
 *								events such as 'Paint Progress Graph'
 * @param pMainContainer		the Container to hold the display and any controls
 * @param pActionLister			the ActionListener for responding to GUI actions
 * @param pEventCommandOnExit	the command string with with which to call the ActionListener when
 *								the panel display is closed - this is usually used to select the
 *								panel which should be displayed such as a menu
 *
 */

public BasicGraphicsPanel(String pTitle, Container pMainContainer, ActionListener pActionLister,
																	String pEventCommandOnExit)
{

	super();

	title = pTitle;

	paintEventCommand = "Paint " + pTitle;

	mainContainer = pMainContainer;

	actionListener = pActionLister;

	eventCommandOnExit = pEventCommandOnExit;

	mainContainer.setName("Basic Graphics");

	currentColor = Color.BLACK;

}//end of BasicGraphicsPanel::BasicGraphicsPanel (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::init
//
/**
 * Initializes the object.  Must be called immediately after instantiation.
 *
 */

public void init()
{

	mainContainer.removeAll();

	mainContainer.setLayout(new FlowLayout());

	setOpaque(true);

	panelWidth = mainContainer.getWidth();
	panelHeight = mainContainer.getHeight();

	setSizes(this, panelWidth, panelHeight);

	mainContainer.add(this);

	addMouseListener(new MouseAdapter()
	{
		@Override
		public void mousePressed(MouseEvent e)  {
			actionListener.actionPerformed(new ActionEvent(this, 1, eventCommandOnExit));
		}
	});

}// end of BasicGraphicsPanel::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::setUseBufferedImage
//
/**
 * Sets the useBufferedImage flag to pState:
 *
 *	If true, a BufferedImage object is created and all subsequent drawing will be done to that
 *  object instead of the JPanel. The BufferedImage can then be written to the panel using
 *  applyBufferedImage() after all drawing is complete to eliminate flicker.
 *
 *	If false, the BufferedImage object is released and all subsequent drawing will be done directly
 *  to the panel.
 *
 * Although JPanel has a Double Buffered option, it does not seem to reduce flicker in a lot of
 * cases. This is especially true when a panel is constantly erased and redrawn. The bigger the
 * panel and the more the drawing, the worse the flicker.
 *
 * WARNING:
 *
 * This function must be called after the GUI has been created and packed as the width and height
 * of the canvas is not set in stone until that time and will show as 0.
 *
 * The buffered image cannot be applied (or possibly even modified) until the call which creates
 * the window returns. Apparently Java is not ready for modifying/applying the buffered image
 * until after execution returns to Java.
 *
 * NOTE
 *
 *  For debugging, it may be easier to disable use of the BufferedImage so the drawing can be
 *  seen step-by-step on the JPanel as the user steps through code.
 *
 * @param pState	true if local buffering is to be used, false if not
 *
 */

@Override
public void setUseBufferedImage(boolean pState)
{

	useBufferedImage = pState;

	if(useBufferedImage){ createBufferedImage(); }
	else { bufferedImage = null; }

}// end of BasicGraphicsPanel::setUseBufferedImage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::applyBufferedImage
//
/**
 * Draws the BufferedImage object to the panel.
 *
 */

@Override
public void applyBufferedImage()
{

	g2D = (Graphics2D)getGraphics();

    if(g2D == null){ return; }

	g2D.drawImage(bufferedImage, 0, 0, null);

}// end of BasicGraphicsPanel::applyBufferedImage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::createBufferedImage
//
/**
 * Creates a BufferedImage object the size of the panel. A Graphics2D object is created for the
 * object and stored in a class member variable for repeated use.
 *
 * WARNING:
 *
 * This function must be called after the GUI has been created and packed as the width and height
 * of the canvas is not set in stone until that time and will show as 0.
 *
 */

public void createBufferedImage()
{

    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
    GraphicsDevice gs = ge.getDefaultScreenDevice();
    GraphicsConfiguration gc = gs.getDefaultConfiguration();

    //Java tutorial suggests checking the following cast with "instanceof", to
    //avoid runtime errors but this seems pointless as the cast MUST work for
    //the program to work so it will crash regardless if the cast is bad

    int width = getWidth(); if (width == 0) {width = 1;}
    int height = getHeight(); if (height == 0) {height = 1;}

    //create an image to store the plot on so it can be copied to the screen during repaint
    bufferedImage = (gc.createCompatibleImage(width, height, Transparency.OPAQUE));

	bufferedImageG2D = (Graphics2D)bufferedImage.getGraphics();

}//end of BasicGraphicsPanel::createBufferedImage
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::paint
//
/**
 * Override of JPanel.paint() to allow for custom graphics.
 *
 * If buffered image is active, this function simply paints the buffered image to the screen.
 *
 * If buffered image is inactive, a paint request action command will be sent to the ActionListener
 * to be passed on to the object responsible for painting. In that case, the class variable g2D
 * will be set to the parameter Graphics g passed in from the system to this method. The painting
 * object should use g2D (which is already preset here) for painting, as the one passed in here
 * specifies the actual paint-able area.
 *
 * TODO MKS
 *  The repaint when buffered image is inactive has NOT been tested.
 *  How is the painting object supposed to use the preset g2D? Add boolean to each drawing method
 *  to force use of the existing preset g2D rather than snagging a new one? This would also solve
 *  having to set color for each draw as it gets lost when each draw snags a new g2D...first draw
 *  would set g2D, subsequent ones would use the existing g2D!
 *
 * @param g		Graphics object provided by Java for drawing
 *
 */

@Override
public void paint(Graphics g)
{

	super.paint(g);

	if(useBufferedImage) {
		g.drawImage(bufferedImage, 0, 0, null);
	} else {
		g2D = (Graphics2D)g;
		actionListener.actionPerformed(new ActionEvent(this, 1, paintEventCommand));
	}

}// end of BasicGraphicsPanel::paint
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::setColor
//
/**
 * Sets the current drawing Color to pColor.
 *
 * NOTE: Does NOT actually set the Graphics2D objects color!!! Each individual drawing function
 *		 must set the Graphics2D color to current color. This is because each time they are called
 *		 the Graphics2D object may be new if an image buffer is not being used and will have to
 *		 have its color set.
 *
 *		 This could be corrected by adding a command to allow client to snag the Graphics2D object
 *		 before starting drawing each session. Then the current color could be set once by client
 *		 at that time and only changed when necessary.
 *
 * @param pColor	the new color to be used for drawing
 *
 */

@Override
public void setColor(Color pColor)
{

	currentColor = pColor;

}// end of BasicGraphicsPanel::setColor
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::setColorRGBAlpha
//
/**
 * Sets the current drawing Color to the specified RGB and Alpha values.
 *
 * NOTE: Does NOT actually set the Graphics2D objects color!!! Each individual drawing function
 *		 must set the Graphics2D color to current color. This is because each time they are called
 *		 the Graphics2D object may be new if an image buffer is not being used and will have to
 *		 have its color set.
 *
 *		 This could be corrected by adding a command to allow client to snag the Graphics2D object
 *		 before starting drawing each session. Then the current color could be set once by client
 *		 at that time and only changed when necessary.
 *
 * @param pRed		the red value for the color
 * @param pGreen	the green value for the color
 * @param pBlue		the blue value for the color
 * @param pAlpha	the alpha value for the color (opacity)
 *
 */

@Override
public void setColorRGBAlpha(int pRed, int pGreen, int pBlue, int pAlpha)
{

	currentColor = new Color(pRed, pGreen, pBlue, pAlpha);

}// end of BasicGraphicsPanel::setColorRGBAlpha
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::drawPixel
//
/**
 * Draws a pixel on the panel.
 *
 * @param pXPos		the x position of the pixel (0,0 is at top left)
 * @param pYPos		the y position of the pixel (0,0 is at top left)
 * @param pColor	the color of the pixel
 *
 */

@Override
public void drawPixel(int pXPos, int pYPos, Color pColor){

	if(useBufferedImage) { g2D = bufferedImageG2D; } else { g2D = (Graphics2D)getGraphics(); }

	g2D.setColor(pColor);

	g2D.drawLine(pXPos, pYPos, pXPos, pYPos);

}// end of BasicGraphicsPanel::drawPixel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::drawText
//
/**
 * Draws text string pText at location pX, pY using the current color.
 *
 * @param pText			the text string to be drawn
 * @param pX			x position of the text
 * @param pY			y position of the text
 *
 */

@Override
public void drawText(String pText, double pX, double pY)
{

	if(useBufferedImage) { g2D = bufferedImageG2D; } else { g2D = (Graphics2D)getGraphics(); }

	g2D.setColor(currentColor);

	g2D.drawString(pText, Math.round(pX), Math.round(pY));

}// end of BasicGraphicsPanel::setColorRGBAlpha
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::drawLineDouble
//
/**
 * Draws a line. With width pLineWidth. If pLineWidth is less than zero, then the previously set
 * line width will be used.
 *
 * @param pX1			x position of the start of the line
 * @param pY1			y position of the start of the line
 * @param pX2			x position of the end of the line
 * @param pY2			y position of the end of the line
 * @param pLineWidth	width of the line - if {@literal < 0} then previously set width will be used
 *
 */

@Override
public void drawLineDouble(double pX1, double pY1, double pX2, double pY2, float pLineWidth)
{

	if(useBufferedImage) { g2D = bufferedImageG2D; } else { g2D = (Graphics2D)getGraphics(); }

	if(pLineWidth >= 0){ g2D.setStroke(new BasicStroke(pLineWidth)); }

	g2D.setColor(currentColor);

	g2D.draw(new Line2D.Double(pX1, pY1, pX2, pY2));

}// end of BasicGraphicsPanel::drawLineDouble
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::drawEllipseDouble
//
/**
 * Draws an ellipse. If pFill is true, the ellipse will be filled.
 *
 * @param pX			x position of the upper left corner of the ellipse
 * @param pY			y position of the upper left corner of the ellipse
 * @param pWidth		the width of the ellipse
 * @param pHeight		the height of the ellipse
 * @param pFill			true if the ellipse is to be filled, false if not
 * @param pLineWidth	width of the line - if {@literal < 0} then previously set width will be used
 *
 */

@Override
public void drawEllipseDouble(double pX, double pY, double pWidth, double pHeight, boolean pFill,
																				  float pLineWidth)
{

	if(useBufferedImage) { g2D = bufferedImageG2D; } else { g2D = (Graphics2D)getGraphics(); }

	if(pLineWidth >= 0){ g2D.setStroke(new BasicStroke(pLineWidth)); }

	g2D.setColor(currentColor);

	Shape circle = new Ellipse2D.Double(pX, pY, pWidth, pHeight);

	g2D.draw(circle);

	if (pFill){ g2D.fill(circle); }

}// end of BasicGraphicsPanel::drawEllipseDouble
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::setSizes
//
// Sets the min, max, and preferred sizes of pComponent to pWidth and pHeight.
//

static public void setSizes(Component pComponent, int pWidth, int pHeight)
{

    pComponent.setMinimumSize(new Dimension(pWidth, pHeight));
    pComponent.setPreferredSize(new Dimension(pWidth, pHeight));
    pComponent.setMaximumSize(new Dimension(pWidth, pHeight));

}//end of BasicGraphicsPanel::setSizes
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::clearPanel
//
/**
 * Clears the panel to color pColor
 *
 * @param pColor	fills the panel with color pColor
 *
 */

@Override
public void clearPanel(Color pColor)
{

	if(useBufferedImage) { g2D = bufferedImageG2D; } else { g2D = (Graphics2D)getGraphics(); }

	g2D.setColor(pColor);

	g2D.fillRect(0, 0, getWidth(), getHeight());

}// end of BasicGraphicsPanel::clearPanel
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::setOptions
//
/**
 * Sets various options.
 *
 * @param pOption1	setting for option 1
 * @param pOption2	setting for option 2
 *
 */

@Override
public void setOptions(int pOption1, int pOption2)
{

}// end of BasicGraphicsPanel::setOptions
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::copyArea
//
/**
 *
 * Copies an area of the component by a distance specified by pDx and pDy. From the point specified
 * by pX and pY, this method copies downwards and to the right. To copy an area of the component to
 * the left or upwards, specify a negative value for pDx or pDy. If a portion of the source
 * rectangle lies outside the bounds of the component, or is obscured by another window or
 * component, copyArea will be unable to copy the associated pixels. The area that is omitted can
 * be refreshed by calling the component's paint method.
 *
 * @param pX		the x coordinate of the source rectangle
 * @param pY		the y coordinate of the source rectangle
 * @param pWidth	the width of the source rectangle
 * @param pHeight	the height of the source rectangle
 * @param pDx		the horizontal distance to copy the pixels
 * @param pDy		the vertical distance to copy the pixels
 *
 */

@Override
public void copyArea(int pX, int pY, int pWidth, int pHeight, int pDx, int pDy)
{

	if(useBufferedImage) { g2D = bufferedImageG2D; } else { g2D = (Graphics2D)getGraphics(); }

	g2D.copyArea(pX, pY, pWidth, pHeight, pDx, pDy);

}// end of BasicGraphicsPanel::copyArea
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// BasicGraphicsPanel::all unimplemented PlotInterface methods
//
/**
 * All unimplemented methods from PlotInterface interface.
 *
 */

@Override
public void update() {
	throw new UnsupportedOperationException("Not supported yet.");
}

@Override
public void drawRectangle(int pXPos, int pYPos, int pWidth, int pHeight, Color pColor) {
	throw new UnsupportedOperationException("Not supported yet.");
}

// end of BasicGraphicsPanel::all unimplemented PlotInterface methods
//-----------------------------------------------------------------------------

}//end of class BasicGraphicsPanel
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
