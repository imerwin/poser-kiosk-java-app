/******************************************************************************
* Title: Help.java
* Author: Mike Schoonover
* Date: 11/15/12
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package view;

import java.awt.*;
import java.io.*;
import javax.swing.*;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class Help
//
/**
 * This class displays text from an HTML file in a viewing window.
 * 
 * @author Mike Schoonover
 * 
 */

public class Help extends JDialog{

	String helpFilename;


//-----------------------------------------------------------------------------
// Help::Help (constructor)
//
/**
 * Constructor which takes a JFrame as the parent GUI component.
 *
 * @param pFrame		the parent window
 * @param pHelpFilename	the full path to the help file in HTML format
 */

public Help(JFrame pFrame, String pHelpFilename)
{

    super(pFrame, "Help");

	helpFilename = pHelpFilename;

}//end of Help::Help (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Help::init
//
/**
 * Initializes the object.  Must be called immediately after instantiation.
 *
 * Creates the GUI, loads the file, and displays it.
 *
 */

public void init()
{

    setDefaultCloseOperation(DISPOSE_ON_CLOSE);

    int panelWidth = 400;
    int panelHeight = 500;

    setMinimumSize(new Dimension(panelWidth, panelHeight));
    setPreferredSize(new Dimension(panelWidth, panelHeight));
    setMaximumSize(new Dimension(panelWidth, panelHeight));

	JEditorPane editorPane = createEditorPane(helpFilename);

    JScrollPane scrollPane = new JScrollPane(editorPane);

	scrollPane.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

    add(scrollPane);

    setVisible(true);

}// end of Help::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Help::createEditorPane
//
/**
 * Creates the JEditorPane for displaying HTML files.Loads source file from pFilename and
 * displays it. Displays an error message if the file could not be loaded
 *
 * @param pFilename	the filename of the file to load and display
 *
 * @return	the new JEditorPane, even if the source file could not be loaded
 *
 */

protected JEditorPane createEditorPane(String pFilename)
{

	JEditorPane editorPane = new JEditorPane();
	editorPane.setEditable(false);

	File file = new File(pFilename);
	java.net.URL fileURL;

	try {
		fileURL = file.toURI().toURL();
		editorPane.setPage(fileURL);
	} catch (IOException e) {
		displayErrorMessage("Cannot read file: " + pFilename);
	}

	return(editorPane);

}// end of Help::createEditorPane
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// Help::displayErrorMessage
//
/**
 * Displays an error dialog window with message pMessage.
 *
 * @param pMessage	the message to display
 *
 */

private void displayErrorMessage(String pMessage)
{

    JOptionPane.showMessageDialog(null, pMessage, "Error", JOptionPane.ERROR_MESSAGE);

}//end of Help::displayErrorMessage
//-----------------------------------------------------------------------------

}//end of class Help
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
