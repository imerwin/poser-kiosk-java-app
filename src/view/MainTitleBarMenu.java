/******************************************************************************
* Title: MainTitleBarMenu.java
* Author: Mike Schoonover
* Date: 11/15/12
*
* Purpose:
*
* This class creates the main menu in the window title bar and sub-menus for the main form.
*
*
* Open Source Policy:
*
* This source code is Public Domain and free to any interested party.  Any
* person, company, or organization may do with it as they please.
*
*/

//-----------------------------------------------------------------------------

package view;

import java.awt.event.*;
import javax.swing.*;
import javax.swing.event.MenuListener;

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
// class MainTitleBarMenu
//
// This class creates the main menu in the window title bar and sub-menus for the main form.
//

public class MainTitleBarMenu extends JMenuBar{

    ActionListener actionListener;
	MenuListener menuListener;

    JMenu fileMenu;
    JMenuItem newFile, openFile, saveFile, saveFileAs;

    JMenu helpMenu;
    JMenuItem logMenuItem, aboutMenuItem, helpMenuItem, exitMenuItem;

//-----------------------------------------------------------------------------
// MainTitleBarMenu::MainTitleBarMenu (constructor)
//

String language;

public MainTitleBarMenu(ActionListener pActionListener, MenuListener pMenuListener)
{

    actionListener = pActionListener; menuListener = pMenuListener;

}//end of MainTitleBarMenu::MainTitleBarMenu (constructor)
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainTitleBarMenu::::init
//
// Initializes the object.  Must be called immediately after instantiation.
//
// The menuListener is added to each top level menu item so it can disable painting of GUI
// components to avoid overriding the sub-menus.
//

public void init()
{

    //File menu
    fileMenu = new JMenu("File");
    fileMenu.setMnemonic(KeyEvent.VK_F);
    fileMenu.setToolTipText("File");
	fileMenu.addMenuListener(menuListener);
    add(fileMenu);

    //File/New
    newFile = new JMenuItem("New File");
    newFile.setMnemonic(KeyEvent.VK_N);
    newFile.setToolTipText("Create a new file using the current information.");
    newFile.setActionCommand("New File");
    newFile.addActionListener(actionListener);
    fileMenu.add(newFile);

    //File/Open
    openFile = new JMenuItem("Open File");
    openFile.setMnemonic(KeyEvent.VK_O);
    openFile.setToolTipText("Open an existing file");
    openFile.setActionCommand("Choose File");
    openFile.addActionListener(actionListener);
    fileMenu.add(openFile);

    //File/Save
    saveFile = new JMenuItem("Save File");
    saveFile.setMnemonic(KeyEvent.VK_S);
    saveFile.setToolTipText("Saves data to file");
    saveFile.setActionCommand("Save File");
    saveFile.addActionListener(actionListener);
    fileMenu.add(saveFile);

    //File/Save As
    saveFileAs = new JMenuItem("Save File as...");
    saveFileAs.setMnemonic(KeyEvent.VK_A);
    saveFileAs.setToolTipText("Displays instructions for saving a file to a different name.");
    saveFileAs.setActionCommand("Save File As");
    saveFileAs.addActionListener(actionListener);
    fileMenu.add(saveFileAs);

    //File/Exit menu item
    exitMenuItem = new JMenuItem("Exit");
    exitMenuItem.setMnemonic(KeyEvent.VK_X);
    exitMenuItem.setToolTipText("Exit");
    exitMenuItem.setActionCommand("Exit Program");
    exitMenuItem.addActionListener(actionListener);
    fileMenu.add(exitMenuItem);

    JMenu toolsMenu;
    JMenuItem scopeStatusMenuItem;

    //Tools menu
    toolsMenu = new JMenu("Tools");
    toolsMenu.setMnemonic(KeyEvent.VK_T);
    toolsMenu.setToolTipText("Tools");
	toolsMenu.addMenuListener(menuListener);
    add(toolsMenu);

    //Mode/Scope Status
    scopeStatusMenuItem = new JMenuItem("Scope Status");
    scopeStatusMenuItem.setMnemonic(KeyEvent.VK_S);
    scopeStatusMenuItem.setToolTipText(
                "Display oscillscope status information.");
    scopeStatusMenuItem.setActionCommand("Display Scope Status");
    scopeStatusMenuItem.addActionListener(actionListener);
    toolsMenu.add(scopeStatusMenuItem);

	//Mode/Unlock Controls
    scopeStatusMenuItem = new JMenuItem("Unlock Controls");
    scopeStatusMenuItem.setMnemonic(KeyEvent.VK_U);
    scopeStatusMenuItem.setToolTipText("Unlocks the user controls and allows data to be modified.");
    scopeStatusMenuItem.setActionCommand("Unlock Controls");
    scopeStatusMenuItem.addActionListener(actionListener);
    toolsMenu.add(scopeStatusMenuItem);

    //Help menu
    helpMenu = new JMenu("Help");
    helpMenu.setMnemonic(KeyEvent.VK_H);
    helpMenu.setToolTipText("Help");
	helpMenu.addMenuListener(menuListener);
    add(helpMenu);

    //Help menu items and submenus

    //View menu items and submenus
    logMenuItem = new JMenuItem("Log");
    logMenuItem.setMnemonic(KeyEvent.VK_L);
    logMenuItem.setToolTipText("Log");
    logMenuItem.setActionCommand("Display Log Window");
    logMenuItem.addActionListener(actionListener);
    helpMenu.add(logMenuItem);

    //option to display the "About" window
    aboutMenuItem = new JMenuItem("About");
    aboutMenuItem.setMnemonic(KeyEvent.VK_A);
    aboutMenuItem.setToolTipText("Display the About window.");
    aboutMenuItem.setActionCommand("Display About");
    aboutMenuItem.addActionListener(actionListener);
    helpMenu.add(aboutMenuItem);

    //option to display the "Help" window
    helpMenuItem = new JMenuItem("Help");
    helpMenuItem.setMnemonic(KeyEvent.VK_H);
    helpMenuItem.setToolTipText("Display the Help window.");
    helpMenuItem.setActionCommand("Display Help");
    helpMenuItem.addActionListener(actionListener);
    helpMenu.add(helpMenuItem);

}// end of MainTitleBarMenu::::init
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainTitleBarMenu::refreshMenuSettings
//
// Sets menu items such as checkboxes and radio buttons to match their
// associated option values.  This function can be called after the variables
// have been loaded to force the menu items to match.
//

public void refreshMenuSettings()
{


}//end of MainTitleBarMenu::refreshMenuSettings
//-----------------------------------------------------------------------------

//-----------------------------------------------------------------------------
// MainTitleBarMenu::isSelected
//
// Returns true is any of the top level menu items are selected.
//
// NOTE: this is a workaround for JMenuBar.isSelected which once true never
// seems to go back false when the menu is no longer selected.
//

@Override
public boolean isSelected()
{

    //return true if any top level menu item is selected

    if (fileMenu.isSelected() || helpMenu.isSelected()) {
        return(true);
    }

    return false;

}//end of MainTitleBarMenu::isSelected
//-----------------------------------------------------------------------------

}//end of class MainTitleBarMenu
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------