#!/bin/bash


#kill the Oak camera Pose Recognition program and restart it

pkill --full "oakDLitePoseRecognizer.py"
source /home/spud/spud/oak/startOakDLitePoseRecognizer.sh

#remove this later? makes sure program is not hung up
killall java

#launch Spud Main Pi program

nohup java -jar Poser_Kiosk.jar &

# sleep for 3 seconds to ensure Java program has time to launch
# if this is not done, Java program never launches before terminal closes
# might be because moving a process to  the background takes time?

sleep 3s
