#!/bin/bash


#launch Spud Main Pi program

nohup java -jar Spud_Main_Pi.jar &

# sleep for 3 seconds to ensure Java program has time to launch
# if this is not done, Java program never launches before terminal closes
# might be because moving a process to  the background takes time?

sleep 3s